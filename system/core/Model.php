<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/libraries/config.html
 */
class CI_Model {
	
	
	
	

	//protected $redis ; 


	public function __construct()
	{
		log_message('info', 'Model Class Initialized');
		/*$this->redis = new Redis();
		$this->redis->pconnect('127.0.0.1','6379');
		$this->redis->auth(REDIS_PASS);
		$this->load->library('session');
		$this->load->database();*/
	}

	
	public function __get($key)
	{
		// Debugging note:
		//	If you're here because you're getting an error message
		//	saying 'Undefined Property: system/core/Model.php', it's
		//	most likely a typo in your model code.
		return get_instance()->$key;
	}
	protected static $illegalString = 
	array(
		'@','"','%','(',')','/','>','<','-',';','\\','\'','*'
		)
        
    ;
	
	protected function _checkIllegal($string = '')
    {
        foreach(CI_Model::$illegalString as $value)
        {
            if(strstr($string,$value) == true)
            {
                return true;
            }
        }
        return false;
    }
	
	
	
/*	public function getName($data)
	{
		
		$sql = 'SELECT * FROM admins WHERE 1 ;';
		$result = $this->db->query($sql,array($data['auto_id']))->row_array();
		return $result;
	}
	
	public function __Value($function,$data)
	{
		//持久化函数，用于有主键且为auto_id的数据的读取，即数据的唯一化
		//调用后返回数据，和一般函数相同，仅用于不需频繁更新的，需要持久化的页面
		
		/*key_id 键名，用于存储，一般为函数名+函数输入值（唯一确定的）  */
		/*function 函数名，用于调用函数*/
		/*data 函数值,要求其中有auto_id作为唯一判别式 
		
		$key_id = ''.$function.'_'.$data['auto_id'];
		$flag = $this->redis->get($key_id);
		
		if(empty($flag))
		{
			
			$result = $this->$function($data);
			$this->redis->setnx($key_id,serialize($result));
			return $result;
		}
		else
		{
			return @unserialize($flag);
		}
		
	}
	public function getPassage($data)
	{
		$sql = 'SELECT * FROM passages WHERE 1 ;';
		$result = $this->db->query($sql,array($data['auto_id']))->row_array();
		return $result;
	}

	public function _getLock($key_id)
	{
		$timestamp = time();
		$flag = 0;
		while($flag != 1)
		{
			$time = $this->redis->get($key_id);
			if(empty($time))
			{
				$this->redis->setnx($key_id,$timestamp);
				$flag = 1;
			}
			else
			{
				if(($timestamp-$time) > 10)
				{
					$this->redis->set($key_id,$timestamp);
					$flag = 1;
				}
				else
				{
					return array("code"=>"201","content"=>"busy");
					usleep(1000);
				}
			}
		}
		return array("code"=>"200","content"=>"success");
	}

	public function _releaseLock($key_id)
	{
		$time = $this->redis->get($key_id);
		$this->redis->del($key_id);
		return $time;
	}
	
	public function _redisKeyInterFace($function,$key_id,$data)
	{
		if($this->_getLock($key_id)['code'] == '200')
		{
			$result = $this->$function($data);
			$this->_releaseLock($key_id);
			return $result;
		}
	}
	
	public function _setMQ($function,$key_id,$data)
	{
		$this->redis->rpush($key_id,$function.serialize($data));
	}

	public function _dealMQ($function= '',$key_id = '')
	{
		$array = array();
		while($this->redis->llen($key_id) != 0)
		{
			$value = $this->redis->lpop($key_id);
			$data = @unserialize(substr($value, strlen(''.$function.'')));
			$result = $this->$function($data);
			$array[] = $result;
		}
		
	}*/
	

	
}
