<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

		<ul class="nav menu">
            <li><a href="/user/raw"><span class="glyphicon glyphicon-th"></span>  Raw Data</a></li>
			<li id="leave_li"><a href="/leave"><span class="glyphicon glyphicon-th"></span> Leave</a></li>
			<li id="sick_li"><a href="/sick_leave"><span class="glyphicon glyphicon-th"></span> Sick Leave</a></li>
			<li><a href="/overtime"><span class="glyphicon glyphicon-th"></span> Overtime</a></li>
			<li id="unpaid_li"><a href="/unpaid"><span class="glyphicon glyphicon-th"></span> Unpaid Leave</a></li>
			<li><a href="/allowance"><span class="glyphicon glyphicon-th"></span>  Allowance</a></li>

			<li role="presentation" class="divider"></li>
			<li class="parent " id="leader_nav">
				<a href="/approve">
					<span class="glyphicon glyphicon-list"></span> My Team<span data-toggle="" href="#sub-item-1" class="icon pull-right"></span>
				</a>
				<ul class="children " id="sub-item-1">
                    <li><a class="" href="/approve/raw"><span class="glyphicon glyphicon-share-alt"></span>Raw Data </a></li>
                    <li><a class="" href="/approve/general"><span class="glyphicon glyphicon-share-alt"></span>Calendar View</a></li>
					<li><a class="" href="/approve/leave"><span class="glyphicon glyphicon-share-alt"></span>Leave </a></li>
					<li><a class="" href="/approve/sick"><span class="glyphicon glyphicon-share-alt"></span>Sick Leave </a></li>
					<li><a class="" href="/approve/unpaid"><span class="glyphicon glyphicon-share-alt"></span>Unpaid Leave </a></li>
					<li><a class="" href="/approve/overtime"><span class="glyphicon glyphicon-share-alt"></span>Overtime </a></li>
					<li><a class="" href="/approve/allowance"><span class="glyphicon glyphicon-share-alt"></span>Allowance</a></li>
					<li><a class="" href="/approve/official"><span class="glyphicon glyphicon-share-alt"></span>Official Transfer </a></li>

                    <li><a class="" href="/approve/balance"><span class="glyphicon glyphicon-share-alt"></span>Balance View</a></li>




                </ul>
			</li>
			<li class="parent" id="hr_nav">
				<a href="/hr">
					<span class="glyphicon glyphicon-pencil"></span>HR<span data-toggle="" href="#sub-item-2" class="icon pull-right"></span>
				</a>
				<ul class="children " id="sub-item-2">
                    <li><a href="/hr/raw/0"><span class="glyphicon glyphicon-share-alt"></span>Raw Data</a></li>
					<li><a href="/hr/relation"><span class="glyphicon glyphicon-share-alt"></span>Report Line</a></li>
                    <li><a href="/hr/list"><span class="glyphicon glyphicon-share-alt"></span>List</a></li>
					<li><a href="/hr/resign/list"><span class="glyphicon glyphicon-share-alt"></span>Resigned List</a></li>
                    <li><a href="/hr/last"><span class="glyphicon glyphicon-share-alt"></span>Last Day</a></li>
                    <li><a href="/hr/head"><span class="glyphicon glyphicon-share-alt"></span>Head Count</a></li>
                    <li><a href="/hr/employee/report"><span class="glyphicon glyphicon-share-alt"></span>Attendance</a></li>
                    <li><a href="/hr/month/report"><span class="glyphicon glyphicon-share-alt"></span>Monthly Payroll</a></li>
                    <li><a href="/hr/balance/all"><span class="glyphicon glyphicon-share-alt"></span>Balance View</a></li>
                    <li><a href="/hr/balancechange/view"><span class="glyphicon glyphicon-share-alt"></span>Balance Change View</a></li>

                </ul>

			</li>
			<li role="presentation" class="divider"></li>
			<li id="sitemap"><a href="/sitemap"><span class="glyphicon glyphicon-th"></span>Company</a></li>
		</ul>
		
	</div><!--/.sidebar-->


<script src="/assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
	$.post("/user/get/name",
	{
		data :1
	},
	function(data,status)
	{
		data = eval(data);
		$("#user_name").text(data);
	});

	$.post("/api/getState",
		{
			data :1
		},
		function(data,stauts)
		{
			json1 = eval("("+data+")");
			switch(json1)
			{

				case 3:
					break;
				case 2:
					$("#leader_nav").empty();
					//$("#sitemap").empty();
					break;
				case 1:
					$("#hr_nav").empty();break;
				case 0:
					$("#leader_nav").empty();
					$("#hr_nav").empty();
					$("#sitemap").empty();
					break;
				default : alert("DFD");break;
			}
		});

	$.post("/api/unpaid/judge",
        {
            data : 1
        },function(data,status)
        {
            json1 = eval("("+data+")");
            if(json1.code == "101")
            {
                $("#unpaid_li").empty();
            }
			else if(json1.code == "200" && json1.msg == "al")
			{
				$("#leave_li").empty();
			}
			else if(json1.code == "200" && json1.msg == 'sl')
			{
				$("#sick_li").empty();
			}
			else
			{
				$("#leave_li").empty();
				$("#sick_li").empty();
			}
        });
</script>