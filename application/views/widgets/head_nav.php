<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/" style="background-color: white;
    /* overflow: auto; */
    display: inline-block;
    width: 180px;
    height: 50px;
    background-size: contain;

    background-image: url(&quot;/assets/img/MG_logo.png&quot;);
    background-repeat: no-repeat;"></a>
            <ul class="user-menu">
                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><label id="user_name"></label><span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/user/profile"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
                        <li><a href="/user/settings"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
                        <li><a href="/api/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div><!-- /.container-fluid -->
</nav>