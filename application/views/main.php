<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>MotionGlobal Leave Center</title>

<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/datepicker3.css" rel="stylesheet">
<link href="/assets/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<?php $this->load->view("/widgets/head_nav");?>
	<?php $this->load->view("/widgets/left_nav");?>	
	<?php
//	var_dump(preg_match("/^[0-9a-zA-z\.]+@motionglobal.com$/","louise.qiu@motionglobal.com"));exit;
	?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Main</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Leave Center</h1>
			</div>
		</div><!--/.row-->
        
		
		<div class="row">
			
			
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-red panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left" style="background-color: #dcdcdc">
							<em class="glyphicon glyphicon-stats glyphicon-l"></em>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large"><?php echo ($res['sick_days']);?></div>
							<div class="text-muted">Sick Days</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-teal panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
							<em class="glyphicon glyphicon-user glyphicon-l"></em>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large"><?php echo ($res['leave_days']);?></div>
							<div class="text-muted">Leave Days</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Monthly Leave Times
					<div class="btn-group">
					  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						Year <span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" role="menu">
						<?php foreach($years as $year) { ?>
						  <li><a onclick="getYearData(<?php echo $year['year']?>)"><?php echo $year['year']?></a></li>
						  <?php } ?>
					  </ul>
					</div>
					</div>
					<div class="panel-body">
						<div class="canvas-wrapper">
							<canvas class="main-chart" id="line-chart" height="200" width="600"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->
		
		</div><!--/.row-->
	</div>	<!--/.main-->

	<script src="/assets/js/jquery-1.11.1.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/chart.min.js"></script>
	<script src="/assets/js/easypiechart.js"></script>
	<script src="/assets/js/easypiechart-data.js"></script>
	<script src="/assets/js/bootstrap-datepicker.js"></script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})




		var randomScalingFactor = function(){ return Math.round(Math.random()*1000)};
	
	var lineChartData = {
			labels : ["Jan","Feb","Mar","Apr","May","June","July","Aug","Sept","Oct","Nov","Dec"],
			datasets : [
				{
					label: "Leave Days",
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [<?php echo implode(",", $sick);?>]
				},
				{
					label: "Sick Leave Days",
					fillColor : "rgba(48, 164, 255, 0.2)",
					strokeColor : "rgba(48, 164, 255, 1)",
					pointColor : "rgba(48, 164, 255, 1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(48, 164, 255, 1)",
					data : [<?php echo implode(",", $leave);?>]
				}
			]

		}
		
	

window.onload = function(){
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
		responsive: true
	});
	
	
};
	function getYearData(year)
	{
		$.post("/index/year",
		{
			year : year
		},
		function(data,status)
		{
			json1 = eval("("+data+")");//alert(json1.sick);return;
			lineChartData.datasets[0].data= [json1.sick];
			lineChartData.datasets[1].data= [json1.leave];
		});
		var chart1 = document.getElementById("line-chart").getContext("2d");

		window.myLine = new Chart(chart1).Line(lineChartData, {
			responsive: true
		});
	}


	</script>

	<script type="text/javascript">
//		var _paq = _paq || [];
//		_paq.push(['setUserId', 'ryoalex@foxmail.com' ]);
//		_paq.push(['setConversionAttributionFirstReferrer', false]);
//		_paq.push(['trackPageView']);
//		// Uncomment following line to track custom events
//		 _paq.push(['trackEvent', 'OroCRM', 'Tracking', 'view', '2333' ]);
//		(function() {
//			var u=(("https:" == document.location.protocol) ? "https" : "http") + "://" + "oro.snowmiao.cc" + "/";
//			_paq.push(['setTrackerUrl', u+'tracking.php']);
//			_paq.push(['setSiteId', 'hr\x2Dsbg\x2Dtest']);
//			var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
//			g.defer=true; g.async=true; g.src=u+'bundles/orotracking/js/piwik.min.js'; s.parentNode.insertBefore(g,s);
//		})();
	</script>

</body>

</html>
