<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>MG Leave Login</title>

<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/datepicker3.css" rel="stylesheet">
<link href="/assets/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="/assets/js/html5shiv.js"></script>
<script src="/assets/js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log in</div>
				<div class="panel-body">
					<form role="form">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="E-mail" id="email" type="email" autofocus="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" id="password" type="password" value="">
							</div>
							
							<a class="btn btn-primary" id="login" style="padding: 6px 20px">Login</a>
							<a class="btn btn-primary" id="register" style="background-color:#0fde2b;border-color:#0fde2b;padding: 6px 20px">Forget Password</a>
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
		

	<script src="/assets/js/jquery-1.11.1.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/chart.min.js"></script>
	<script src="/assets/js/chart-data.js"></script>
	<script src="/assets/js/easypiechart.js"></script>
	<script src="/assets/js/easypiechart-data.js"></script>
	<script src="/assets/js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})

		$("#login").click(function()
		{
			$.post("/login/action",
			{
				user : $("#email").val(),
				passwords : $("#password").val()
			},
			function(data,status)
			{
				json1 = eval("("+data+")");
				if(json1.code == "200")
				{
					location.href = '/';
				}
				else
				{
					alert(json1.text);
				}
			});

		});

		$("#register").click(function()
		{
			location.href = "user/reset/view";
		});

		
	</script>	
</body>

</html>
