<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MotionGlobal Leave Center</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/hr"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active">Hr</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Employee Information</h1>
        </div>
    </div><!--/.row-->

    <?php //var_dump($days);?>
    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                   <div class="row">
                       <div class="col-md-4"> <button type='button' class='btn btn-primary' onclick='location.href="/hr/new/employee"'>Add New Employee</button></div>

                       <div class="col-md-6"><input class="form-control" id="search_input"></div>
                       <div class="col-md-2"><button class="btn btn-warning" onclick="search()">Search</button></div>
                   </div>

                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead id ="table_content">
                        <tr id="member_info">
                            <th data-align="right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><font id="depart_text">Department</font><span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <?php foreach($departs as $depart) { ?>
                                            <li><a onclick="depart_filter('<?php echo$depart['departs']?>')"><?php echo$depart['departs']?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </th>
                            <th >Employee Name</th>
                            <th >Email Address</th>
                            <th>Title</th>
                            <th >Edit</th>

                        </tr>

                        </thead>

                    </table>

                </div>
            </div>


        </div>

        <!--        <div class="col-md-4">-->
        <!---->
        <!--            <div class="panel panel-red">-->
        <!--                <div class="panel-heading dark-overlay"><span class="glyphicon glyphicon-calendar"></span>Calendar</div>-->
        <!--                <div class="panel-body">-->
        <!--                    <div id="calendar"></div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!---->
        <!---->
        <!--        </div><!--/.col-->
    </div><!--/.row-->
</div>	<!--/.main-->

<script src="/assets/js/jquery-1.11.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/chart.min.js"></script>
<script src="/assets/js/easypiechart.js"></script>
<script src="/assets/js/easypiechart-data.js"></script>
<script src="/assets/js/bootstrap-datepicker.js"></script>
<script>
    $('#calendar').datepicker({
    });

    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

    function depart_filter(depart)
    {
        $.post("/hr/get/employee/depart",
            {
                depart : depart
            },
            function(data,status)
            {
                json1 = eval("("+data+")");
                $("#depart_text").text(depart);
                $("#table_content").siblings().remove();
                for(i=0;i<json1.length;i++)
                {
                    $(".table").append("<tr><th>"+depart+"</th><th>"+json1[i].user_name+"</th><th>"+json1[i].user_id+"</th><th>"+json1[i].title+"</th><th><button type='button' class='btn btn-success' onclick='edit("+json1[i].auto_id+")'>Edit</button></th></tr>");
                }
            });
    }

    function edit(id)
    {
        location.href = "/hr/edit/employee/"+id;
    }
    function view(id)
    {
        location.href ="/hr/raw/"+id;
    }

    function search()
    {
        $.post("/hr/get/list/search",
            {
                chars : $("#search_input").val()
            },
            function(data,status)
            {
                json1 = eval("("+data+")");
                $("#table_content").siblings().remove();
                for(i=0;i<json1.length;i++)
                {
                    $(".table").append("<tr><th>"+json1[i].department+"</th><th>"+json1[i].user_name+"</th><th>"+json1[i].user_id+"</th><th>"+json1[i].title+"</th><th><button type='button' class='btn btn-success' onclick='edit("+json1[i].auto_id+")'>Edit</button></th></tr>");
                }
            });
    }

</script>
</body>

</html>
