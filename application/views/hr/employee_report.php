<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Employee attendance Report</title>

    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/hr"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">hr</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-mg-8">
            <h1 class="">Employee Attendance Report</h1>
        </div>


    </div><!--/.row-->



    <div class="row">
        <div class="col-mg-12 "  id="#tab_user">
            <div class="panel panel-default">
                
                <div class="panel-body">
                    <div class="col-md-4">
                        <div class="panel-heading">Start Date</div>
                        <div id="start_date_input" class="input-append panel-body">
                            <input data-format="yyyy-MM-dd" type="text" id="start_date_content"></input>
                            <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel-heading">End Date</div>
                        <div id="end_date_input" class="input-append panel-body">
                            <input data-format="yyyy-MM-dd" type="text" id="end_date_content"></input>
                            <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel-heading">Action</div>
                        <div id="" class="input-append panel-body">
                            <input type="button" class="btn btn-success" value="Search" onclick="generate()">
                        </div>
                    </div>



                    <div class="col-md-12">
                        <form role="form">
                            <table class="table" id="table_content">
                                <thead id="tabel_head">
                                <tr>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><font id="depart_text">Department</font><span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <?php foreach($departs as $depart) { ?>
                                                    <li><a onclick="depart_filter('<?php echo $depart['departs']?>')"><?php echo $depart['departs']?></a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><font id="name_text">Name</font><span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu" id="name_ul">

                                            </ul>
                                        </div>
                                    </td>
                                    <td>Date</td>
                                    <td>Check in</td>
                                    <td>Check out</td>
                                    <td>Annual Leave</td>
                                    <td>Sick Leave</td>
                                    <td>Overtime Day</td>
                                    <td>Overtime Paid</td>
                                    <td>Missing Card</td>
                                    <td>Unpaid Day</td>
                                    <td>Allowance</td>
                                    <td>Reason</td>
                                </tr>
                                </thead>

                            </table>



                        </form>


                    </div>


                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->





</div><!--/.main-->

<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
        src="/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script>
    var departs = null;
    $('#start_date_input').datetimepicker({
        pickTime :false
    });

    $('#end_date_input').datetimepicker({
        pickTime :false
    });
    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

    function generate()
    {
        $.post("/api/get/employee/report",
            {
                start : $("#start_date_content").val(),
                end   : $("#end_date_content").val()
            },
            function(data,status)
            {
                $("#tabel_head").siblings().empty();
                json1 = eval("("+data+")");
                for(i=0;i<json1.length;i++)
                {
                    $("#table_content").append("<tr><td>"+json1[i].Department+"</td><td>"+json1[i].Name+"</td>" +
                        "<td>"+json1[i].Date+"</td>"+"<td>"+json1[i].Check_in+"</td><td>"+json1[i].Check_out+"</td>" +
                        "<td>"+json1[i].Annual_leave+"</td>"+"<td>"+json1[i].Sick_leave+"</td><td>"+json1[i].Overtime_Al+"</td>" +
                        "<td>"+json1[i].Overtime_paid+"</td><td>"+json1[i].Missing_card+"</td>" + "<td>"+json1[i].Unpaid+"</td>" +
                        "<td>"+json1[i].Allowance+"</td><td>"+json1[i].Reason+"</td></tr>");
                }
            });
    }

    function depart_filter(depart)
    {
        $("#name_ul").empty();
        $("#name_text").text("Name");
        $("#depart_text").text(depart);
        $.post("/api/get/name/depart",
            {
                depart: depart
            },
            function (data, status)
            {
                json1 = eval("(" + data + ")");
                for(i =0;i<json1.length;i++)
                {
                    $("#name_ul").append('<li><a onclick="name_filter(\''+json1[i].user_name+'\')">'+json1[i].user_name+'</a></li>');
                }
            });

        $.post("/api/get/employee/report",
            {
                start : $("#start_date_content").val(),
                end   : $("#end_date_content").val(),
                depart : depart
            },
            function(data,status)
            {
                $("#tabel_head").siblings().empty();
                json1 = eval("("+data+")");//alert(json1.length);return;
                for(i=0;i<json1.length;i++)
                {
                    $("#table_content").append("<tr><td>"+json1[i].Department+"</td><td>"+json1[i].Name+"</td>" +
                        "<td>"+json1[i].Date+"</td>"+"<td>"+json1[i].Check_in+"</td><td>"+json1[i].Check_out+"</td>" +
                        "<td>"+json1[i].Annual_leave+"</td>"+"<td>"+json1[i].Sick_leave+"</td><td>"+json1[i].Overtime_Al+"</td>" +
                        "<td>"+json1[i].Overtime_paid+"</td><td>"+json1[i].Missing_card+"</td>" + "<td>"+json1[i].Unpaid+"</td>" +
                        "<td>"+json1[i].Allowance+"</td><td>"+json1[i].Reason+"</td></tr>");
                }
            });
    }

    function name_filter(name)
    {
        $("#name_text").text(name);
        $.post("/api/get/employee/report",
            {
                start : $("#start_date_content").val(),
                end   : $("#end_date_content").val(),
                user : name
            },
            function(data,status)
            {
                $("#tabel_head").siblings().empty();
                json1 = eval("("+data+")");//alert(json1.length);return;
                for(i=0;i<json1.length;i++)
                {
                    $("#table_content").append("<tr><td>"+json1[i].Department+"</td><td>"+json1[i].Name+"</td>" +
                        "<td>"+json1[i].Date+"</td>"+"<td>"+json1[i].Check_in+"</td><td>"+json1[i].Check_out+"</td>" +
                        "<td>"+json1[i].Annual_leave+"</td>"+"<td>"+json1[i].Sick_leave+"</td><td>"+json1[i].Overtime_Al+"</td>" +
                        "<td>"+json1[i].Overtime_paid+"</td><td>"+json1[i].Missing_card+"</td>" + "<td>"+json1[i].Unpaid+"</td>" +
                        "<td>"+json1[i].Allowance+"</td><td>"+json1[i].Reason+"</td></tr>");
                }
            });
    }
</script>
</body>

</html>
