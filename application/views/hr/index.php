<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MotionGlobal Leave Center</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/hr"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active">Hr</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><HR> Panel</h1>
        </div>
    </div><!--/.row-->

    <?php //var_dump($days);?>
    <div class="row">

        <a href="/hr/raw/0">
            <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/hr/raw/0'">
                <div class="panel panel-teal panel-widget">
                    <div class="row no-padding">
                        <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                            <em class="glyphicon glyphicon-user glyphicon-l"></em>
                        </div>
                        <div class="col-sm-9 col-lg-7 widget-right">
                            <div class="large">Raw Data</div>
                            <div class="text-muted"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <a href="/hr/relation">
        <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/hr/relation'">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                        <em class="glyphicon glyphicon-user glyphicon-l"></em>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large">Report Line</div>
                        <div class="text-muted"></div>
                    </div>
                </div>
            </div>
        </div>
        </a>

        <a href="/hr/list">
        <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/hr/list'">
            <div class="panel panel-red panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                        <em class="glyphicon glyphicon-user glyphicon-l"></em>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large">List</div>
                        <div class="text-muted"></div>
                    </div>
                </div>
            </div>
        </div>
        </a>


        <a href="/hr/last">
        <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/hr/last'">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                        <em class="glyphicon glyphicon-user glyphicon-l"></em>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large">Last Day</div>
                        <div class="text-muted"></div>
                    </div>
                </div>
            </div>
        </div>
        </a>

        <a href="/hr/head">
        <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/hr/head'">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                        <em class="glyphicon glyphicon-user glyphicon-l"></em>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large">Head Count</div>
                        <div class="text-muted"></div>
                    </div>
                </div>
            </div>
        </div>
        </a>


        <a href="/hr/resign/list">
            <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/hr/employee/report'">
                <div class="panel panel-teal panel-widget">
                    <div class="row no-padding">
                        <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                            <em class="glyphicon glyphicon-user glyphicon-l"></em>
                        </div>
                        <div class="col-sm-9 col-lg-7 widget-right">
                            <div class="large">Resigned List</div>
                            <div class="text-muted"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <a href="/hr/employee/report">
        <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/hr/employee/report'">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                        <em class="glyphicon glyphicon-user glyphicon-l"></em>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large">Employee Report</div>
                        <div class="text-muted"></div>
                    </div>
                </div>
            </div>
        </div>
        </a>

        <a href="/hr/month/report">
        <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/hr/month/report'">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                        <em class="glyphicon glyphicon-user glyphicon-l"></em>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large">Monthly Payroll</div>
                        <div class="text-muted"></div>
                    </div>
                </div>
            </div>
        </div>
        </a>

        <a href="/hr/balance/all">
            <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/hr/balance/all'">
                <div class="panel panel-teal panel-widget">
                    <div class="row no-padding">
                        <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                            <em class="glyphicon glyphicon-user glyphicon-l"></em>
                        </div>
                        <div class="col-sm-9 col-lg-7 widget-right">
                            <div class="large">Balance View</div>
                            <div class="text-muted"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div><!--/.row-->
</div>	<!--/.main-->

<script src="/assets/js/jquery-1.11.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/chart.min.js"></script>
<script src="/assets/js/easypiechart.js"></script>
<script src="/assets/js/easypiechart-data.js"></script>
<script src="/assets/js/bootstrap-datepicker.js"></script>
<script>
    $('#calendar').datepicker({
    });

    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

</script>
</body>

</html>
