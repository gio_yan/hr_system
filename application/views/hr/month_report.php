<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Monthly Payroll Report</title>

    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/hr"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">hr</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Monthly Payroll Report</h1>
            <h3 class="page-header"></h3>
        </div>
    </div><!--/.row-->



    <div class="row">
        <div class="col-mg-12 "  id="#tab_user">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-4">
                        <div class="panel-heading">Start Date</div>
                        <div id="start_date_input" class="input-append panel-body">
                            <input data-format="yyyy-MM-dd" type="text" id="start_date_content"></input>
                            <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel-heading">End Date</div>
                        <div id="end_date_input" class="input-append panel-body">
                            <input data-format="yyyy-MM-dd" type="text" id="end_date_content"></input>
                            <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel-heading">Action</div>
                        <div id="" class="input-append panel-body">
                            <input type="button" class="btn btn-success" value="Search" onclick="generate()">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <form role="form">
                            <table class="table" id="table_content">
                                <tr>
                                    <td>Department</td>
                                    <td>Name</td>
                                    <td>Paid Overtime Days</td>
                                    <td>Unpaid Days</td>
                                    <td>On hold</td>
                                    <td>Notes</td>
                                </tr>

                            </table>



                        </form>


                    </div>


                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->





</div><!--/.main-->

<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
        src="/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script>
    var departs = null;
    $('#start_date_input').datetimepicker({
        pickTime :false
    });

    $('#end_date_input').datetimepicker({
        pickTime :false
    });
    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

    function generate()
    {
        $.post("/api/get/month/report",
            {
                start : $("#start_date_content").val(),
                end   : $("#end_date_content").val()
            },
            function(data,status)
            {
                $("#table_content").empty();
                $("#table_content").append("<tr><td>Department</td><td>Name</td><td>Paid Overtime Days</td><td>Unpaid Days</td><td>On hold</td><td>Notes</td></tr>");
                json1 = eval("("+data+")");
                for( x in json1)
                {
                    $("#table_content").append("<tr><td>"+(json1[x].department)+"</td><td>"+(json1[x].name)+"</td><td>"+(json1[x].overtime)+"</td><td>"+(json1[x].unpaid)+"</td><td></td><td>"+json1[x].notes+"</td></tr>");
                }
            });
    }
</script>
</body>

</html>
