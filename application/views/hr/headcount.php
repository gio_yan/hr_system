<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Head Count</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/hr"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active">Hr</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">HeadCount</h1>
        </div>
    </div><!--/.row-->

    <?php //var_dump($days);?>
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Employee Now
                </div>
                <div class="panel-body" id="now_table">
                    <table class="table">
                        <thead id ="table_content">
                        <tr id="member_info">
                            <th data-align="">
                                Month/Year
                            </th>
                            <th >CS</th>
                            <th >IT</th>
                            <th >SC</th>
                            <th >MC</th>
                            <th >HR</th>
                            <th >FIN</th>
                            <th >MKT</th>
                            <th >BI</th>
                            <th >Director</th>
                            <th >Total</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($now as $r=>$v) {?>
                        <tr>
                            <td><?php echo$r?></td>
                            <td><?php echo$v['CS']?></td>
                            <td><?php echo$v['IT']?></td>
                            <td><?php echo$v['SC']?></td>
                            <td><?php echo$v['MC']?></td>
                            <td><?php echo$v['HR']?></td>
                            <td><?php echo$v['FIN']?></td>
                            <td><?php echo$v['MKT']?></td>
                            <td><?php echo$v['BI']?></td>
                            <td><?php echo$v['Director']?></td>
                            <td><?php echo array_sum($v);?></td>
                        </tr>
                        <?php }?>
                        </tbody>

                    </table>

                </div>


                <div class="panel-heading">
                    New Hire
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead id ="table_content">
                        <tr id="member_info">
                            <th data-align="">
                                Month/Year
                            </th>
                            <th >CS</th>
                            <th >IT</th>
                            <th >SC</th>
                            <th >MC</th>
                            <th >HR</th>
                            <th >FIN</th>
                            <th >MKT</th>
                            <th >BI</th>
                            <th >Director</th>
                            <th >Total</th>

                        </tr>
                        <?php foreach($new as $r=>$v) {?>
                            <tr>
                                <th><?php echo$r?></th>
                                <th><?php echo$v['CS']?></th>
                                <th><?php echo$v['IT']?></th>
                                <th><?php echo$v['SC']?></th>
                                <th><?php echo$v['MC']?></th>
                                <th><?php echo$v['HR']?></th>
                                <th><?php echo$v['FIN']?></th>
                                <th><?php echo$v['MKT']?></th>
                                <th><?php echo$v['BI']?></th>
                                <th><?php echo$v['Director']?></th>
                                <td><?php echo array_sum($v);?></td>
                            </tr>
                        <?php }?>
                        </thead>

                    </table>

                </div>

                <div class="panel-heading">
                    Left
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead id ="table_content">
                        <tr id="member_info">
                            <th data-align="">
                                Month/Year
                            </th>
                            <th >CS</th>
                            <th >IT</th>
                            <th >SC</th>
                            <th >MC</th>
                            <th >HR</th>
                            <th >FIN</th>
                            <th >MKT</th>
                            <th >BI</th>
                            <th >Director</th>
                            <th >Total</th>

                        </tr>
                        <?php foreach($leave as $r=>$v) {?>
                            <tr>
                                <th><?php echo$r?></th>
                                <th><?php echo$v['CS']?></th>
                                <th><?php echo$v['IT']?></th>
                                <th><?php echo$v['SC']?></th>
                                <th><?php echo$v['MC']?></th>
                                <th><?php echo$v['HR']?></th>
                                <th><?php echo$v['FIN']?></th>
                                <th><?php echo$v['MKT']?></th>
                                <th><?php echo$v['BI']?></th>
                                <th><?php echo$v['Director']?></th>
                                <td><?php echo array_sum($v);?></td>
                            </tr>
                        <?php }?>
                        </thead>

                    </table>

                </div>
            </div>


        </div>

        <!--        <div class="col-md-4">-->
        <!---->
        <!--            <div class="panel panel-red">-->
        <!--                <div class="panel-heading dark-overlay"><span class="glyphicon glyphicon-calendar"></span>Calendar</div>-->
        <!--                <div class="panel-body">-->
        <!--                    <div id="calendar"></div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!---->
        <!---->
        <!--        </div><!--/.col-->
    </div><!--/.row-->
</div>	<!--/.main-->

<script src="/assets/js/jquery-1.11.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/chart.min.js"></script>
<script src="/assets/js/easypiechart.js"></script>
<script src="/assets/js/easypiechart-data.js"></script>
<script src="/assets/js/bootstrap-datepicker.js"></script>
<script src="/assets/js/FileSaver.js"></script>
<script>
    $('#calendar').datepicker({
    });

    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })
    function depart_filter(depart)
    {
        $.post("/hr/get/employee/depart",
            {
                depart : depart
            },
            function(data,status)
            {
                json1 = eval("("+data+")");
                $("#depart_text").text(depart);
                $("#table_content").siblings().remove();
                for(i=0;i<json1.length;i++)
                {
                    $(".table").append("<tr><th>"+depart+"</th><th>"+json1[i].user_name+"</th><th>"+json1[i].user_id+"</th><th><button type='button' class='btn btn-success' onclick='edit("+json1[i].auto_id+")'>Edit</button></th><th>Bye</th></tr>");
                }
            });
    }

    function edit(id)
    {
        location.href = "/hr/edit/employee/"+id;
    }

    function test()
    {
        var blob = new Blob([document.getElementById('now_table').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
        var strFile = "Report.xls";
        saveAs(blob, strFile);
        return false;

    }

</script>
</body>

</html>
