<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Relation Management</title>

    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/hr"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">hr</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Report Line</h1>
        </div>
    </div><!--/.row-->



    <div class="row">
        <div class="col-mg-12 "  id="#tab_user">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class = 'row'>
                        <div class="col-md-4"><input class="form-control" id="search_input"></div>
                        <div class="col-md-4"><button class="btn btn-warning" onclick="search()">Search</button></div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <form role="form">
                            <table class="table" id="table_content">
                                <tr>
                                    <td>Depart</td>
                                    <td>Name</td>
                                    <td>Manager Depart</td>
                                    <td>Manager Name</td>
                                    <td>Edit</td>
                                    <td>Save</td>
                                </tr>
                                <?php foreach($content as $re) { ?>
                                <tr>
                                    <th id="em_depart_<?php echo$re['em_id']?>"><?php echo$re['em_depart']?></th>
                                    <th id="em_name_<?php echo$re['em_id']?>"><?php echo$re['em_name']?></th>
                                    <th id="ma_depart_<?php echo$re['em_id']?>"><?php echo$re['ma_depart']?></th>
                                    <th id="ma_name_<?php echo$re['em_id']?>"><?php echo$re['ma_name']?></th>
                                    <th><button type="button" class="btn btn-success" onclick="edit(<?php echo$re['em_id']?>)">Edit</button></th>
                                    <th><button type="button" class="btn btn-primary" onclick="save(<?php echo$re['em_id']?>)">Save</button></th>
                                </tr>
                                <?php }?>
                            </table>



                        </form>

                        <div style="padding-left: 40%">
                            <ul class="pagination" id="paginations">
                                <li class="page-first"><a onclick="locate(0)">&lt;&lt;</a></li>
                                <li class="page-pre"><a onclick="locate(<?php echo($page-1)<=0?0:($page-1)?>)">&lt;</a></li>
                                <?php foreach($pages as $p) {?>
                                    <li class="page"><a onclick="locate(<?php echo$p?>)"><?php echo(1+$p)?></a></li>
                                <?php }?>
                                <li class="page-next"><a onclick="locate(<?php echo($page+1)>=($all-1)?($all-1):($page+1) ?>)">&gt;</a></li>
                                <li class="page-last"><a  onclick="locate(<?php echo($all-1)?>)">&gt;&gt;</a></li>
                            </ul>
                        </div>
                    </div>


                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->





</div><!--/.main-->

<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
        src="/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script>
    var departs = null;
    $('#start_date_input').datetimepicker({
        pickTime :false
    });
    $.post("/api/get/departs",
    {
        data : 1
    },
    function(data,status)
    {
       departs = data;// eval("("+data+")");
    });
    $('#birthday_input').datetimepicker({
        pickTime :false
    });
    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

    $(document).ready(function(){
        $("#tab_detail").css("display","block");
    });

    function locate(id)
    {
        $.post("/api/get/relation",
            {
                page : id
            },
            function(data,status)
            {
                json1 = eval("("+data+")");
                $("#paginations").empty();
                $("#table_content").empty();
                $("#table_content").append('<tr> <td>Depart</td> <td>Name</td> <td>Manager Depart</td> <td>Manager Name</td> <td>Edit</td> <td>Save</td> </tr>');
                //alert(json1.content[0].user_name);return;
                for(i =0;i<json1.content.length;i++)
                {
                    var contents = '<tr><th id="em_depart_'+json1.content[i].em_id+'">'+json1.content[i].em_depart+'</th><th id="em_name_'+json1.content[i].em_id+'">'+json1.content[i].em_name+'</th><th id="ma_depart_'+json1.content[i].em_id+'">'+json1.content[i].ma_depart+'</th><th id="ma_name_'+json1.content[i].em_id+'">'+json1.content[i].ma_name+'</th><th><button type="button" class="btn btn-success" onclick="edit('+json1.content[i].em_id+')">Edit</button></th><th><button type="button" class="btn btn-primary" onclick="save('+json1.content[i].em_id+')">Save</button></th>';
                    $("#table_content").append(contents);

                }
                $("#paginations").append('<li class="page-first"><a onclick="locate(0)">&lt;&lt;</a></li><li class="page-pre"><a onclick="locate('+((json1.page-1)<=0 ? 0 : (json1.page-1))+')">&lt;</a></li>');
                for(i =0;i<json1.pages.length;i++)
                {
                    $("#paginations").append('<li class="page"><a onclick="locate('+json1.pages[i]+')"</a>'+(1+json1.pages[i])+'</li>');
                }
                var p = (1+parseInt(json1.page)) >=(json1.all-1) ? (json1.all-1) : (1+parseInt(json1.page)) ;
                $("#paginations").append('<li class="page-next"><a onclick="locate('+p+')">&gt;</a></li><li class="page-last"><a  onclick="locate('+(json1.all-1)+')">&gt;&gt;</a></li>');

            });
    }

    function edit(id)
    {
        $("#ma_depart_"+id).empty();
        var name = "ma_de_select_"+id;
        $("#ma_depart_"+id).append("<select class='ma-dp' id='ma_de_select_"+id+"' onchange='getMa("+id+")'></select>");
        $("#ma_name_"+id).empty();
        $("#ma_name_"+id).append("<select class='' id='ma_user_select_"+id+"'></select>");
        json1 = eval("("+departs+")");
        for(i = 0 ;i< json1.length ;i++)
        {
            $("#ma_de_select_"+id).append("<option value='"+json1[i]+"'>"+json1[i]+"</option>");
        }

    }

    function getMa(id)
    {
        $("#ma_user_select_"+id).empty();
        $.post("/hr/get/employee/depart",
            {
                depart : $("#ma_de_select_"+id+" option:selected").val()
            },
            function(data,status)
            {
                json1 = eval("("+data+")");
                for(i=0;i<json1.length;i++)
                {
                    $("#ma_user_select_"+id).append("<option value='"+json1[i].auto_id+"'>"+json1[i].user_name+"</option>");
                }
            });
    }


    function save(id)
    {
        if(confirm("Are you sure to update the relation? "))
        {
            $.post("/hr/set/relation",
                {
                    em_id : id,
                    ma_id : $("#ma_user_select_"+id+" option:selected").val()
                },
                function(data,status)
                {
                    json1 = eval("("+data+")");
                    if(json1.code == "200")
                    {
                        alert("Relation has been updated");
                        location.href = "/hr/relation";
                    }
                    else
                    {
                        location.href = "/";
                    }
                });
        }

    }

    function search()
    {
        $.post("/hr/get/relation/search",
            {
                chars : $("#search_input").val()
            },
            function(data,status)
            {
                json1 = eval("("+data+")");
                $("#paginations").empty();
                $("#table_content").empty();
                $("#table_content").append('<tr> <td>Depart</td> <td>Name</td> <td>Manager Depart</td> <td>Manager Name</td> <td>Edit</td> <td>Save</td> </tr>');
                //alert(json1.content[0].user_name);return;
                for(i =0;i<json1.length;i++)
                {
                    var contents = '<tr><th id="em_depart_'+json1[i].em_id+'">'+json1[i].em_depart+'</th><th id="em_name_'+json1[i].em_id+'">'+json1[i].em_name+'</th><th id="ma_depart_'+json1[i].em_id+'">'+json1[i].ma_depart+'</th><th id="ma_name_'+json1[i].em_id+'">'+json1[i].ma_name+'</th><th><button type="button" class="btn btn-success" onclick="edit('+json1[i].em_id+')">Edit</button></th><th><button type="button" class="btn btn-primary" onclick="save('+json1[i].em_id+')">Save</button></th>';
                    $("#table_content").append(contents);

                }
            });
    }
</script>
</body>

</html>
