<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Profile</title>

    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">Profile</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Employee Information</h1>
            <h3 class="page-header"></h3>
        </div>
    </div><!--/.row-->



    <div class="row">
        <div class="col-mg-12 "  id="#tab_user">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <div class="col-md-4">
                        <form role="form">

                            <div class="panel panel-default">
                                <div class="panel-heading"><font color="red">Employee Email*</font></div>
                                <div class="panel-body" id="work_email_input">

                                </div>
                                <div class="panel-heading"><font color="red">Employee Name*</font></div>
                                <div class="panel-body" id="name_input">

                                </div>

                                <div class="panel-heading"><font color="red">Employee Department*</font></div>
                                <div class="panel-body" id="department_ipnut">
                                    <select class="form-control" id="department_content">
                                        <?php foreach($departs as $depart) { ?>
                                            <option value="<?php echo$depart['departs']?>" ><?php echo$depart['departs']?></option>
                                        <?php }?>
                                    </select>
                                </div>

                                <div class="panel-heading"><font color="red">Employee Title*</font></div>
                                <div class="panel-body" id="title_input">

                                </div>



                                <div class="panel-heading">Employee On Board Date</div>
                                <div id="start_date_input" class="input-append panel-body">
                                    <input data-format="yyyy-MM-dd" type="text" id="start_date_content"></input>
                                    <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                                </div>




                            </div>


                            <!--                            <button type="button" class="btn btn-success">Edit</button>-->
                            <!--                            <button type="button" class="btn btn-primary">Save</button>-->
                        </form>
                    </div>

                    <div class="col-md-4">
                        <form role="form">

                            <div class="panel panel-default">




                                <div class="panel-heading">Employee Office Phone Number</div>
                                <div class="panel-body" id="phone_number_input">

                                </div>
                                <div class="panel-heading">Employee Moblie Number</div>
                                <div class="panel-body" id="mobile_number_input">

                                </div>
                                <div class="panel-heading">Employee Chinese Name</div>
                                <div class="panel-body" id="chinese_name_input">

                                </div>

                                <div class="panel-heading">Employee Work Location</div>
                                <div class="panel-body" id="work_location_input">

                                </div>

                                <div class="panel-heading"><font color="red">Is Intern?*</font></div>
                                <div class="panel-body" id="intern_input">
                                    <select class="form-control" id="intern_content">
                                        <option value="no" >No</option>
                                        <option value="yes" >Yes</option>
                                    </select>
                                </div>


                            </div>





                        </form>
                    </div>

                    <div class = "col-md-4">
                        <div class="panel panel-default">

                            <div class="panel-heading">Employee Birthday</div>
                            <div id="birthday_input" class="input-append panel-body">
                                <input data-format="yyyy-MM-dd" type="text" id="birthday_content"></input>
									    <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                            </div>

                            <div class="panel-heading">Employee Personal Email</div>
                            <div class="panel-body" id="personal_email_input">

                            </div>
                            <div class="panel-heading">Employee Skype</div>
                            <div class="panel-body" id="skype_input">

                            </div>

                            <div class="panel-heading">Employee Home Address</div>
                            <div class="panel-body" id="home_address_input">

                            </div>

                            <div class="panel-heading"><font color="red">Is Probation?*</font></div>
                            <div class="panel-body" id="intern_input">
                                <select class="form-control" id="probation_content">
                                    <option value="no" >No</option>
                                    <option value="yes" >Yes</option>
                                </select>
                            </div>



                        </div>


                        <button type="button" class="btn btn-primary btn-big">Save</button>
                    </div>
                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->





</div><!--/.main-->


<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
        src="/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script>

    $('#start_date_input').datetimepicker({
        pickTime :false
    });

    $('#birthday_input').datetimepicker({
        pickTime :false
    });

    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

    $("#work_email_input").append('<input class="form-control" id="work_email_content" value="">');
    $("#name_input").append('<input class="form-control" id="name_content">');
    $("#title_input").append('<input class="form-control" id="title_content" value="">');
   // $("#department_ipnut").append('<input class="form-control" id="department_content">');
   // $("#start_date_input").append('<input class="form-control" id="start_date_content">');
    $("#phone_number_input").append('<input class="form-control" id="phone_number_content" >');
    $("#mobile_number_input").append('<input class="form-control" id="mobile_number_content" >');
    $("#chinese_name_input").append('<input class="form-control" id="chinese_name_content" value="">');
    $("#work_location_input").append('<input class="form-control" id="work_location_content" value="">');
  //  $("#birthday_input").append('<input class="form-control" id="birthday_content" value="">');
    $("#personal_email_input").append('<input class="form-control" id="personal_email_content" value="">');
    $("#skype_input").append('<input class="form-control" id="skype_content" value="">');
    $("#home_address_input").append('<input class="form-control" id="home_address_content" value="">');

    $(".btn-primary").attr("onclick","post_to()");


    function post_to()
    {
        if($("#work_email_content").val() == '' || $("#name_content").val() == '' ||
            $("#title_content").val() == ''|| $("#department_content").val() == '')
        {
            alert("Please fill basic information of the new employed");return;
        }
        else
        {
            $.post("/hr/new/employee/action",
                {
                    work_email :  $("#work_email_content").val(),
                    name :  $("#name_content").val(),
                    title : $("#title_content").val(),
                    department: $("#department_content").val(),
                    start_date : $("#start_date_content").val(),
                    phone_number: $("#phone_number_content").val(),
                    moblie_number : $("#mobile_number_content").val(),
                    chinese_name: $("#chinese_name_content").val(),
                    work_location : $("#work_location_content").val(),
                    birthday: $("#birthday_content").val(),
                    personal_email : $("#personal_email_content").val(),
                    skype: $("#skype_content").val(),
                    home_address : $("#home_address_content").val(),
                    intern : $("#intern_content").val(),
                    probation : $("#probation_content").val(),
                },
                function(data,status)
                {
                    json1 = eval("("+data+")");
                    if(json1.code == "200")
                    {
                        confirm("Employee profile has been inserted");
                        location.href= '/hr';
                    }
                    else
                    {
                        alert(json1.msg);
                    }
                });
        }

    }
</script>
</body>

</html>
