<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Raw Data Report</title>

    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/hr"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">hr</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Balance Change Report</h1>
        </div>
    </div><!--/.row-->



    <div class="row">
        <div class="col-mg-12 "  id="#tab_user">
            <div class="panel panel-default">
                <div class="panel-body">


                    <div class="col-md-4">
                        <div class="panel-heading">Department</div>
                        <div id="" class="input-append panel-body">
                            <select id="department_selector" onchange="depart_filter()">
                                <?php foreach($departs as $depart) { ?>
                                    <option value="<?php echo $depart['departs']?>"><?php echo $depart['departs']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel-heading">Name</div>
                        <div id="" class="input-append panel-body">
                            <select id="employee_selector">

                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel-heading">Action</div>
                        <div id="" class="input-append panel-body">
                            <input type="button" class="btn btn-success" value="Search" onclick="generate()">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <form role="form">
                            <table class="table" id="table_content">
                                <tr>
                                    <td>Create Time</td>
                                    <td>Start Time</td>
                                    <td>End Time</td>
                                    <td>Leave Day Balance</td>
                                    <td>Sick Leave Day Balance</td>
                                    <td>Time Length</td>
                                    <td>Reason</td>
                                </tr>
                            </table>
                        </form>
                    </div>

                    <div class="col-md-12">

                        <div class="panel-body" >

                            <div style="margin-bottom: 6px"><div style="background: #e5e5e5;color:#ffffff;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>&nbsp; <div style="float:left"> Annual leave</div></div>
                            <div style="margin-bottom: 6px"><div style="background: orangered;color:#ffffff;float:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>&nbsp; <div style="float:left"> Sick Leave</div></div>
                            <div style="margin-bottom: 6px"><div style="background: #7ec801;float:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>&nbsp; <div style="float:left">Overtime</div></div>
                            <div id="new_balance_record"></div>
                        </div>

                    </div>

                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->





</div><!--/.main-->

<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
        src="/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script>
    var departs = null;
    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

    function generate()
    {
        $.post("/hr/balancechange/get",
            {
                id : $("#employee_selector").val(),
                depart : $("#department_selector").val()
            },
            function(data,status)
            {
                trColor = '';

                $("#table_content").empty();

                json1 = eval("("+data+")");

                if(json1.code=="101")
                {
                    alert(json1.msg);
                }
                $("#table_content").append("<tr><td>Create Time</td><td>Start Time</td><td>End Time</td><td>Leave Day Balance</td><td>Sick Leave Day Balance</td><td>Time Length</td><td>Reason</td></tr>");
                for(i=0;i<json1.length;i++)
                {
                    if(json1[i].flag == 0) // leave
                        trColor = "#e5e5e5";
                    else if(json1[i].flag == 1) // ot
                        trColor = "#7ec801"+";color: white";
                    else // sick leave
                        trColor = "orangered"+";color: white";
                    if(i != json1.length - 1)
                        $("#table_content").append("<tr style='background-color: "+ trColor +"'><td>"+json1[i].create_time+"</td><td>"+json1[i].start_time+"</td>" +
                            "<td>"+json1[i].end_time+"</td>"+"<td>"+json1[i].leave_day_balance+"</td><td>"+json1[i].sick_leave_day_balance+"</td><td>"+json1[i].time_length+"</td>" +
                            "<td>"+json1[i].reason+"</td></tr>");
                }
                $("#new_balance_record").empty();
                $("#new_balance_record").append("<p  style='font-size: initial;' >"+"Update Time: "+json1[json1.length-1][0].cal_year+", add leave balance: "+json1[json1.length-1][0].leave_day_balance+"</p>");

            });
    }

    function depart_filter()
    {
        $("#employee_selector").empty();
        $.post("/api/get/name/depart",
            {
                depart: $("#department_selector").val()
            },
            function (data, status)
            {
                json1 = eval("(" + data + ")");
                for(i =0;i<json1.length;i++)
                {
                    $("#employee_selector").append('<option value='+json1[i].user_id+'>'+json1[i].user_name+'</option>');
                }
            });
    }
</script>
</body>

</html>
