<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Profile</title>

    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">Profile</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Employee Information</h1>
            <h3 class="page-header"></h3>
        </div>
    </div><!--/.row-->



    <div class="row">
        <div class="col-mg-12 "  id="#tab_user">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <div class="col-md-4">
                        <form role="form">

                            <div class="panel panel-default">
                                <div class="panel-heading"><font color="red">Employee Email*</font></div>
                                <div class="panel-body" id="work_email_input">
                                    <?php echo $content['work_email'];?>
                                </div>
                                <div class="panel-heading"><font color="red">Employee Name*</font></div>
                                <div class="panel-body" id="name_input">
                                    <?php echo $content['name'];?>
                                </div>
                                <div class="panel-heading"><font color="red">Employee Department*</font></div>
                                <div class="panel-body" id="department_ipnut">
                                    <select class="form-control" id="department_content">
                                        <?php foreach($departs as $depart) { ?>
                                            <option value="<?php echo $depart['departs']?>" ><?php echo $depart['departs']?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="panel-heading"><font color="red">Employee Title*</font></div>
                                <div class="panel-body" id="title_input">
                                    <?php echo $content['title'];?>
                                </div>

                                <div class="panel-heading">Employee On Board Date</div>
                                <div id="start_date_input" class="input-append panel-body">
                                    <input data-format="yyyy-MM-dd" type="text" id="start_date_content" value="<?php echo $content['start_date'];?>"></input>
                                        <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                                </div>

                                <div class="panel-heading"><font color="red">Is Probation?*</font></div>
                                <div class="panel-body" id="probation_input">
                                    <select class="form-control" id="probation_content">
                                        <option value="<?php echo $probations[0]?>" ><?php echo $probations[0]?></option>
                                        <option value="<?php echo $probations[1]?>" ><?php echo $probations[1]?></option>
                                    </select>
                                </div>

                            </div>


                            <!--                            <button type="button" class="btn btn-success">Edit</button>-->
                            <!--                            <button type="button" class="btn btn-primary">Save</button>-->
                        </form>
                    </div>

                    <div class="col-md-4">
                        <form role="form">

                            <div class="panel panel-default">



                                <div class="panel-heading">Employee Office Phone Number</div>
                                <div class="panel-body" id="phone_number_input">
                                    <?php echo $content['phone_number'];?>
                                </div>
                                <div class="panel-heading">Employee Moblie Number</div>
                                <div class="panel-body" id="mobile_number_input">
                                    <?php echo $content['moblie_number'];?>
                                </div>
                                <div class="panel-heading">Employee Chinese Name</div>
                                <div class="panel-body" id="chinese_name_input">
                                    <?php echo $content['chinese_name'];?>
                                </div>
                                <!--                                    <div class="panel-heading">Your Work Email</div>-->
                                <!--                                    <div class="panel-body" id="">-->
                                <!--                                        --><?php//// echo $data['work_email'];?>
                                <!--                                    </div>-->
                                <div class="panel-heading">Employee Work Location</div>
                                <div class="panel-body" id="work_location_input">
                                    <?php echo $content['work_location'];?>
                                </div>


                                <div class="panel-heading"><font color="red">Probation Date</font></div>

                                <div id="probation_date_input" class="input-append panel-body">
                                    <input data-format="yyyy-MM-dd" type="text" id="prbation_date_content" value="<?php echo $content['probation_date'];?>"></input>
                                        <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                                </div>



                                <div class="panel-heading"><font color="red">Is Intern?*</font></div>
                                <div class="panel-body" id="intern_input">
                                    <select class="form-control" id="intern_content">
                                        <option value="<?php echo $interns[0]?>" ><?php echo $interns[0]?></option>
                                        <option value="<?php echo $interns[1]?>" ><?php echo $interns[1]?></option>
                                    </select>
                                </div>

                            </div>





                        </form>
                    </div>

                    <div class = "col-md-4">
                        <div class="panel panel-default">
                            .
                            <div class="panel-heading">Employee Birthday</div>
                            <div id="birthday_input" class="input-append panel-body">
                                <input data-format="yyyy-MM-dd" type="text" id="birthday_content" value="<?php echo  date("Y-m-d",strtotime($content['birthday']));?>"></input>
									    <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                            </div>

                            <div class="panel-heading">Employee Personal Email</div>
                            <div class="panel-body" id="personal_email_input">
                                <?php echo $content['personal_email'];?>
                            </div>
                            <div class="panel-heading">Employee Skype</div>
                            <div class="panel-body" id="skype_input">
                                <?php echo $content['skype'];?>
                            </div>

                            <div class="panel-heading">Employee Home Address</div>
                            <div class="panel-body" id="home_address_input">
                                <?php echo $content['home_address'];?>
                            </div>

                            <!--                                <div class="panel-heading">Employee Last Day</div>-->
                            <!--                                <div class="panel-body" id="last_day_input">-->
                            <!--                                    --><?php //echo $content['last_day']?>
                            <!--                                </div>-->

                            <div class="panel-heading">Employee Last Day</div>
                            <div id="last_day_input" class="input-append panel-body">
                                <input data-format="yyyy-MM-dd" type="text" id="last_day_content" value="<?php echo $content['last_day'];?>"></input>
									    <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                            </div>


                        </div>

                        <button type="button" class="btn btn-success">Edit</button>
                        <button type="button" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->





</div><!--/.main-->

<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
        src="/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script>
    $('#start_date_input').datetimepicker({
        pickTime :false
    });

    $('#birthday_input').datetimepicker({
        pickTime :false
    });

    $('#last_day_input').datetimepicker({
        pickTime :false
    });

    $('#probation_date_input').datetimepicker({
        pickTime :false
    });
    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

    $(document).ready(function(){
        $("#tab_detail").css("display","block");
    });


    $(".btn-success").click(function()
    {
        $("#title_input").empty();
        // $("#department_ipnut").empty();
        //  $("#start_date_input").empty();
        $("#phone_number_input").empty();
        $("#mobile_number_input").empty();
        $("#chinese_name_input").empty();
        $("#work_location_input").empty();
        //   $("#birthday_input").empty();
        $("#personal_email_input").empty();
        $("#skype_input").empty();
        $("#home_address_input").empty();
        //  $("#last_day_input").empty();




        $("#title_input").append('<input class="form-control" id="title_content" value="<?php echo  $content['title'];?>">');
        //  $("#department_ipnut").append('<input class="form-control" id="department_content" value="<?php echo  $content['department'];?>">');
        //  $("#start_date_input").append('<input class="form-control" id="start_date_content" value="<?php echo  $content['start_date'];?>">');
        $("#phone_number_input").append('<input class="form-control" id="phone_number_content" value="<?php echo  $content['phone_number'];?>">');
        $("#mobile_number_input").append('<input class="form-control" id="mobile_number_content" value="<?php echo  $content['moblie_number'];?>">');
        $("#chinese_name_input").append('<input class="form-control" id="chinese_name_content" value="<?php echo  $content['chinese_name'];?>">');
        $("#work_location_input").append('<input class="form-control" id="work_location_content" value="<?php echo  $content['work_location'];?>">');
        //     $("#birthday_input").append('<input class="form-control" id="birthday_content" value="<?php echo  $content['birthday'];?>">');
        $("#personal_email_input").append('<input class="form-control" id="personal_email_content" value="<?php echo  $content['personal_email'];?>">');
        $("#skype_input").append('<input class="form-control" id="skype_content" value="<?php echo  $content['skype'];?>">');
        $("#home_address_input").append('<input class="form-control" id="home_address_content" value="<?php echo  $content['home_address'];?>">');
        //    $("#last_day_input").append('<input class="form-control" placeholder="Format: YYYY-MM-DD, do not fill until he/she has already left." id="last_day_content" value="<?php echo  $content['last_day'];?>">');

        $(".btn-primary").attr("onclick","post_to()");
    });

    function post_to()
    {
        $.post("/hr/edit/employee/action",
            {
                id : <?php echo $content['auto_id']?>,
                title : $("#title_content").val(),
                department: $("#department_content").val(),
                start_date : $("#start_date_content").val(),
                probation_date : $("#prbation_date_content").val(),
                phone_number: $("#phone_number_content").val(),
                moblie_number : $("#mobile_number_content").val(),
                chinese_name: $("#chinese_name_content").val(),
                work_location : $("#work_location_content").val(),
                birthday: $("#birthday_content").val(),
                personal_email : $("#personal_email_content").val(),
                skype: $("#skype_content").val(),
                home_address : $("#home_address_content").val(),
                last_day: $("#last_day_content").val(),
                intern : $("#intern_content").val(),
                probation : $("#probation_content").val()
            },
            function(data,status)
            {
                json1 = eval("("+data+")");
                if(json1.code == "200")
                {
                    confirm("Employee profile has been updated");
                    location.href= '/hr';
                }
                else
                {
                    alert(json1.msg);
                }
            });
    }
</script>
</body>

</html>
