<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Leave</title>

<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/datepicker3.css" rel="stylesheet">
<link href="/assets/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
		
	<?php $this->load->view("/widgets/left_nav");?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Leave</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Leave Information</h1>
			</div>
		</div><!--/.row-->
									
		
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table class="table">
						    <thead id ="table_content">
						    <tr>
						        <th data-align="right">Leave ID</th>
						        <th >Apply Time</th>
						        <th >Start Time</th>
						        <th >End Time</th>
						        <th >Time Length</th>
						        <th >State</th>
						        <th >View</th>
						        <th >Pass</th>
						    </tr>
						   <?php foreach($content['data'] as $data) { ?>
						   <tr>
						    	<th><?php echo $data['auto_id'];?></th>
						    	<th><?php echo $data['create_time'];?></th>
						    	<th><?php echo $data['start_time'];?></th>
						    	<th><?php echo $data['end_time'];?></th>
						    	<th><?php echo $data['time_length'];?></th>
						    	<th><?php echo $data['state'];?></th>
						    	<th><?php echo "<button class='btn btn-success' onclick='view_detail(".$data['auto_id'].")'>view</button>";?></th>
						    	<th><button class="btn btn-danger" onclick="pass(<?php echo $data['auto_id'];?>)">Pass</button></th>
						    </tr>
						    <?php } ?>
						    </thead>
						</table>
						
					</div>
				</div>


			</div>
			
			
		</div><!--/.row-->



		
	</div>	<!--/.main-->
		  
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/chart.min.js"></script>
	<script src="/assets/js/easypiechart.js"></script>
	<script src="/assets/js/easypiechart-data.js"></script>
	<script src="/assets/js/bootstrap-datepicker.js"></script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})

		function view_detail(id)
		{

		}

		function pass(id)
		{
			if(confirm("Are you sure to Pass the Apply?"))
			{
				$.post("/leave/pass",
				{
					id : id
				},
				function(data,status)
				{
					json1 = eval("("+data+")");
					if(json1.code == "200")
					{
						confirm("Apply has been passed");
						location.href = '/worker/<?php echo $id;?>';
					}
					else
					{
						alert(json1.text);
					}
				});
			}
			
		}


	</script>	
</body>

</html>
