<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Overtime Records</title>

<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/datepicker3.css" rel="stylesheet">
<link href="/assets/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
		
	<?php $this->load->view("/widgets/left_nav");?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Overtime Records</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Overtime Records</h1>
			</div>
		</div><!--/.row-->



		<div id="loading" class="panel" align="center"><img src="/assets/img/5.gif" alt="" /><br><h3>Waiting For Transmission</h3></div>

		<div class="row" id="main_panel">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table class="table"  id ="table_content">
						    <thead>
						    <tr>
						        <th data-align="right">Employee Name</th>
					
						        <th >Start Time</th>
						        <th >End Time</th>
						        <th >Time Length</th>
						        <th >Pay Or Day</th>
						        <th >Compension</th>
                                <th>Status</th>
						        <th >View</th>
						        <th >Cancel</th>
						    </tr>
						    <?php foreach($content as $data) { ?>
						    <tr>
						    	<th><?php echo $data['user_name'];?></th>
						    	
						    	<th><?php echo $data['start_time'];?></th>
						    	<th><?php echo $data['end_time'];?></th>
						    	<th><?php echo $data['time_length'];?></th>
						    	<th><?php echo $data['pay_or_day'];?></th>
						    	<th><?php echo $data['compension'];?></th>
                                <th><?php echo $data['state'];?></th>
						    	<th><?php echo "<button class='btn btn-success' onclick='view_detail(".$data['auto_id'].")'>view</button>";?></th>
						    	<?php if( $data['state'] == "approved_by_mgr") { ?>
						    	<th><button class="btn btn-danger" onclick="cancel(<?php echo $data['auto_id'];?>)">cancel</button></th>
						    	<?php }else if($data['state'] == "pending") {?>
                                <th><button class="btn btn-danger" onclick="approve(<?php echo $data['auto_id'];?>)">approve</button></th>
                                <?php }?>
						    </tr>
						    <?php } ?>
						    </thead>
						</table>
						<div style="padding-left: 40%">
						<ul class="pagination" id="paginations">
						<li class="page-first"><a onclick="locate(0)">&lt;&lt;</a></li>
						<li class="page-pre"><a onclick="locate(<?php echo ($page-1)<=0?0:($page-1)?>)">&lt;</a></li>
						<?php foreach($pages as $p) {?>
						<li class="page"><a onclick="locate(<?php echo $p?>)"><?php echo (1+$p)?></a></li>
						<?php }?>
						<li class="page-next"><a onclick="locate(<?php echo ($page+1)>=($all-1)?(($all-1)<=0?0:($all-1)):($page+1) ?>)">&gt;</a></li>
						<li class="page-last"><a  onclick="locate(<?php echo (($all-1)<=0?0:($all-1))?>)">&gt;&gt;</a></li>
						</ul>
						</div>

						<button class="btn btn-primary btn-lg" onclick="location.href='/overtime/new'">Add New Overtime</button>
					</div>
				</div>


			</div>
			
			
		</div><!--/.row-->
	</div>	<!--/.main-->
		  

	<script src="/assets/js/jquery-1.11.1.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/chart.min.js"></script>
	<script src="/assets/js/easypiechart.js"></script>
	<script src="/assets/js/easypiechart-data.js"></script>
	<script src="/assets/js/bootstrap-datepicker.js"></script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})


		
		

		function view_detail(id)
		{
			location.href = "/approve/overtime/detail/"+id;
		}
		
		function cancel(id)
		{
			if(confirm("Are you sure to cancel Overtime application?"))
			{
				$("#loading").show();
				$("#main_panel").hide();
				$.post("/approve/overtime/cancel",
				{
					id : id
				},
				function(data,status)
				{
					$("#loading").hide();
					$("#main_panel").show();
					json1 = eval("("+data+")");
					if(json1.code == "200")
					{
						alert(" Overtime application has been cancelled");
						location.href="/approve/overtime";
					}
					else
					{
						alert(json1.text);
					}
				});
			}
			
		}

		function approve(id)
        {
            if(confirm("Are you sure to approve Overtime application?"))
            {
				$("#loading").show();
				$("#main_panel").hide();
                $.post("/approve/overtime/pass",
                    {
                        id : id
                    },
                    function(data,status)
                    {
						$("#loading").hide();
						$("#main_panel").show();
                        json1 = eval("("+data+")");
                        if(json1.code == "200")
                        {
                            alert(" Overtime application has been Approved");
                            location.href="/approve/overtime";
                        }
                        else
                        {
                            alert(json1.text);
                        }
                    });
            }
        }
		function locate(id)
		{
			$.post("/api/approve/overtime/month",
			{
				page : id
			},
			function(data,status)
			{
				json1 = eval("("+data+")");
				$("#paginations").empty();
				$("#table_content").empty();
				$("#table_content").append('<thead><tr><th data-align="right">Employee Name</th><th >Start Time</th><th >End Time</th><th >Time Length</th><th>Pay Or Day</th><th>Compension</th><th >Status</th><th >View</th><th >Action</th></tr>');
				//alert(json1.content[0].user_name);return;
				for(i =0;i<json1.content.length;i++)
				{
					var contents = '<tr><th>'+json1.content[i].user_name+'</th><th>'+json1.content[i].start_time+'</th><th>'+json1.content[i].end_time+'</th><th>'+json1.content[i].time_length+'</th><th>'+json1.content[i].pay_or_day+'</th><th>'+json1.content[i].compension+'</th><th>'+json1.content[i].state+'</th><th><button class="btn btn-success" onclick="view_detail('+json1.content[i].auto_id+')">view</button></th>';

					if(json1.content[i].state == "pending")
					{
						$("#table_content").append(contents+'<th><button class="btn btn-danger" onclick="pass('+json1.content[i].auto_id+')">Approve</button></th></tr>');
					}
					else if(json1.content[i].state == "approved_by_mgr")
					{
						$("#table_content").append(contents+'<th><button class="btn btn-danger" onclick="cancel('+json1.content[i].auto_id+')">Cancel</button></th></tr>');
					}
					else
					{
						$("#table_content").append(contents);
					}
				}
				$("#paginations").append('<li class="page-first"><a onclick="locate(0)">&lt;&lt;</a></li><li class="page-pre"><a onclick="locate('+((json1.page-1)<=0 ? 0 : (json1.page-1))+')">&lt;</a></li>');
				for(i =0;i<json1.pages.length;i++)
				{
					$("#paginations").append('<li class="page"><a onclick="locate('+json1.pages[i]+')"</a>'+(1+json1.pages[i])+'</li>');
				}
				var p = (1+parseInt(json1.page)) >=(json1.all-1) ? ((json1.all-1)>=0?(json1.all-1):0) : (1+parseInt(json1.page)) ;
				$("#paginations").append('<li class="page-next"><a onclick="locate('+p+')">&gt;</a></li><li class="page-last"><a  onclick="locate('+((json1.all-1)>=0?(json1.all-1):0)+')">&gt;&gt;</a></li>');

			});
		}
	</script>	
</body>

</html>
