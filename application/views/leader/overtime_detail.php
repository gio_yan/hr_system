<!DOCTYPE HTML>
<html>
  <head>
    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
     href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="/assets/css/datepicker3.css" rel="stylesheet">
	<link href="/assets/css/styles.css" rel="stylesheet">
	<title>Overtime Detail</title>
  </head>
  <body>

  <?php $this->load->view("/widgets/head_nav");?>
	<?php $this->load->view("/widgets/left_nav");?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="/approve/overtime"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="">Overtime Detail</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
			</div>
		</div><!--/.row-->

		<div id="loading" class="panel" align="center"><img src="/assets/img/5.gif" alt="" /><br><h3>Waiting For Transmission</h3></div>

		<div class="row" id="main_panel">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"></div>
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form">
								<div class="form-group">
									<label>Employee</label>
									<select class="form-control" id="employee_name">
										
										<option value="<?php echo $content['user_id'];?>" ><?php echo $content['user_name'];?></option>
									
									</select>
								</div>
								
																
								<div class="panel panel-default">
								  <div class="panel-heading">Start Time</div>
								  <div class="panel-body" id="title_panel">
								    
								    <div id="datetimepicker_date_start" class="input-append">
									    <input data-format="yyyy-MM-dd" type="text" id="start_date" value="<?php echo date("Y-m-d",strtotime($content['start_time']));?>" readonly></input>

									  </div>
									  <div id="datetimepicker_time_start" class="input-append">
									    <input data-format="hh:mm:ss" type="text" id="start_time"  value="<?php echo date("H:i:s",strtotime($content['start_time']));?>" readonly></input>

									  </div>
									
								  </div>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">End Time</div>
								  <div class="panel-body" id="title_panel">
								    
								    <div id="datetimepicker_date_end" class="input-append">
									    <input data-format="yyyy-MM-dd" type="text" id="end_date" value="<?php echo date("Y-m-d",strtotime($content['end_time']));?>" readonly></input>

									  </div>
									  <div id="datetimepicker_time_end" class="input-append">
									    <input data-format="hh:mm:ss" type="text" id="end_time"  value="<?php echo date("H:i:s",strtotime($content['end_time']));?>" readonly></input>

									  </div>
									
								  </div>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">Pay OR Day</div>
								  <div class="form-group">
									
									<div class="radio">
										<label>
											<input type="radio" name="p-d" id="pay-pd" value="pay">Pay
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="p-d" id="day-pd" value="day">Day
										</label>
									</div>
									
								  </div>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">Compensation</div>
								  <div class="form-group">
                                      <div class="radio">
                                          <label>
                                              <input type="radio" name="compensation" id="1-d" value="1">1 day leave
                                          </label>
                                      </div>


									<div class="radio">
										<label>
											<input type="radio" name="compensation" id="4-d" value="4" >1.5 days leave (After normal working hour, normal weekday)
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="compensation" id="2-d" value="2">2 days leave (Weekend)
										</label>
									</div>

									<div class="radio">
										<label>
											<input type="radio" name="compensation" id="3-d" value="3">3 days leave (National Holiday)
										</label>
									</div>
									
								  </div>
								</div>
								

								<div class="panel panel-default">
								  <div class="panel-heading">Time Length</div>
								  <input class = "form-control" id="time_length" value="<?php echo $content['time_length']?>" readonly/>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">Reason</div>
								  <textarea class="form-group" style="width: 100%" id="reason" readonly><?php echo $content['reason'];?></textarea>
								</div>
								<?php if($content['state'] == "pending" ) { ?>
                                    <button type="button" class="btn btn-success btn-lg" onclick="approve()">Approve</button>
									<button type="button" class="btn btn-danger btn-lg" onclick="cancel()">Decline</button>
								<?php } else {?>
								<button type="button" class="btn btn-danger btn-lg" onclick="cancel()">Cancel</button>
								<?php } ?>
								</div>
		
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->
    
    <script type="text/javascript"
     src="/assets/js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    
    
    <script type="text/javascript">
      $('#datetimepicker_time_start').datetimepicker({
        pickDate :false
      });

      $('#datetimepicker_date_start').datetimepicker({
        pickTime :false
      });

      $('#datetimepicker_time_end').datetimepicker({
        pickDate :false
      });

      $('#datetimepicker_date_end').datetimepicker({
        pickTime :false
      });


      !function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})

	$(document).ready(function(){
        var name ="<?php echo $content['compension']?>".substr(0,2);
        //alert(name.substr(0,1));return;
        $("#<?php echo $content['pay_or_day']?>"+"-pd").attr("checked","checked");
        if(name.substr(1,1) == '.' && name.substr(0,1) == '1')
        {
            $("#4-d").attr("checked","checked");
        }
        else
        {
            name = name.substr(0,1);
        }
        $("#"+name+"-d").attr("checked","checked");
	});

//		function cancel()
//		{
//			if(confirm("Are you sure to Cancel the Overtime application?"))
//			{
//				$.post("/approve/overtime/cancel",
//				{
//					id : <?php //echo $id;?>
//				},
//				function(data,status)
//				{
//					json1 = eval("("+data+")");
//					if(json1.code == "200")
//					{
//						confirm("application has been cancelled");
//						location.href = '/approve/overtime';
//					}
//					else
//					{
//						alert(json1.text);
//					}
//				});
//			}
//		}

      function cancel()
      {
          if(confirm("Are you sure to cancel Overtime application?"))
          {
			  $("#loading").show();
			  $("#main_panel").hide();
              $.post("/approve/overtime/cancel",
                  {
                      id : <?php echo $id;?>
                  },
                  function(data,status)
                  {
					  $("#loading").hide();
					  $("#main_panel").show();
                      json1 = eval("("+data+")");
                      if(json1.code == "200")
                      {
                          alert("Your Overtime application has been Cancelld");
                          location.href="/approve/overtime";
                      }
                      else
                      {
                          alert(json1.text);
                      }
                  });
          }

      }

      function approve()
      {
          if(confirm("Are you sure to approve Overtime application?"))
          {
			  $("#loading").show();
			  $("#main_panel").hide();
              $.post("/approve/overtime/pass",
                  {
                      id : <?php echo $id;?>
                  },
                  function(data,status)
                  {
					  $("#loading").hide();
					  $("#main_panel").show();
                      json1 = eval("("+data+")");
                      if(json1.code == "200")
                      {
                          alert("Your Overtime application has been Approved");
                          location.href="/approve/overtime";
                      }
                      else
                      {
                          alert(json1.text);
                      }
                  });
          }
      }


    </script>
  </body>
</html>




