<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Detail</title>

    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">Official Transfer Detail</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Official Transfer Detail</h1>
            <h3 class="page-header"></h3>
        </div>
    </div><!--/.row-->


    <div id="loading" class="panel" align="center"><img src="/assets/img/5.gif" alt="" /><br><h3>Waiting For Transmission</h3></div>

    <div class="row" id="main_panel">
        <div class="col-mg-12 "  id="#tab_user">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <form role="form">

                            <div class="panel panel-default">

                                <div class="panel-heading"><font color="red">Employee Name*</font></div>
                                <div class="panel-body" id="name_input">
                                    <?php echo $content['name'];?>
                                </div>
                                <div class="panel-heading">Employee On Board Date</div>
                                <div id="start_date_input" class="input-append panel-body">
                                    <input data-format="yyyy-MM-dd" type="text" id="start_date_content" value="<?php echo $content['start_date'];?>"></input>
                                        <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                                </div>

                                <div class="panel-heading"><font color="red">Is Probation?*</font></div>
                                <div class="panel-body" id="probation_input">
                                    <select class="form-control" id="probation_content">
                                        <option value="<?php echo $probations[0]?>" ><?php echo $probations[0]?></option>
                                        <option value="<?php echo $probations[1]?>" ><?php echo $probations[1]?></option>
                                    </select>
                                </div>

                            </div>


                            <!--                            <button type="button" class="btn btn-success">Edit</button>-->
                            <!--                            <button type="button" class="btn btn-primary">Save</button>-->
                        </form>
                    </div>

                    <div class="col-md-6">
                        <form role="form">

                            <div class="panel panel-default">





                                <div class="panel-heading">Proabtion Date</div>
                                <div id="birthday_input" class="input-append panel-body">
                                    <input data-format="yyyy-MM-dd" type="text" id="birthday_input_content" value="<?php echo $content['probation_date'];?>"></input>
                                        <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                                </div>

                                <div class="panel-heading"><font color="red">Is Intern?*</font></div>
                                <div class="panel-body" id="intern_input">
                                    <select class="form-control" id="intern_content">
                                        <option value="<?php echo $interns[0]?>" ><?php echo $interns[0]?></option>
                                        <option value="<?php echo $interns[1]?>" ><?php echo $interns[1]?></option>
                                    </select>
                                </div>

                            </div>


                            <button type="button" class="btn btn-primary btn-big" onclick="post_to()">Save</button>


                        </form>
                    </div>



                    </div>
                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->





</div><!--/.main-->

<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
        src="/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script>
    $('#start_date_input').datetimepicker({
        pickTime :false
    });

    $('#birthday_input').datetimepicker({
        pickTime :false
    });
    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

    $(document).ready(function(){
        $("#tab_detail").css("display","block");
    });


    $(".btn-success").click(function()
    {
        $(".btn-primary").attr("onclick","post_to()");
    });

    function post_to()
    {
        $("#loading").show();
        $("#main_panel").hide();
        $.post("/approve/official/edit",
            {
                id : <?php echo $content['auto_id']?>,
                probation_date : $("#prbation_date_content").val(),
                intern : $("#intern_content").val(),
                probation : $("#probation_content").val()
            },
            function(data,status)
            {
                $("#loading").hide();
                $("#main_panel").show();
                json1 = eval("("+data+")");
                if(json1.code == "200")
                {
                    confirm("Employee profile has been updated");
                    location.href= '/approve/official';
                }
                else
                {
                    alert(json1.msg);
                }
            });
    }
</script>
</body>

</html>
