<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>General</title>

    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/hr"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">hr</li>
        </ol>
    </div><!--/.row-->




    <div class="row">
        <div class="col-mg-12 "  id="#tab_user">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="col-md-4">

                        Year:<select id="date_year" class="form-control">
                            <?php  for($i=0;$i>=-5;$i--) {?>
                                <option value="<?php echo date("Y")+$i;?>"><?php echo date("Y")+$i;?></option>
                            <?php } ?>
                        </select>
                        Month:<select id="date_month" class="form-control">
                            <?php  for($i=1;$i<=12;$i++) {?>
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php } ?>
                        </select>
                        <!--                        <div id="start_date_input" class="input-append panel-body">-->
                        <!--                            <input data-format="yyyy-MM" type="text" id="start_date_content"value="--><?php //echo date("Y-m");?><!--"></input>-->
                        <!--                            <span class="add-on"  style="padding: 1px 20px">-->
                        <!--									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">-->
                        <!--                                          </i>-->
                        <!--									    </span>-->
                        <!--                        </div>-->
                    </div>



                    <div class="col-md-4">

                        <div id="" class="input-append panel-body">
                            <input type="button" class="btn btn-success" value="Search" onclick="generate()">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <form role="form">
                            <table class="table" cellpadding="2" cellspacing="2" id="table_content">
                                <tr>
                                    <th colspan='7' id="year_month">
                                    </th>
                                </tr>
                                <tr>
                                    <th>Sun</th>
                                    <th>Mon</th>
                                    <th>Tue</th>
                                    <th>Wed</th>
                                    <th>Thur</th>
                                    <th>Fri</th>
                                    <th>Sat</th>
                                </tr>
                            </table>



                        </form>


                    </div>


                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->





</div><!--/.main-->

<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>

<script>

    var months = new Array("Janu");

    var departs = null;

    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })


    function generate()
    {
        month = $("#date_month").val() ;
        month = month < 10 ? "0"+month : month;
        $.post("/approve/general/data",
            {
                month : $("#date_year").val()+"-"+month
            },
            function(data,status)
            {
                $("#table_content").empty();
                $("#table_content").append("<tr><th colspan='7'>"+$("#date_year").val()+"-"+$("#date_month").val()+"</th></tr><tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thur</th><th>Fri</th><th>Sat</th></tr>");
                json1 = eval("("+data+")");
                for(i=0;i<json1.length;i++)
                {
                    var content = "";
                    for(j=0;j<7;j++)
                    {
                        content = content+"<td align='center'>"+json1[i][j]+"</td>";
                    }
                    $("#table_content").append("<tr>"+content+"</tr>");
                    content = "";
                }
            });
    }

</script>
</body>

</html>
