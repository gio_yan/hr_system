<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Raw Data Report</title>

    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/hr"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">hr</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Team Member Balance</h1>
        </div>
    </div><!--/.row-->



    <div class="row">
        <div class="col-mg-12 "  id="#tab_user">
            <div class="panel panel-default">
                <div class="panel-body">


                    <div class="col-md-12">
                        <form role="form">
                            <table class="table" id="table_content">
                                <tr>
                                    <td>Name </td>
                                    <td>Annual Leave </td>
                                    <td>Sick Leave </td>
                                    <td>To be Expired </td>
                                </tr><?php //var_dump($content);exit;?>
                                <?php foreach ($content as $con) { ?>
                                    <tr>
                                        <td><?php echo $con['user_name'];?></td>
                                        <td><?php echo $con['leave_days'];?></td>
                                        <td><?php echo $con['sick_days'];?></td>
                                        <td><?php echo $con['leave_overdue']['num']."d in ".$con['leave_overdue']['day']." days";?></td>
                                    </tr>
                                <?php }?>

                            </table>



                        </form>


                    </div>


                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->





</div><!--/.main-->

<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
        src="/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script>
    var departs = null;
    $('#start_date_input').datetimepicker({
        pickTime :false
    });

    $('#end_date_input').datetimepicker({
        pickTime :false
    });
    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

    function generate()
    {
        $.post("/hr/raw/data",
            {
                start : $("#start_date_content").val(),
                end   : $("#end_date_content").val(),
                id : $("#employee_name").val()
            },
            function(data,status)
            {

                $("#table_content").empty();
                $("#table_content").append("<tr><td>Record Date</td> <td>Check In</td> <td>Check Out</td></tr>");
                json1 = eval("("+data+")");
                if(json1.code=="101")
                {
                    alert(json1.msg);
                }
                for(i=0;i<json1.length;i++)
                {
                    in_flag = out_flag = 'white';
                    if(json1[i].check_in == '--')
                    {
                        json1[i].check_in = '';
                        in_flag = 'red';
                    }
                    if(json1[i].check_out == '--')
                    {
                        json1[i].check_out = '';
                        out_flag = 'red';
                    }
                    $("#table_content").append("<tr><td>"+json1[i].record_date+"</td><td style='background-color:"+in_flag+"'>"+json1[i].check_in+"</td><td style='background-color:"+out_flag+"'>"+json1[i].check_out+"</td></tr>");
                }
            });
    }
</script>
</body>

</html>
