<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Leave</title>

<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/datepicker3.css" rel="stylesheet">
<link href="/assets/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
		
	<?php $this->load->view("/widgets/left_nav");?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Approve</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">Pending Requests</h3>
			</div>
		</div><!--/.row-->
        
		
		<div class="row">

            <a href="/approve/raw">
                <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/approve/raw'">
                    <div class="panel panel-teal panel-widget">
                        <div class="row no-padding">
                            <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                                <em class="glyphicon glyphicon-user glyphicon-l"></em>
                            </div>
                            <div class="col-sm-9 col-lg-7 widget-right">
                                <div class="large">Raw Data </div>

                            </div>
                        </div>
                    </div>
                </div>
            </a>


            
			<a href="/approve/general">
            <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/approve/general'">
                <div class="panel panel-teal panel-widget">
                    <div class="row no-padding">
                        <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                            <em class="glyphicon glyphicon-user glyphicon-l"></em>
                        </div>
                        <div class="col-sm-9 col-lg-7 widget-right">
                            <div class="large">Calendar View</div>
                            <div class="text-muted"></div>
                        </div>
                    </div>
                </div>
            </div>
			</a>

			<a href="/approve/leave">
			<div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/approve/leave'">
				<div class="panel panel-red panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
							<div class="large"><?php echo $leave['num']?></div>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">Leave</div>

						</div>
					</div>
				</div>
			</div>
			</a>

			<a href="/approve/sick">
			<div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/approve/sick'">
				<div class="panel panel-teal panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
							<div class="large"><?php echo $sick['num']?></div>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">Sick </div>

						</div>
					</div>
				</div>
			</div>
			</a>

			<a href="/approve/unpaid">
			<div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/approve/unpaid'">
				<div class="panel panel-teal panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
							<div class="large"><?php echo $unpaid['num']?></div>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">Unpaid </div>

						</div>
					</div>
				</div>
			</div>
			</a>

			<a href="/approve/overtime">
			<div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/approve/overtime'">
				<div class="panel panel-teal panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
							<div class="large"><?php echo $overtime['num']?></div>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">Overtime </div>

						</div>
					</div>
				</div>
			</div>
			</a>



			<a href="/approve/allowance">
				<div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/approve/mc'">
					<div class="panel panel-teal panel-widget">
						<div class="row no-padding">
							<div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
								<div class="large"><?php echo $allowance['num']//count all unfinished?></div>
							</div>
							<div class="col-sm-9 col-lg-7 widget-right">
								<div class="large">Allowance </div>

							</div>
						</div>
					</div>
				</div>
			</a>

			<a href="/approve/official">
				<div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/approve/official'">
					<div class="panel panel-teal panel-widget">
						<div class="row no-padding">
							<div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
								<em class="glyphicon glyphicon-user glyphicon-l"></em>
							</div>
							<div class="col-sm-9 col-lg-7 widget-right">
								<div class="large">Official Transfer </div>

							</div>
						</div>
					</div>
				</div>
			</a>

            <a href="/approve/balance">
                <div class="col-xs-12 col-md-6 col-lg-4"  onclick="location.href='/approve/balance'">
                    <div class="panel panel-teal panel-widget">
                        <div class="row no-padding">
                            <div class="col-sm-3 col-lg-5 widget-left" style="background-color: #30a4ff">
                                <em class="glyphicon glyphicon-user glyphicon-l"></em>
                            </div>
                            <div class="col-sm-9 col-lg-7 widget-right">
                                <div class="large">Balance View </div>

                            </div>
                        </div>
                    </div>
                </div>
            </a>


		</div><!--/.row-->
		
		
		
		</div><!--/.row-->
	</div>	<!--/.main-->
		  
	<script src="/assets/js/jquery-1.11.1.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/chart.min.js"></script>
	<script src="/assets/js/easypiechart.js"></script>
	<script src="/assets/js/easypiechart-data.js"></script>
	<script src="/assets/js/bootstrap-datepicker.js"></script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})

		
	</script>	
</body>

</html>
