<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> Unpaid Records</title>

<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/datepicker3.css" rel="stylesheet">
<link href="/assets/css/styles.css" rel="stylesheet">
<title>Approve Unpaid Leave</title>
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
		
	<?php $this->load->view("/widgets/left_nav");?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="/approve/unpaid"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active"> Unpaid Leave Records</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Unpaid Leave Records</h1>
			</div>
		</div><!--/.row-->

		<div id="loading" class="panel" align="center"><img src="/assets/img/5.gif" alt="" /><br><h3>Waiting For Transmission</h3></div>

		<div class="row" id="main_panel">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table class="table" id ="table_content">
						    <thead >
						    <tr>
						        <th data-align="right">Employee Name</th>
						        <th >Start Time</th>
						        <th >End Time</th>
						        <th >Time Length</th>
						        <th >Status</th>
						        <th >Type</th>
						        <th >View</th>
						        <th >Action</th>
						    </tr>
						   <?php foreach($content as $data) { ?>
						   <tr>
						    	<th><?php echo $data['user_name'];?></th>
						    	<th><?php echo $data['start_time'];?></th>
						    	<th><?php echo $data['end_time'];?></th>
						    	<th><?php echo $data['time_length'];?></th>
						    	<th><?php echo $data['state'];?></th>
						    	<th><?php echo $data['leave_type'];?></th>
						    	<th><?php echo "<button class='btn btn-success' onclick='viewDetail(".$data['auto_id'].")'>view</button>";?></th>
						    	<?php if($data['state'] == 'pending') { ?>
						    	<th><button class="btn btn-danger" onclick="pass(<?php echo $data['auto_id'];?>)">Approve</button></th>
						    	<?php } else if($data['state'] == 'approved_by_mgr') { ?>
						    	<th><button class="btn btn-danger" onclick="cancel(<?php echo $data['auto_id'];?>)">Cancel</button></th>
						    	<?php } else {} ?>
						    </tr>
						    <?php } ?>
						    </thead>
						</table>
						<div style="padding-left: 40%">
						<ul class="pagination" id="paginations">
						<li class="page-first"><a onclick="locate(0)">&lt;&lt;</a></li>
						<li class="page-pre"><a onclick="locate(<?php echo ($page-1)<=0?0:($page-1)?>)">&lt;</a></li>
						<?php foreach($pages as $p) {?>
						<li class="page"><a onclick="locate(<?php echo $p?>)"><?php echo (1+$p)?></a></li>
						<?php }?>
						<li class="page-next"><a onclick="locate(<?php echo ($page+1)>=($all-1)?(($all-1)<=0?0:($all-1)):($page+1) ?>)">&gt;</a></li>
						<li class="page-last"><a  onclick="locate(<?php echo (($all-1)<=0?0:($all-1))?>)">&gt;&gt;</a></li>
						</ul>
						</div>
					</div>
				</div>


			</div>
			
			
		</div><!--/.row-->



		
	</div>	<!--/.main-->
     
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/chart.min.js"></script>
	<script src="/assets/js/easypiechart.js"></script>
	<script src="/assets/js/easypiechart-data.js"></script>
	<script src="/assets/js/bootstrap-datepicker.js"></script>
	<script>
		

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		});

		function viewDetail(id)
		{
			location.href = "/approve/unpaid/detail/"+id;
		}

		function pass(id)
		{
			if(confirm("Are you sure to Pass the application?"))
			{
				$("#loading").show();
				$("#main_panel").hide();
				$.post("/approve/unpaid/pass",
				{
					id : id
				},
				function(data,status)
				{
					$("#loading").hide();
					$("#main_panel").show();
					json1 = eval("("+data+")");
					if(json1.code == "200")
					{
						confirm("Application has been passed");
						location.href = '/approve/unpaid';
					}
					else
					{
						alert(json1.text);
					}
				});
			}
			
		}

		function cancel(id)
		{
			if(confirm("Are you sure to Cancel the approved application?"))
			{
				$("#loading").show();
				$("#main_panel").hide();
				$.post("/approve/unpaid/cancel",
				{
					id : id
				},
				function(data,status)
				{
					$("#loading").hide();
					$("#main_panel").show();
					json1 = eval("("+data+")");
					if(json1.code == "200")
					{
						confirm("Application has been cancelled");
						location.href = '/approve/unpaid';
					}
					else
					{
						alert(json1.text);
					}
				});
			}
		}

		function locate(id)
		{
			$.post("/api/approve/unpaid/month",
			{
				page : id
			},
			function(data,status)
			{
				json1 = eval("("+data+")");
				$("#paginations").empty();
				$("#table_content").empty();
				$("#table_content").append('<thead><tr><th data-align="right">Employee Name</th><th >Start Time</th><th >End Time</th><th >Time Length</th><th >Status</th><th >View</th><th >Action</th></tr>');
				//alert(json1.content[0].user_name);return;
				for(i =0;i<json1.content.length;i++)
				{
					var contents = '<tr><th>'+json1.content[i].user_name+'</th><th>'+json1.content[i].start_time+'</th><th>'+json1.content[i].end_time+'</th><th>'+json1.content[i].time_length+'</th><th>'+json1.content[i].state+'</th><th><button class="btn btn-success" onclick="viewDetail('+json1.content[i].auto_id+')">view</button></th>';

					if(json1.content[i].state == "pending")
					{
						$("#table_content").append(contents+'<th><button class="btn btn-danger" onclick="pass('+json1.content[i].auto_id+')">Approve</button></th></tr>');
					}
					else if(json1.content[i].state == "approved_by_mgr")
					{
						$("#table_content").append(contents+'<th><button class="btn btn-danger" onclick="cancel('+json1.content[i].auto_id+')">Cancel</button></th></tr>');
					}
					else
					{
						$("#table_content").append(contents);
					}
				}
				$("#paginations").append('<li class="page-first"><a onclick="locate(0)">&lt;&lt;</a></li><li class="page-pre"><a onclick="locate('+((json1.page-1)<=0 ? 0 : (json1.page-1))+')">&lt;</a></li>');
				for(i =0;i<json1.pages.length;i++)
				{
					$("#paginations").append('<li class="page"><a onclick="locate('+json1.pages[i]+')"</a>'+(1+json1.pages[i])+'</li>');
				}
				var p = (1+parseInt(json1.page)) >=(json1.all-1) ? ((json1.all-1)>=0?(json1.all-1):0) : (1+parseInt(json1.page)) ;
				$("#paginations").append('<li class="page-next"><a onclick="locate('+p+')">&gt;</a></li><li class="page-last"><a  onclick="locate('+((json1.all-1)>=0?(json1.all-1):0)+')">&gt;&gt;</a></li>');

			});
		}
	</script>	
</body>

</html>
