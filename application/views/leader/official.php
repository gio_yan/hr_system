<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Official</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>

<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active">Official</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Official Transfer</h1>
        </div>
    </div><!--/.row-->



    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table"  id ="table_content">
                        <thead>
                        <tr>
                            <th data-align="right">Employee Name</th>
                            <th >On Board Date</th>
                            <th >Probation Date</th>
                            <th >View</th>
                        </tr>
                        </thead>
                        <?php foreach($content as $data) { ?>
                            <tr>
                                <td><?php echo $data['user_name'];?></td>

                                <td><?php echo $data['start_date'];?></td>
                                <td><?php echo $data['probation_date'];?></td>

                                <td><?php echo "<button class='btn btn-success' onclick='view_detail(".$data['auto_id'].")'>View</button>";?></td>

                            </tr>
                        <?php } ?>

                    </table>

                </div>
            </div>


        </div>


    </div><!--/.row-->
</div>	<!--/.main-->


<script src="/assets/js/jquery-1.11.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/chart.min.js"></script>
<script src="/assets/js/easypiechart.js"></script>
<script src="/assets/js/easypiechart-data.js"></script>
<script src="/assets/js/bootstrap-datepicker.js"></script>
<script>
    $('#calendar').datepicker({
    });

    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })





    function view_detail(id)
    {
        location.href = "/approve/official/detail/"+id;
    }


    
</script>
</body>

</html>
