<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Unpaid Leave</title>

<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/datepicker3.css" rel="stylesheet">
<link href="/assets/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<?php $this->load->view("/widgets/head_nav");?>
		
	<?php $this->load->view("/widgets/left_nav");?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">		
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="/"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Unpaid Leave</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Unpaid Leave Information</h1>
			</div>
		</div><!--/.row-->

		<div id="loading" class="panel" align="center"><img src="/assets/img/5.gif" alt="" /><br><h3>Waiting For Transmission</h3></div>
		
		<div class="row" id="main_panel">
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<table class="table">
						    <thead id ="table_content">
						    <tr>
						        <th >application Time</th>
						        <th >Start Time</th>
						        <th >End Time</th>
						        <th >Time Length</th>
						        <th >Leave Type</th>
						        <th >State</th>
						        <th >View</th>
						        <th >Cancel</th>
						    </tr>
						    <?php foreach($leave_data as $data) { ?>
						    <tr>
						    	<th><?php echo $data['create_time'];?></th>
						    	<th><?php echo $data['start_time'];?></th>
						    	<th><?php echo $data['end_time'];?></th>
						    	<th><?php echo $data['time_length'];?></th>
						    	<th><?php echo $data['leave_type'];?></th>
						    	<th><?php echo $data['state'];?></th>
						    	<th><?php echo "<button class='btn btn-success' onclick='view_detail(".$data['auto_id'].")'>view</button>";?></th>
						    	<?php if($data['state'] == "pending") { ?>
						    	<th><button class="btn btn-danger" onclick="cancel(<?php echo $data['auto_id'].",'".$data['leave_type']."'";?>)">cancel</button></th>
						    	<?php } ?>
						    </tr>
						    <?php } ?>
						    </thead>
						</table>
						<button class="btn btn-primary btn-lg" onclick="location.href='unpaid/new'">Add New Leave application</button>
					</div>
				</div>


			</div>
			
			<div class="col-md-4">
			
				<div class="panel panel-grey">
					<div class="panel-heading dark-overlay"><span class="glyphicon glyphicon-calendar"></span>Calendar</div>
					<div class="panel-body">
						<div id="calendar"></div>
					</div>
				</div>
				
				
								
			</div><!--/.col-->
		</div><!--/.row-->
	</div>	<!--/.main-->
		  

	<script src="/assets/js/jquery-1.11.1.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/chart.min.js"></script>
	<script src="/assets/js/easypiechart.js"></script>
	<script src="/assets/js/easypiechart-data.js"></script>
	<script src="/assets/js/bootstrap-datepicker.js"></script>
	<script>
		$('#calendar').datepicker({
		});
		$(".datepicker-days").empty();
		$(".datepicker-months").show();
		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})


		
		function do_it(month)
		{
			//alert(month);
			//alert( $(".datepicker-switch").first().text());return;

			$("#table_content").empty();
			$("#table_content").append("<tr><th >application Time</th><th >Start Time</th><th >End Time</th><th >Time Length</th><th >State</th><th>View</th><th>Cancel</th></tr>");
			var day = (month+1);
			day = day < 10 ? "0"+day : day;
			$.post("/api/unpaid/month",
			{
				date : $(".datepicker-switch").first().text()+"-"+day+"-01"
			},
			function(data,status)
			{
				$(".datepicker-months").show();
				json1 = eval("("+data+")");
				for(i=0 ;i<json1.length;i++)
				{
					if(json1[i].state == "pending")
					{
						$("#table_content").append("<tr><th>"+json1[i].create_time+"</th><th>"+json1[i].start_time+"</th><th>"+json1[i].end_time+"</th><th>"+json1[i].time_length+"</th><th>"+json1[i].state+"</th><th><button class='btn btn-success' onclick='view_detail("+json1[i].auto_id+")'>view</button></th><th><button class='btn btn-danger' onclick='cancel("+json1[i].auto_id+")'>cancel</button></th></tr>");
					}
					else
					{
						$("#table_content").append("<tr><th>"+json1[i].create_time+"</th><th>"+json1[i].start_time+"</th><th>"+json1[i].end_time+"</th><th>"+json1[i].time_length+"</th><th>"+json1[i].state+"</th><th><button class='btn btn-success' onclick='view_detail("+json1[i].auto_id+")'>view</button></th></tr>");
					}
				}
			});

		}

		function setVal(month)
		{
			setTimeout("do_it("+month+")","100");

		}

		function view_detail(id)
		{
			location.href = "/unpaid/detail/"+id;
		}
		
		function cancel(id,leave_type)
		{
			if(confirm("Are you sure to cancel Leave application?"))
			{
				$("#loading").show();
				$("#main_panel").hide();
				$.post("/unpaid/cancel",
				{
					id : id,
					leave_type : "leave"
				},
				function(data,status)
				{
					$("#loading").hide();
					$("#main_panel").show();
					json1 = eval("("+data+")");
					if(json1.code == "200")
					{
						alert("Your Leave application has been Cancelld");
						location.href="/unpaid";
					}
					else
					{
						alert(json1.text);
					}
				});
			}
			
		}
	</script>	
</body>

</html>
