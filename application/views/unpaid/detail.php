<!DOCTYPE HTML>
<html>
<head>
	<link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="screen"
		  href="/assets/css/bootstrap-datetimepicker.min.css">
	<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="/assets/css/datepicker3.css" rel="stylesheet">
	<link href="/assets/css/styles.css" rel="stylesheet">
	<title>Unpaid Leave Detail</title>
</head>
<body>

<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="/unpaid"><span class="glyphicon glyphicon-home"></span></a></li>
			<li class="">Unpaid Detail</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Unpaid Detail</h1>

		</div>
	</div><!--/.row-->
	<div id="loading" class="panel" align="center"><img src="/assets/img/5.gif" alt="" /><br><h3>Waiting For Transmission</h3></div>

	<div class="row" id="main_panel">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"></div>
				<div class="panel-body">
					<div class="col-md-6">
						<form role="form">
							<div class="panel panel-default">
								<div class="panel-heading">Start Time</div>
								<div class="panel-body" id="title_panel">

									<div id="datetimepicker_date_start" class="input-append">
										<input data-format="yyyy-MM-dd" type="text" id="start_date" value="<?php echo date("Y-m-d",strtotime($content['start_time']));?>"></input>
									    <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
										  </i>
									    </span>
									</div>
									<div id="datetimepicker_time_start" class="input-append">
										<input data-format="hh:mm" type="text" id="start_time"  value="<?php echo date("H:i:s",strtotime($content['start_time']));?>"></input>
									    <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
										  </i>
									    </span>
									</div>

								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading">End Time</div>
								<div class="panel-body" id="title_panel">

									<div id="datetimepicker_date_end" class="input-append">
										<input data-format="yyyy-MM-dd" type="text" id="end_date" value="<?php echo date("Y-m-d",strtotime($content['end_time']));?>"></input>
									    <span class="add-on" style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
										  </i>
									    </span>
									</div>
									<div id="datetimepicker_time_end" class="input-append">
										<input data-format="hh:mm" type="text" id="end_time"  value="<?php echo date("H:i:s",strtotime($content['end_time']));?>"></input>
									    <span class="add-on" style="padding: 1px 20px">
									      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
										  </i>
									    </span>
									</div>

								</div>
							</div>


							<div class="panel panel-default">
								<div class="panel-heading">Time Length</div>
								<div class="input-group">
									<input class = "form-control" id="time_length" value="<?php echo $content['time_length'];?>" readonly/>
									<span class="input-group-addon" style="color: #fff;background-color: #30a5ff;" onclick="getToalDays()">Get Total Days</span>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading">Reason</div>
								<textarea class="form-group" style="width: 100%" id="reason"><?php echo $content['reason'];?></textarea>
							</div>

							<div class="panel panel-default" id = 'doctor_notes_visible' hidden>
								<div class="form-group">
									<div class="panel-heading">With Doctor Notes ?</div>
									<select class="form-control" id="doctor_notes">
										<option value="no">No</option>
										<option value="yes">Yes</option>
									</select>
								</div>
							</div>

							<?php if($content['state'] == "pending") {?>
								<button type="button" class="btn btn-primary btn-lg" onclick="edit()">Submit</button>
							<?php } ?>

					</div>

					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->

</div><!--/.main-->


<script type="text/javascript"
		src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
		src="/assets/js/bootstrap-datetimepicker.min.js">
</script>


<script type="text/javascript">
	$('#datetimepicker_time_start').datetimepicker({
		pickDate :false
	});

	$('#datetimepicker_date_start').datetimepicker({
		pickTime :false
	});

	$('#datetimepicker_time_end').datetimepicker({
		pickDate :false
	});

	$('#datetimepicker_date_end').datetimepicker({
		pickTime :false
	});


	!function ($) {
		$(document).on("click","ul.nav li.parent > a > span.icon", function(){
			$(this).find('em:first').toggleClass("glyphicon-minus");
		});
		$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
	}(window.jQuery);

	$(window).on('resize', function () {
		if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
	})
	$(window).on('resize', function () {
		if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
	})

	switch("<?php echo $content['leave_type'];?>")
	{
		case "sick_leave":
			$("#doctor_notes_visible").show();break;
		case "leave" :
		default:
			$("#doctor_notes_visible").hide();break;
	}

	$("#doctor_notes").val("<?php echo $content['doctor_notes'];?>");

	function edit()
	{
		$("#loading").show();
		$("#main_panel").hide();
		$.post("/unpaid/detail/edit",
			{
				start_time : $("#start_date").val()+" "+$("#start_time").val(),
				end_time : $("#end_date").val()+" "+$("#end_time").val(),
				time_length: $("#time_length").val(),
				reason : $("#reason").val(),
				leave_type : <?php echo '"'.$content['leave_type'].'"';?>,
				doctor_notes : $("#doctor_notes").val(),
				id : <?php echo $id; ?>
			},
			function(data,status)
			{
				$("#loading").hide();
				$("#main_panel").show();
				json1 = eval("("+data+")");
				if(json1.code == "200")
				{
					confirm("You've edited this leave request successfully~");
					location.href = "/unpaid";
				}
				else
				{
					alert(json1.text);
				}
			});
	}

	function getToalDays()
	{
		$.post("/api/paid/days",
			{
				start_time : $("#start_date").val()+" "+$("#start_time").val(),
				end_time : $("#end_date").val()+" "+$("#end_time").val(),
				time_length: $("#time_length").val(),
				reason : $("#reason").val(),
				leave_type : "leave"
			},
			function(data,status)
			{
				json1 = eval("("+data+")");
				$("#time_length").val(json1.amount+"d");
			});
	}

</script>
</body>
</html>




