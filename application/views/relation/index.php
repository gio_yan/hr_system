<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MotionGlobal Leave Center</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">
    <style type="text/css">
        .tip{width:300px;height:200px;display:none;background-color:#f6f7f7;color:#333333;line-height:18px;border:1px solid #e1e3e2;padding:5px; z-index: 12001;}
    </style>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active">Corporation Relation</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Organization Chart</h1>
            <div id="tooltip" class="tip" style="display: none"></div>
            <div id="createtable">
            </div>
        </div>
    </div><!--/.row-->


    <div class="row">


    </div><!--/.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Click on names to navigate

                </div>
                <div class="panel-body">

                    <table class="class" id = "table_relation">
                        <tr id="leader">
                            <td><?php echo $leader;?></td>
                        </tr>
                        <tr id="colleague">
                            <?php foreach($colleague as $r) {?>
                            <td><?php echo $r;?></td>
                            <?php }?>
                        </tr>
                        <tr id="team">
                            <?php foreach($team as $r) {?>
                                <td><?php echo $r;?></td>
                            <?php }?>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </div><!--/.row-->

</div><!--/.row-->
</div>	<!--/.main-->

<script src="/assets/js/jquery-1.11.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/chart.min.js"></script>
<script src="/assets/js/easypiechart.js"></script>
<script src="/assets/js/easypiechart-data.js"></script>
<script src="/assets/js/bootstrap-datepicker.js"></script>
<script>
    $('#calendar').datepicker({
    });

    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

    function getInfo(id)
    {
        location.href = "/sitemap/"+id;
    }

    function mousePosition(ev){
        ev = ev || window.event;
        if(ev.pageX || ev.pageY){
            return {x:ev.pageX, y:ev.pageY};
        }
        return {
            x:ev.clientX + document.body.scrollLeft + document.body.clientLeft,
            y:ev.clientY + document.body.scrollTop + document.body.clientTop
        };
    }

    $(".person_td").mouseover(function(e){

        var mousePos = mousePosition(e);
        var  xOffset = 10;
        var  yOffset = 15;
        $("#tooltip").css("display","block").css("position","absolute").css("top",0 + "px").css("right",100 + "px");

        $.post("/relation/get/basic",
            {
                id : $(this).attr("id")
            },
            function(data,status)
            {
                $("#tooltip").empty();
                json1 = eval("("+data+")");
                $("#tooltip").append("<h5>Name: "+json1.name+"</h5><h5>Chinese Name: "+json1.chinese_name+"</h5><h5>Work Email: "+json1.work_email+"</h5><h5>Phone Number: "+json1.phone_number+"</h5><h5>Mobile Number: "+json1.moblie_number+"</h5><h5>Personal Email: "+json1.personal_email+"</h5><h5>Skype: "+json1.skype+"</h5>");
            });


    });
    $(".person_td").mouseout(function(){
        //$("#tooltip").empty();
        //$("#tooltip").css("display","none");
    });

</script>
</body>

</html>
