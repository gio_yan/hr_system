<!DOCTYPE HTML>
<html>
<head>
    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">
    <title>New MC</title>
</head>
<body>

<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/leave"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">New MC</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create a New Missing Card Request</h1>
        </div>
    </div><!--/.row-->


    <div id="loading" class="panel" align="center"><img src="/assets/img/5.gif" alt="" /><br><h3>Waiting For Transmission</h3></div>

    <div class="row" id="main_panel">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo $date?></div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <form role="form">



                            <div class="panel panel-default">
                                <div class="panel-heading">Start Time</div>
                                <div class="panel-body" id="title_panel">
                                    <div id="" class="input-append">
<!--                                        <input data-format="hh:mm" type="text" id="start_time" value="09:30"></input>-->
<!--									    <span class="add-on"  style="padding: 1px 20px">-->
<!--									      <i data-time-icon="icon-time" data-date-icon="icon-calendar">-->
<!--                                          </i>-->
<!--									    </span>-->
                                        <select name="select_1" id="start_time" style="width: 100px;" onchange="getToalDays()">
                                            <option value="09:30" onclick="getToalDays()">09:30</option>
                                            <option value="14:30" onclick="getToalDays()">14:30</option>

                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">End Time</div>
                                <div class="panel-body" id="title_panel">
                                    <div id="" class="input-append">
<!--                                        <input data-format="hh:mm" type="text" id="end_time" value="18:30"></input>-->
<!--									    <span class="add-on" style="padding: 1px 20px">-->
<!--									      <i data-time-icon="icon-time" data-date-icon="icon-calendar">-->
<!--                                          </i>-->
<!--									    </span>-->
                                        <select name="select_1" id="end_time" style="width: 100px;" onchange="getToalDays()">
                                            <option value="18:30" onclick="getToalDays()">18:30</option>
                                            <option value="13:30" onclick="getToalDays()">13:30</option>


                                        </select>
                                    </div>

                                </div>
                            </div>




                            <div class="panel panel-default">
                                <div class="panel-heading">Reason</div>
								  <textarea class="form-group" style="width: 100%" id="reason">

	                              </textarea>
                            </div>

                            <button type="button" class="btn btn-primary btn-lg" onclick="submit_new()">Submit</button>
                    </div>

                    </form>
                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->

</div><!--/.main-->

<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
        src="/assets/js/bootstrap-datetimepicker.min.js">
</script>


<script type="text/javascript">
    $('#datetimepicker_time_start').datetimepicker({
        pickDate :false
    });

    $('#datetimepicker_date_start').datetimepicker({
        pickTime :false
    });

    $('#datetimepicker_time_end').datetimepicker({
        pickDate :false
    });

    $('#datetimepicker_date_end').datetimepicker({
        pickTime :false
    });

    $("#reason").empty();
    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })


    function submit_new()
    {
        $("#loading").show();
        $("#main_panel").hide();
        $.post("/mc/add/new",
            {
                date : "<?php echo $date?>",
                start_time : $("#start_time").val(),
                end_time : $("#end_time").val(),
                reason : $("#reason").val()
            },
            function(data,status)
            {
                $("#loading").hide();
                $("#main_panel").show();
                json1 = eval("("+data+")");
                if(json1.code == "200")
                {
                    confirm("Your MC has been received");
                    location.href = "/mc";
                }
                else
                {
                    alert(json1.text);
                }
            });
    }

</script>
</body>
</html>




