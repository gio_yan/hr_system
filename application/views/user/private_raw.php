<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Private Raw Data </title>

    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/hr"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">hr</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Private Raw Data</h1>
        </div>
    </div><!--/.row-->



    <div class="row">
        <div class="col-mg-12 "  id="#tab_user">
            <div class="panel panel-default">
                <div class="panel-body">


<!--                    <div class="col-md-4">-->
<!---->
<!--                        <div class="panel-body" ></div>-->
<!---->
<!--                    </div>-->
<!--                    <div class="col-md-4">-->
<!---->
<!--                        <div class="panel-body" style="background: #e5e5e5;">Weekends</div>-->
<!---->
<!--                    </div>-->


                    <div class="col-md-4">
                        <div class="panel-heading">Start Date</div>
                        <div id="start_date_input" class="input-append panel-body">
                            <input data-format="yyyy-MM-dd" type="text" id="start_date_content"value="<?php echo date("Y-m-d",strtotime("1 month ago"));?>"></input>
                            <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel-heading">End Date</div>
                        <div id="end_date_input" class="input-append panel-body">
                            <input data-format="yyyy-MM-dd" type="text" id="end_date_content" value="<?php echo date("Y-m-d");?>"></input>
                            <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel-heading">Action</div>
                        <div id="" class="input-append panel-body">
                            <input type="button" class="btn btn-success" value="Search" onclick="generate()">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <form role="form">
                            <table class="table" id="table_content">
                                <tr>
                                    <td>Record Date</td>
                                    <td>Weekday</td>
                                    <td>Check In</td>
                                    <td>Check Out</td>
                                    <td>Detail</td>
                                </tr>

                            </table>



                        </form>


                    </div>
                    <div class="col-md-12">

                        <div class="panel-body" >

                            <div style="margin-bottom: 6px"><div style="background: #f9b63e;color:#ffffff;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>&nbsp; <div style="float:left"> Empty Record</div></div>
                            <div style="margin-bottom: 6px"><div style="background: #4baee8;color:#ffffff;float:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>&nbsp; <div style="float:left"> Early check out, or late check in</div></div>
                            <div style="margin-bottom: 6px"><div style="background: #e5e5e5;float:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>&nbsp; <div style="float:left"> Weekends</div></div>
                        </div>

                    </div>

                </div>
            </div>
        </div><!-- /.col-->

    </div><!-- /.row -->






</div><!--/.main-->

<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
        src="/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script>
    var departs = null;
    $('#start_date_input').datetimepicker({
        pickTime :false
    });

    $('#end_date_input').datetimepicker({
        pickTime :false
    });
    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })

    function generate()
    {
        var weeks  = new Array("Sun", "Mon", "Tue","Wed","Thur","Fri","Sat");
        $.post("/hr/raw/data",
            {
                start : $("#start_date_content").val(),
                end   : $("#end_date_content").val(),
                id : <?php echo $id;?>
            },
            function(data,status)
            {

                $("#table_content").empty();
                $("#table_content").append("<tr><td>Record Date</td><td>Weekday</td> <td>Check In</td> <td>Check Out</td><td>Detail</td></tr>");
                json1 = eval("("+data+")");
                if(json1.code=="101")
                {
                    alert(json1.msg);
                }
                for(i in json1)
                {
                    in_flag = out_flag = 'white';
                    font_flag = "black";
                    weekend_flag = '';
                    if(json1[i].check_in == '--')
                    {
                        json1[i].check_in = '';
                        in_flag = '#f9b63e';
                    }
                    if(json1[i].check_out == '--')
                    {
                        json1[i].check_out = '';
                        out_flag = '#f9b63e';
                    }
                    if(json1[i].color_flag == "blue")
                    {
                        in_flag = out_flag = "#4baee8";
                        font_flag = 'white';
                    }
                    else if(json1[i].color_flag == "green")
                    {
                        in_flag = out_flag = "#e5e5e5";
                        //font_flag = 'white';
                        weekend_flag = "#e5e5e5";
                    }
                    $("#table_content").append("<tr style='background-color: "+weekend_flag+"'><td>"+json1[i].record_date+"</td><td>"+weeks[json1[i].weekday]+"</td><td style='background-color:"+in_flag+";color:"+font_flag+"'>"+json1[i].check_in+"</td><td style='background-color:"+out_flag+";color:"+font_flag+"'>"+json1[i].check_out+"</td><td>"+json1[i].reason+"</td></tr>");
                }
            });
    }
</script>
</body>

</html>
