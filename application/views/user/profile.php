<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Profile</title>

<link href="/assets/css/bootstrap.css" rel="stylesheet">
<link href="/assets/css/datepicker3.css" rel="stylesheet">
<link href="/assets/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<?php $this->load->view("/widgets/head_nav");?>
	<?php $this->load->view("/widgets/left_nav");?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="">Profile</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">Complete following information, please.</h3>
			</div>
		</div><!--/.row-->

        <div class="row">
            <div class = "col-md-6">
                <div class="panel panel-default">
                <div class="page-header">Leave Days Balance:<font size="12px" color="red"><?php echo $data['leave_days']?></font></div>
                </div>
            </div>

            <div class = "col-md-6">
                <div class="panel panel-default">
                    <div class="page-header">Sick Days Balance:<font size="12px" color="red"><?php echo $data['sick_days']?></font></div>
                </div>
            </div>
        </div><!--/.row-->

		<div class="row">
			<div class="col-mg-12">
				<div class="panel panel-default">
					<div class="panel-heading"></div>
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form">

								<div class="panel panel-default">
								  <div class="panel-heading">Your Name</div>
								  <div class="panel-body" id="">
								    <?php echo $data['name'];?>
								  </div>
									<div class="panel-heading">Your Title</div>
									<div class="panel-body" id="">
										<?php echo $data['title'];?>
									</div>
									<div class="panel-heading">Your Department</div>
									<div class="panel-body" id="">
										<?php echo $data['department'];?>
									</div>
									<div class="panel-heading">Your Chinese Name</div>
									<div class="panel-body" id="cn_name_panel">
										<?php echo $data['chinese_name'];?>
									</div>
									<div class="panel-heading">Your Work Email</div>
									<div class="panel-body" id="">
										<?php echo $data['work_email'];?>
									</div>
									<div class="panel-heading">Your Work Location</div>
									<div class="panel-body" id="">
										<?php echo $data['work_location'];?>
									</div>

								</div>
																

								
								<button type="button" class="btn btn-success">Edit</button>  
								<button type="button" class="btn btn-primary">Save</button>
							</form>
						</div>
						<div class = "col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">Your Office Phone Number</div>
								<div class="panel-body" id="phone_panel">
									<?php echo $data['phone_number'];?>
								</div>
								<div class="panel-heading">Your Mobile Number</div>
								<div class="panel-body" id="mobile_panel">
									<?php echo $data['moblie_number'];?>
								</div>
								<div class="panel-heading">Your Personal Email</div>
								<div class="panel-body" id="personal_email_panel">
									<?php echo $data['personal_email'];?>
								</div>
								<div class="panel-heading">Your Skype</div>
								<div class="panel-body" id="skype_panel">
									<?php echo $data['skype'];?>
								</div>
								<div class="panel-heading">On Board Date</div>
								<div class="panel-body" id="">
									<?php echo $data['start_date'];?>
								</div>
								<div class="panel-heading">Your Home Address</div>
								<div class="panel-body" id="home_address">
									<?php echo $data['home_address'];?>
								</div>



							</div>


						</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->

	<script src=" /assets/js/jquery-1.11.1.min.js"></script>
	<script src=" /assets/js/bootstrap.min.js"></script>
	<script src=" /assets/js/chart.min.js"></script>

	<script src=" /assets/js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})


		$(".btn-success").click(function()
		{
			$("#cn_name_panel").empty();
			$("#phone_panel").empty();
			$("#personal_email_panel").empty();
			$("#mobile_panel").empty();
			$("#skype_panel").empty();
			$("#home_address").empty();


			$("#cn_name_panel").append('<input class="form-control" id="chinese_name_input" value="<?php echo $data['chinese_name'];?>">');
			$("#phone_panel").append('<input class="form-control" id="phone_number_input" value="<?php echo $data['phone_number'];?>">');
			$("#personal_email_panel").append('<input class="form-control" id="personal_email_input" value="<?php echo $data['personal_email'];?>">');
			$("#mobile_panel").append('<input class="form-control" id="moblie_number_input" value="<?php echo $data['moblie_number'];?>">');
			$("#skype_panel").append('<input class="form-control" id="skype_input" value="<?php echo $data['skype'];?>">');
			$("#home_address").append('<input class="form-control" id="home_address_input" value="<?php echo $data['home_address'];?>">');

			$(".btn-primary").attr("onclick","post_to()");
		});

		function post_to()
		{
			$.post("/user/edit/profile",
			{
				chinese_name : $("#chinese_name_input").val(),
				phone_number: $("#phone_number_input").val(),
				personal_email: $("#personal_email_input").val(),
				moblie_number: $("#moblie_number_input").val(),
				skype: $("#skype_input").val(),
				home_address: $("#home_address_input").val()
			},
			function(data,status)
			{
				json1 = eval("("+data+")");
				if(json1.code == "200")
				{
					confirm("Your profile has been updated");
					location.href= '/user/profile';
				}
			});
		}
	</script>	
</body>

</html>
