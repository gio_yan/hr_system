<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Profile</title>

    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">settings</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Password Alter</h1>

        </div>
    </div><!--/.row-->



    <div class="row">
        <div class="col-mg-12">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <form role="form">

                            <div class="panel panel-default">
                                <div class="panel-heading">Password On use</div>
                                <div class="panel-body">
                                    <input type="password" class = "form-control"  id="pass">
                                </div>
                                
                                <div class="panel-heading">New Password</div>
                                <div class="panel-body" id="">
                                    <input type="password" class = "form-control"  id="pass_new">
                                </div>


                                <div class="panel-heading">New Password Confirm</div>
                                <div class="panel-body" id="">
                                    <input type="password" class = "form-control"  id="pass_con">
                                </div>

                                </div>
                                <div class="panel-heading">Operation</div>
                                <div class="panel-body" id="cn_name_panel">
                                    <button type="button" class="btn btn-primary" onclick="post_to()">Save</button>
                                </div>


                            </div>




                        </form>
                    </div>



                    </div>
                </div>
            </div><!-- /.col-->
        </div><!-- /.row -->

    </div><!--/.main-->

    <script src=" /assets/js/jquery-1.11.1.min.js"></script>
    <script src=" /assets/js/bootstrap.min.js"></script>
    <script src=" /assets/js/chart.min.js"></script>

    <script src=" /assets/js/bootstrap-datepicker.js"></script>
    <script>
        !function ($) {
            $(document).on("click","ul.nav li.parent > a > span.icon", function(){
                $(this).find('em:first').toggleClass("glyphicon-minus");
            });
            $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
        }(window.jQuery);

        $(window).on('resize', function () {
            if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
        })
        $(window).on('resize', function () {
            if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
        })

        function post_to()
        {
            $.post("/user/edit/settings",
                {
                    password : $("#pass").val(),
                    password_new: $("#pass_new").val(),
                    password_confirm: $("#pass_con").val()
                },
                function(data,status)
                {
                    json1 = eval("("+data+")");
                    if(json1.code == "200")
                    {
                        confirm("Your Password has been updated");
                        location.href= '/';
                    }
                    else
                    {
                        alert(json1.msg);
                    }
                });
        }
    </script>
</body>

</html>
