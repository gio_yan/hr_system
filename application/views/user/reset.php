<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>MG Center</title>

<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/datepicker3.css" rel="stylesheet">
<link href="/assets/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="/assets/js/html5shiv./assets/js"></script>
<script src="/assets/js/respond.min./assets/js"></script>
<![endif]-->

</head>

<body>
	<?php $this->load->view("/widgets/head_nav");?>
		
	
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Reset Password</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Reset Password</h1>
			</div>
		</div><!--/.row-->


        <div id="loading" class="panel" align="center"><img src="/assets/img/5.gif" alt="" /><br><h3>Waiting For Transmission</h3></div>

        <div class="row" id="main_panel">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form">
							
								<div class="form-group">
									<label>Email Address in MotionGlobal</label>
									<input class="form-control" placeholder="XXX@motionglobal.com" id="user">
								</div>
																

								<input type="button" id="sumbit" class="btn btn-primary" value="Submit"> 
								
							</div>
							
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->

	<script src="/assets/js/jquery-1.11.1.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/chart.min.js"></script>
	<script src="/assets/js/chart-data.js"></script>
	<script src="/assets/js/easypiechart.js"></script>
	<script src="/assets/js/easypiechart-data.js"></script>
	<script src="/assets/js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})

		$("#sumbit").click(function(){
            $("#loading").show();
            $("#main_panel").hide();
			$.post("/user/reset/password",
			{
				email : $("#user").val()
			},
			function(data,status)
			{
                $("#loading").hide();
                $("#main_panel").show();
				json1 = eval("("+data+")");
				if(json1.code == "200")
				{
					confirm("A mail has been sent to your mailbox,Please check your mail");
					location.href = '/';
				}
				else
				{
					alert(json1.text);
				}
			});
		});
	</script>	
</body>

</html>
