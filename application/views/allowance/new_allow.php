<!DOCTYPE HTML>
<html>
<head>
    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/styles.css" rel="stylesheet">
    <style type="text/css">

    </style>
    <title>New Allowance</title>
</head>
<body>

<?php $this->load->view("/widgets/head_nav");?>
<?php $this->load->view("/widgets/left_nav");?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="">New Allowance</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create New Allowance Request</h1>
        </div>
    </div><!--/.row-->

    <div id="loading" class="panel" align="center"><img src="/assets/img/5.gif" alt="" /><br><h3>Waiting For Transmission</h3></div>

    <div class="row" id="main_panel">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <div class="col-md-6">


                            <div class="panel panel-default">
                                <div class="form-group">
                                    <div class="panel-heading">Allowance Type</div>
                                    <select class="form-control" id="leave_type" onchange="">
                                        <option value="visa related (SH)">visa related (SH)</option>
                                        <option value="visa related (Medical)">visa related (Medical)</option>
                                        <option value="visa related(HK/Macau)">visa related(HK/Macau)</option>
                                        <option value="visa related(home/3rd country)">visa related(home/3rd country)</option>
                                        <option value="Funeral(1 day)">Funeral(1 day)</option>
                                        <option value="Funeral(2 days)">Funeral(2 days)</option>
                                        <option value="Funeral(3 days)">Funeral(3 days)</option>
                                        <option value="Police Reg.">Police Reg.</option>
                                        <option value="Marriage leave">Marriage leave</option>
                                        <option value="Maternity leave">Maternity leave</option>
                                        <option value="3 years award(1 day leave)">3 years award(1 day leave)</option>
                                        <option value="5 years award(3 days leave)">5 years award(3 days leave)</option>
                                        <option value="work from home">work from home</option>
                                        <option value="Business trip">Business trip</option>
                                        <option value="Paternity leave">Paternity leave</option>
                                    </select>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">Start Time</div>
                                <div class="panel-body" id="title_panel">

                                    <div id="datetimepicker_date_start" class="input-append">
                                        <input data-format="yyyy-MM-dd" type="text" id="start_date" value="<?php echo date("Y-m-d")?>">
									    <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                                    </div>
                                    <div id="datetimepicker_time_start" class="input-append">
                                        <input data-format="hh:mm" type="text" id="start_time" value="09:30">
                                        <span class="add-on"  style="padding: 1px 20px">
                                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                          </i>
                                        </span>

                                    </div>

                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">End Time</div>
                                <div class="panel-body" id="title_panel">

                                    <div id="datetimepicker_date_end" class="input-append">
                                        <input data-format="yyyy-MM-dd" type="text" id="end_date" value="<?php echo date("Y-m-d");?>"  ></input>
                                        <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
                                          </i>
									    </span>

                                    </div>
                                    <div id="datetimepicker_time_end" class="input-append">
                                        <input data-format="hh:mm" type="text" id="end_time" value="12:30" ></input>
                                         <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                          </i>
									    </span>
                                    </div>

                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">Time Length</div>
                                <div class="input-group">
                                    <input class = "form-control" id="time_length" value="0.375d" readonly/>
                                    <span class="input-group-addon" style="background-color: #30a5ff;color:#fff" onclick="getToalDays()">Click here to get total days</span>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">Reason</div>
								  <textarea class="form-group" style="width: 100%" id="reason">

	                              </textarea>
                            </div>

                            <button type="button" class="btn btn-primary btn-lg" onclick="submit_new()">Submit</button>
                    </div>

                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->

</div><!--/.main-->

<script type="text/javascript"
        src="/assets/js/bootstrap.min.js">
</script>
<script type="text/javascript"
        src="/assets/js/bootstrap-datetimepicker.min.js">
</script>


<script type="text/javascript">
    var max_length = 0;
    $('#datetimepicker_time_start').datetimepicker({
        pickDate :false
    });

    $('#datetimepicker_date_start').datetimepicker({
        pickTime :false
    });

    $('#datetimepicker_time_end').datetimepicker({
        pickDate :false
    });

    $('#datetimepicker_date_end').datetimepicker({
        pickTime :false
    });
    $("#reason").empty();

    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })


    function submit_new()
    {
        getToalDays();
        $("#loading").show();
        $("#main_panel").hide();
        $.post("/allowance/add/new",
            {
                start_time : $("#start_date").val()+" "+$("#start_time").val(),
                end_time : $("#end_date").val()+" "+$("#end_time").val(),
                time_length: $("#time_length").val(),
                reason : $("#reason").val(),
                leave_type : $("#leave_type").val()
            },
            function(data,status)
            {
                //alert(data);return;
                $("#loading").hide();
                $("#main_panel").show();
                json1 = eval("("+data+")");
                if(json1.code == "200")
                {
                    confirm("Your application has been received");
                    location.href = "/allowance";
                }
                else
                {
                    alert(json1.text);
                }
            });
    }

    function count_end()
    {
        $.post("/api/get/allowance/end",
            {
                start_time : $("#start_date").val()+" "+$("#start_time").val(),
                type : $("#leave_type").val()
            },
            function(data,status)
            {
                json1 = eval("("+data+")");
                $("#end_date").val(json1.end_date);
                $("#end_time").val(json1.end_time);
                $("#time_length").val(json1.time_length+"d");
            });
    }

    function getToalDays()
    {
        $.post("/api/paid/days",
            {
                start_time : $("#start_date").val()+" "+$("#start_time").val(),
                end_time : $("#end_date").val()+" "+$("#end_time").val(),
                time_length: $("#time_length").val(),
                reason : $("#reason").val(),
                leave_type : "leave"
            },
            function(data,status)
            {
                json1 = eval("("+data+")");
                $("#time_length").val(json1.amount+"d");
            });
    }
</script>
</body>
</html>




