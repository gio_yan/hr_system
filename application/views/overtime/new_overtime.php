<!DOCTYPE HTML>
<html>
  <head>
    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
     href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="/assets/css/datepicker3.css" rel="stylesheet">
	<link href="/assets/css/styles.css" rel="stylesheet">
	<title>New Overtime</title>
  </head>
  <body>

  <?php $this->load->view("/widgets/head_nav");?>
	<?php $this->load->view("/widgets/left_nav");?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="">New Overtime</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Create a New Overtime Request</h1>
			</div>
		</div><!--/.row-->


		<div id="loading" class="panel" align="center"><img src="/assets/img/5.gif" alt="" /><br><h3>Waiting For Transmission</h3></div>

		<div class="row" id="main_panel">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"></div>
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form">
								<div class="form-group">
									<label>Employee</label>
									<select class="form-control" id="employee_name">
										<?php foreach($content as $con) { ?>
										<option value="<?php echo $con['user_id'];?>" ><?php echo $con['user_name'];?></option>
										<?php }?>
									</select>
								</div>
								
																
								<div class="panel panel-default">
								  <div class="panel-heading">Start Time</div>
								  <div class="panel-body" id="title_panel">
								    
								    <div id="datetimepicker_date_start" class="input-append">
									    <input data-format="yyyy-MM-dd" type="text" id="start_date" value="<?php echo date("Y-m-d")?>"></input>
									    <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
									      </i>
									    </span>
									  </div>
									  <div id="datetimepicker_time_start" class="input-append">
									    <input data-format="hh:mm" type="text" id="start_time" value="09:30"></input>
									    <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
									      </i>
									    </span>
<!--										  <select name="select_1" id="start_time" style="width: 100px;" onchange="getToalDays()">-->
<!--											  <option value="09:30" onclick="getToalDays()">09:30</option>-->
<!--											  <option value="14:30" onclick="getToalDays()">14:30</option>-->
<!---->
<!--										  </select>-->
									  </div>
									
								  </div>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">End Time</div>
								  <div class="panel-body" id="title_panel">
								    
								    <div id="datetimepicker_date_end" class="input-append">
									    <input data-format="yyyy-MM-dd" type="text" id="end_date" value="<?php echo date("Y-m-d")?>"></input>
									    <span class="add-on" style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
									      </i>
									    </span>
									  </div>
									  <div id="datetimepicker_time_end" class="input-append">
									    <input data-format="hh:mm" type="text" id="end_time" value="18:30"></input>
									    <span class="add-on" style="padding: 1px 20px">
									      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
									      </i>
									    </span>
<!--										  <select name="select_2" id="end_time" style="width: 100px;" onchange="getToalDays()">-->
<!---->
<!--											  <option value="13:30" onclick="getToalDays()">13:30</option>-->
<!--											  <option value="18:30" onclick="getToalDays()">18:30</option>-->
<!---->
<!--										  </select>-->
									  </div>
									
								  </div>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">Pay OR Day</div>
								  <div class="form-group">

									  <div class="radio">
										  <label>
											  <input type="radio" name="p-d" id="optionsRadios2" value="day" checked="">Day
										  </label>
									  </div>

									<div class="radio">
										<label>
											<input type="radio" name="p-d" id="optionsRadios1" value="pay" >Pay
										</label>
									</div>

									
								  </div>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">Compensation</div>
								  <div class="form-group">
									
									<div class="radio">
										<label>
											<input type="radio" name="compensation" id="optionsRadios1" value="1" checked="" onclick="testVal()">*1 time
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="compensation" id="optionsRadios2" value="2"  onclick="testVal()">*1.5 times
										</label>
									</div>

									<div class="radio">
										<label>
											<input type="radio" name="compensation" id="optionsRadios3" value="3" onclick="testVal()">*2 times
										</label>
									</div>

									  <div class="radio">
										  <label>
											  <input type="radio" name="compensation" id="optionsRadios4" value="4" onclick="testVal()">*3 times
										  </label>
									  </div>
								  </div>
								</div>
								

								<div class="panel panel-default">
								 <div class="panel-heading">Time Length</div>
								 	<div class="input-group">
								 	 <input class = "form-control" id="time_length" readonly/>
										<span class="input-group-addon" style="background-color: #30a5ff;color:#fff" onclick="getToalDays()">Click here to get total days</span>
									</div>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">Reason</div>
								  <textarea class="form-group" style="width: 100%" id="reason">
	                                
	                              </textarea> 
								</div>
								
								<button type="button" class="btn btn-primary btn-lg" onclick="submit_new()">Save</button>

								</div>
		
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->
    
    <script type="text/javascript"
     src="/assets/js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    
    
    <script type="text/javascript">
      $('#datetimepicker_time_start').datetimepicker({
        pickDate :false
      });

      $('#datetimepicker_date_start').datetimepicker({
        pickTime :false
      });

      $('#datetimepicker_time_end').datetimepicker({
        pickDate :false
      });

      $('#datetimepicker_date_end').datetimepicker({
        pickTime :false
      });
	  $("#reason").empty();
		var mul = 1.0;

      !function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})


		function submit_new()
		{
			$("#loading").show();
			$("#main_panel").hide();
			$.post("/overtime/add/new",
			{
				start_time : $("#start_date").val()+" "+$("#start_time").val(),
				end_time : $("#end_date").val()+" "+$("#end_time").val(),
				time_length: $("#time_length").val(),
				reason : $("#reason").val(),
				compensation : $('input[type="radio"][name="compensation"]:checked').val(),
				pOd : $('input[type="radio"][name="p-d"]:checked').val(),
				employee_id : $("#employee_name").val()
			},
			function(data,status)
			{
				$("#loading").hide();
				$("#main_panel").show();
				json1 = eval("("+data+")");
				if(json1.code == "200")
				{
					confirm("Your application has been received");
					location.href = "/approve/overtime";
				}
				else
				{
					alert(json1.text);
				}
			});
		}
	  function getToalDays()
	  {
		  $.post("/api/paid/days",
			  {
				  start_time : $("#start_date").val()+" "+$("#start_time").val(),
				  end_time : $("#end_date").val()+" "+$("#end_time").val(),
				  time_length: $("#time_length").val(),
				  reason : $("#reason").val(),
				  leave_type : "overtime"
			  },
			  function(data,status)
			  {
				  json1 = eval("("+data+")");
//                mul = 0;
//                switch($('input[type="radio"][name="compensation"]:checked').val())
//                {
//                    case "1":mul = 1.0;break;
//                    case "2":mul = 1.5;break;
//                    case "3":mul = 2.0;break;
//                    case "4":mul = 3.0;break;
//                    default : mul=1;break;
//                }

				  $("#time_length").val((mul*eval(json1.amount))+"d");
			  });
	  }

	  function testVal()
	  {
		  mul = ($('input[type="radio"][name="compensation"]:checked').val());
		  if(mul == 2) mul = 1.5;
		  else if(mul == 3) mul = 2;
		  else if(mul == 4) mul = 3;
		  else mul = 1;
		  getToalDays();
	  }

    </script>
  </body>
</html>




