<!DOCTYPE HTML>
<html>
  <head>
    <link href="/assets/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
     href="/assets/css/bootstrap-datetimepicker.min.css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="/assets/css/datepicker3.css" rel="stylesheet">
	<link href="/assets/css/styles.css" rel="stylesheet">
	<title>Overtime Detail</title>
  </head>
  <body>

  <?php $this->load->view("/widgets/head_nav");?>
	<?php $this->load->view("/widgets/left_nav");?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="/approve/overtime"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="">Overtime Detail</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Overtime Detail</h1>
			</div>
		</div><!--/.row-->

		<div id="loading" class="panel" align="center"><img src="/assets/img/5.gif" alt="" /><br><h3>Waiting For Transmission</h3></div>

		<div class="row" id="main_panel">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"></div>
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form">
								
								
																
								<div class="panel panel-default">
								  <div class="panel-heading">Start Time</div>
								  <div class="panel-body" id="title_panel">
								    
								    <div id="datetimepicker_date_start" class="input-append">
									    <input data-format="yyyy-MM-dd" type="text" id="start_date" value="<?php echo date("Y-m-d",strtotime($content['start_time']));?>"></input>
									    <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
									      </i>
									    </span>
									  </div>
									  <div id="datetimepicker_time_start" class="input-append">
									    <input data-format="hh:mm:ss" type="text" id="start_time"  value="<?php echo date("H:i:s",strtotime($content['start_time']));?>"></input>
									    <span class="add-on"  style="padding: 1px 20px">
									      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
									      </i>
									    </span>
<!--										  <select name="select_1" id="start_time" style="width: 100px;" onchange="getToalDays()">-->
<!--											  <option value="09:30" onclick="getToalDays()">09:30</option>-->
<!--											  <option value="14:30" onclick="getToalDays()">14:30</option>-->
<!---->
<!--										  </select>-->
									  </div>
									
								  </div>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">End Time</div>
								  <div class="panel-body" id="title_panel">
								    
								    <div id="datetimepicker_date_end" class="input-append">
									    <input data-format="yyyy-MM-dd" type="text" id="end_date" value="<?php echo date("Y-m-d",strtotime($content['end_time']));?>"></input>
									    <span class="add-on" style="padding: 1px 20px">
									      <i data-time-icon="icon-date" data-date-icon="icon-calendar">
									      </i>
									    </span>
									  </div>
									  <div id="datetimepicker_time_end" class="input-append">
									    <input data-format="hh:mm:ss" type="text" id="end_time"  value="<?php echo date("H:i:s",strtotime($content['end_time']));?>"></input>
									    <span class="add-on" style="padding: 1px 20px">
									      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
									      </i>
									    </span>
<!--										  <select name="select_2" id="end_time" style="width: 100px;" onchange="getToalDays()">-->
<!---->
<!--											  <option value="13:30" onclick="getToalDays()">13:30</option>-->
<!--											  <option value="18:30" onclick="getToalDays()">18:30</option>-->
<!---->
<!--										  </select>-->
									  </div>
									
								  </div>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">Pay OR Day</div>
								  <div class="form-group">

									  <div class="radio">
										  <label>
											  <input type="radio" name="p-d" id="day-pd" value="day">Day
										  </label>
									  </div>

									<div class="radio">
										<label>
											<input type="radio" name="p-d" id="pay-pd" value="pay">Pay
										</label>
									</div>

									
								  </div>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">Compensation</div>
								  <div class="form-group">

                                      <div class="radio">
                                          <label>
                                              <input type="radio" name="compensation" id="1-d" value="1">*1 time
                                          </label>
                                      </div>

									<div class="radio">
										<label>
											<input type="radio" name="compensation" id="4-d" value="2" >*1.5 times
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="compensation" id="2-d" value="3">*2 times
										</label>
									</div>

									<div class="radio">
										<label>
											<input type="radio" name="compensation" id="3-d" value="4">*3 times
										</label>
									</div>


								  </div>
								</div>
								

								<div class="panel panel-default">
								  <div class="panel-heading">Time Length</div>
								  <input class = "form-control" id="time_length" value="<?php echo $content['time_length']?>" readonly/>
								</div>

								<div class="panel panel-default">
								  <div class="panel-heading">Reason</div>
								  <textarea class="form-group" style="width: 100%" id="reason"><?php echo $content['reason'];?></textarea>
								</div>
								
							</div>
		
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->
    
    <script type="text/javascript"
     src="/assets/js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    
    
    <script type="text/javascript">
      $('#datetimepicker_time_start').datetimepicker({
        pickDate :false
      });

      $('#datetimepicker_date_start').datetimepicker({
        pickTime :false
      });

      $('#datetimepicker_time_end').datetimepicker({
        pickDate :false
      });

      $('#datetimepicker_date_end').datetimepicker({
        pickTime :false
      });
	  $("#start_time").val("<?php echo date("H:i",strtotime($content['start_time']));?>");
	  $("#end_time").val("<?php echo date("H:i",strtotime($content['end_time']));?>");

      !function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})

	$(document).ready(function(){
		var name ="<?php echo $content['compension']?>".substr(0,3);
		//alert(name.substr(0,1));return;
		$("#<?php echo $content['pay_or_day']?>"+"-pd").attr("checked","checked");
		if(name.substr(2,1) == '.' && name.substr(1,1) == '1')
        {
            $("#4-d").attr("checked","checked");
        }
		else
        {
            name = name.substr(1,1);
        }
		$("#"+name+"-d").attr("checked","checked");
	});

		


    </script>
  </body>
</html>




