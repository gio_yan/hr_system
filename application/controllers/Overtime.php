<?php
class Overtime extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model("user_model");
		$this->load->model("overtime_model");
		$this->load->model("leader_model");
		$this->load->model("mail_model");
	}

	public function overtimeView()
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			
			$data['leave_data'] = $this->overtime_model->getOvertimeByMonthState(date("Y-m"));
			$this->load->view("/overtime/index",$data);
		}
	}

	public function newOvertimeView() // Generates a new Overtime Apply View
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				$data['content'] = $this->leader_model->getSubWorkerState($_SESSION['user_id']);
				$this->load->view("/overtime/new_overtime",$data);
			}
			else
			{
				echo "<script>location.href='/'</script>";
			}
		}
	}

	public function newSelfOvertimeView() // Append a new overtime for oneself
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            $this->load->view("/overtime/self_overtime_new");
        }
    }

    public function newSelfOvertimeAction()
    {
        $data = $this->input->post();
		//var_dump($data);exit;
//		$info = $this->user_model->getUpStreamList();
//		$this->mail_model->send("New Overtime Request",Mail_model::$overtimeContent,$info);
		$res = $this->overtime_model->newSelfOvertimeState($data);
//		$info = $this->user_model->getUpStreamList();
//		$this->mail_model->send("New Overtime Request",Mail_model::$overtimeContent,$info);
		echo json_encode($res);

    }

	public function overtimeDetailView()
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			$id = $this->uri->segment(3);
			$data= array();
			$data['content'] = $this->overtime_model->overtimeDetailState($id);
			$data['id'] = $id;
			$this->load->view("overtime/detail",$data);
		}
	}

	public function editOvertimection()
	{
		/*Edit Leave Information */
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101"));
		}
		else
		{
			$data = $this->input->post();
			echo json_encode($this->overtime_model->editOvertimeState($data));
		}
	}

	
	public function getOvertimeByMonthAction()
	{
		//According to the post data to return responding leave data
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array());
		}
		else
		{
			$data = $this->input->post();
			$date = date("Y-m",strtotime($data['date']));
			echo json_encode($this->overtime_model->getOvertimeByMonthState($date));
		}
	}

	
	public function cancelOvertimeLeaveAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101"));
		}
		else
		{
			echo json_encode($this->overtime_model->updateOvertimeApplyCancel($data));
		}
		
	}

	public function newOvertimeAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101"));
		}
		else
		{
			//echo json_encode($data);exit;
            $info = $this->user_model->getUpStreamList();
            $this->mail_model->send("New Overtime Request",Mail_model::$overtimeContent,$info);
			echo json_encode($this->overtime_model->insertOvertimeApply($data));
		}
	}
//	public function 
}