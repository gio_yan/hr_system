<?php
/*
 * Author : Alex Fu
 * Time :  2017/1/9
 */
require_once("./application/libraries/PHPMailerAutoload.php");
class Sitemap extends CI_Controller
{
    protected $mail_tool ;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->library('encryption');
        $this->load->library("email");
        $this->load->model("sitemap_model");
        $this->load->model("user_model");
       // $this->mail_tool = new smtp("smtp.emailsrvr.com","25","itom@motionglobal.com","motion888");
    }

    public function updateSubLinksAction()
    {

        $data = array();
        if(!isset($_SESSION['user_state']) || ($this->user_model->getNavState() == 0 )  )
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            $data['id'] = $this->uri->segment(2);
            if(!isset($data['id'])) $data['id'] = $_SESSION['user_id'];

            $data = $this->sitemap_model->getLinkedInfo($data['id']);
            $this->load->view("relation/index",$data);
        }


    }

    public function updateSubLinksTest()
    {
        $this->sitemap_model->updateSubLinksState();echo 1;
    }
    public function getInfoAllState()
    {
        $data = $this->input->post();//var_dump($data['id']);exit;
        echo json_encode($this->sitemap_model->getLinkedInfo($data['id']));
    }

    public function getAllMapAction()
    {
        var_dump($this->sitemap_model->isOnShow());exit;
        echo json_encode($this->sitemap_model->nameChart($this->sitemap_model->getAllMapState()));
    }

    public function getMapSingleAction()
    {
        $data = $this->input->post();
        echo json_encode($this->sitemap_model->getMapSingleState($data));
    }
    public function getBasicInfoAction()
    {
        $data = $this->input->post();
        if(!isset($_SESSION['user_state']) || ($this->user_model->getNavState() == 0 )  )
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            echo json_encode($this->sitemap_model->getBasicInfoState($data['id']));
        }
    }
}