<?php
class Hr extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->library('encryption');
        $this->load->library("email");
        $this->load->model("user_model");
        $this->load->model("hr_model");
        $this->load->model("sitemap_model");
    }

    public function indexView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if($this->user_model->isHr() == "yes")
            {
                $data['departs'] = $this->hr_model->getAllDeparts();
                $this->load->view("hr/index",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }

    public function listView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if($this->user_model->isHr() == "yes")
            {
                $data['departs'] = $this->hr_model->getAllDeparts();
                $this->load->view("hr/list",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }

    public function resignedListView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if($this->user_model->isHr() == "yes")
            {
                $data['departs'] = $this->hr_model->getAllDeparts();
                $this->load->view("hr/resign_list",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }
    public function getEmployeeDataByDepartAction()
    {
        $data = $this->input->post();

        if($this->user_model->isHr() == "yes" && isset($data['depart']))
        {
            echo json_encode($this->hr_model->getEmployeeDataByDepartState($data));
        }
        else
        {
            echo json_encode(array("code"=>"101"));
        }
    }

    public function getResignedEmployeeDataByDepartAction()
    {
        $data = $this->input->post();

        if($this->user_model->isHr() == "yes" && isset($data['depart']))
        {
            echo json_encode($this->hr_model->getResignedEmployeeDataByDepartState($data));
        }
        else
        {
            echo json_encode(array("code"=>"101"));
        }
    }

    public function EditEmployeeView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if($this->user_model->isHr() == "yes")
            {
                $data['id'] = $this->uri->segment(4);
                $data['departs'] = $this->hr_model->getAllDeparts();
                $data['content'] =$this->hr_model->getEmployeeAllState($data['id']);//var_dump($data['content']);exit;
                $temp = 0;
                foreach($data['departs'] as $r =>$v)
                {
                    if($v['departs'] == $data['content']['department'])
                    {
                        $temp = $r;
                    }
                }
                $temp_data = $data['departs'][$temp];
                $data['departs'][$temp] = $data['departs'][0];
                $data['departs'][0]=$temp_data;

                $interns = array();
                $interns[] = $data['content']['Internable'];
                $interns[] = $interns[0] == 'no' ? "yes" : "no";
                $data['interns'] = $interns;

                $probations = array();
                $probations[] = $data['content']['Probationable'];
                $probations[] = $probations[0] == 'no' ? "yes" : "no";
                $data['probations'] = $probations;
                $this->load->view("hr/edit_member",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }

    public function editEmployeeAction()
    {
        if($this->user_model->isHr() == "yes")
        {
            $data = $this->input->post();
            echo json_encode($this->hr_model->updateEmployeeState($data));
        }
        else
        {
            echo json_encode(array("code"=>"101"));
        }

    }

    public function newEmployeeView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if($this->user_model->isHr() == "yes")
            {

                $data['departs'] = $this->hr_model->getAllDeparts();
                $this->load->view("/hr/new_employee",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }

    public function newEmployeeAction()
    {
        if($this->user_model->isHr() == "yes")
        {
            $data = $this->input->post();
            echo json_encode($this->hr_model->newEmployeeState($data));
        }
        else
        {
            echo json_encode(array("code"=>"101"));
        }

    }

    public function relationView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if ($this->user_model->isHr() == "yes")
            {
                $data = $this->hr_model->getRelations(0);
                $this->load->view("/hr/relation",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }

        }

    }

    public function getRelationByPageAction()
    {

        if($this->user_model->isHr() == "yes")
        {
            $page = $this->input->post("page") ;
            echo json_encode($this->hr_model->getRelations($page));
        }
        else
        {
            echo json_encode(array("code"=>"101"));
        }


    }

    public function getDepartmentsAction()
    {

        if($this->user_model->isHr() == "yes")
        {
            $data = $this->hr_model->getAllDeparts();
            $data_new = array();
            foreach($data as $da)
            {
                $data_new[] = $da['departs'];
            }
            echo json_encode($data_new);
        }
        else
        {
            echo json_encode(array("code"=>"101"));
        }

    }

    public function updateRelationAction()
    {

        if($this->user_model->isHr() == "yes")
        {
            $data = $this->input->post();
            echo json_encode($this->hr_model->updateRelationState($data));
            $this->sitemap_model->updateSubLinksState();
        }
        else
        {
            echo json_encode(array("code"=>"101"));
        }

    }

    public function monthReportView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if ($this->user_model->isHr() == "yes")
            {
                $this->load->view("/hr/month_report",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }

    public function monthReportAction()
    {
        $data = $this->input->post();
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array());
        }
        else
        {
            if ($this->user_model->isHr() == "yes")
            {
                echo json_encode($this->hr_model->monthReportState($data['start'],$data['end']));
            }
            else
            {
                echo json_encode(array());
            }
        }
    }

    public function employChartView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if ($this->user_model->isHr() == "yes")
            {
                $data['departs'] = $this->hr_model->getAllDeparts();
                $this->load->view("/hr/employee_report",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }

    public function employeeReportAction()
    {
        $data = $this->input->post();
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array());
        }
        else
        {
            if ($this->user_model->isHr() == "yes")
            {
                $data['depart'] = isset($data['depart']) ? $data['depart'] : '';
                $data['user'] = isset($data['user']) ? $data['user'] : '';
                echo json_encode($this->hr_model->employeeReportState($data['start'],$data['end'],$data['depart'],$data['user']));
            }
            else
            {
                echo json_encode(array());
            }
        }
    }

    public function lastDayChartView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if ($this->user_model->isHr() == "yes")
            {
                $this->load->view("/hr/last",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }

    public function lastDayChartAction()
    {
        $data = $this->input->post();
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array());
        }
        else
        {
            if ($this->user_model->isHr() == "yes")
            {
                echo json_encode($this->hr_model->getLeftEmployeeState($data['start'],$data['end']));
            }
            else
            {
                echo json_encode(array());
            }
        }
    }

    public function headChartView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if ($this->user_model->isHr() == "yes")
            {
                $data = $this->hr_model->getHeadCount();
                $this->load->view("/hr/headcount",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }

    public function rawDataPanelView()
    {
        $data = array();
        $data['id'] = $this->uri->segment(3);
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if ($this->user_model->isHr() == "yes")
            {
                $data['departs'] = $this->hr_model->getAllDeparts();
                $this->load->view("/hr/raw_panel",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }

    public function rawDataAction()
    {
        $data = $this->input->post();
        if(!isset($_SESSION['user_state']))
        {

            echo json_encode(array("code"=>"101","msg"=>"Information required"));
        }
        else
        {
            if ($this->user_model->isHr() == "yes" || $_SESSION['user_id'] == $data['id'] || $this->user_model->isRawCheckableState($data))
            {

                echo json_encode($this->hr_model->getRawDataSingle($data));
            }
            else
            {

                echo json_encode(array("code"=>"101","msg"=>"Information required"));
            }
        }
    }

    public function searchRelationAction()
    {
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101","msg"=>"Information required"));
        }
        else
        {
            if ($this->user_model->isHr() == "yes" )
            {
                $data = $this->input->post();
                echo json_encode($this->hr_model->getRelationAsSearch($data));
            }
            else
            {
                echo json_encode(array("code"=>"101","msg"=>"Information required"));
            }
        }
    }

    public function searchListAction()
    {
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101","msg"=>"Information required"));
        }
        else
        {
            if ($this->user_model->isHr() == "yes" )
            {
                $data = $this->input->post();
                echo json_encode($this->hr_model->getListAsSearch($data));
            }
            else
            {
                echo json_encode(array("code"=>"101","msg"=>"Information required"));
            }
        }
    }

    public function getNameByDepartmentAction()
    {
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101","msg"=>"Information required"));
        }
        else
        {
            if ($this->user_model->isHr() == "yes" )
            {
                $data = $this->input->post();
                echo json_encode($this->hr_model->getNameByDepartmentState($data));
            }
            else
            {
                echo json_encode(array("code"=>"101","msg"=>"Information required"));
            }
        }
    }

    public function balanceView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if ($this->user_model->isHr() == "yes")
            {
                $depart = $this->uri->segment(3);
                $data['content'] = $this->hr_model->getBalanceByDepartState(strtoupper($depart));
                $data['departs'] = $this->hr_model->getAllDeparts();
                $this->load->view("/hr/member_balance",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }

    public function balanceChangeView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if ($this->user_model->isHr() == "yes")
            {
                $data['departs'] = $this->hr_model->getAllDeparts();
                $this->load->view("/hr/balance_change",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }
        }
    }

    public function balanceChangeAction()
    {
        $data = $this->input->post();
        if(!isset($_SESSION['user_state']))
        {

            echo json_encode(array("code"=>"101","msg"=>"Information required"));
        }
        else
        {
            if ($this->user_model->isHr() == "yes" || $_SESSION['user_id'] == $data['id'] || $this->user_model->isRawCheckableState($data))
            {

                echo json_encode($this->hr_model->getLeaveBalanceChangeByWorker($data));
            }
            else
            {

                echo json_encode(array("code"=>"101","msg"=>"Information required"));
            }
        }
    }


}