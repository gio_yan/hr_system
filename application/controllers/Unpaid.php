<?php

/* Author AlexFu
   Time : 2016-12-08
   Motion Global
   Unpaid Controller
*/
class Unpaid extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model("unpaid_model");
		$this->load->model("user_model");
		$this->load->model("leave_model");
		$this->load->model("mail_model");
	}

	public function unPaidLeaveView() // Index View 
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			$data['leave_data'] = $this->unpaid_model->getUnpaidLeaveByMonthState(date("Y-m"));
			$this->load->view("/unpaid/index",$data);
		}
	}

	public function newUnpaidLeaveView() // Generates a new Leave Apply View
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			$this->load->view("/unpaid/new_leave",$data);
		}
	}

	public function unpaidDetailView()
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			$id = $this->uri->segment(3);
			$data= array();
			$data['content'] = $this->unpaid_model->unpaidDetailState($id);
			$data['id'] = $id;
			$this->load->view("unpaid/detail",$data);
		}
	}

	public function editUnpaidAction()
	{
		/*Edit Leave Information */
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101"));
		}
		else
		{
			$data = $this->input->post();
			echo json_encode($this->unpaid_model->editUnpaidState($data));
		}
	}

//	public function pendUnpaidAction()
//	{
//		if(!isset($_SESSION['user_state']))
//		{
//			echo json_encode(array("code"=>"101"));
//		}
//		else
//		{
//			$data = $this->input->post();
//			echo json_encode($this->unpaid_model->pendUnpaidState($data));
//		}
//	}
	public function getUnpaidLeaveByMonthAction()
	{
		//According to the post data to return responding leave data
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array());
		}
		else
		{
			$data = $this->input->post();//var_dump($data);exit;
			$date = date("Y-m",strtotime($data['date']));
			echo json_encode($this->unpaid_model->getUnpaidLeaveByMonthState($date));
		}
	}

	public function cancelUnpaidLeaveAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101"));
		}
		else
		{
			echo json_encode($this->unpaid_model->updateUnpaidApplyCancel($data));
		}
		
	}

	public function newUnpaidLeaveAction()
	{
		$data = $this->input->post();//var_dump($data);exit;
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101"));
		}
		else
		{

			$dayLenth = ($this->leave_model->getActualPayDaysState($data));//var_dump($dayLenth);exit;
			$res = $this->unpaid_model->insertUnpaidLeaveApply($data);//var_dump($res);exit;
			$info = $this->user_model->getUpStreamList();
			//var_dump($this->mail_model->getManagers());exit;
            //var_dump($info);exit;
            $detailInformation = "<br/>It is ".$data['leave_type']." and will begin at "
                .$data['start_time'].", end at ".$data['end_time'].", lasts for ".$data['time_length'].".<br/>The reason is ".$data['reason'];
			if($res['code'] == 200)
			{
				if($dayLenth['amount'] >= 10.0 || in_array($_SESSION['user_id'],$this->mail_model->getManagers()))
				{
					$this->mail_model->send("New Leave Request",Mail_model::$unpaidContent.$detailInformation.Mail_model::$loginCheck,($this->mail_model->getDirectorEmails()),$this->user_model->getUsernameState());
				}
			}
			//var_dump($info);exit;
			$this->mail_model->send("New Unpaid Leave Request",Mail_model::$unpaidContent.$detailInformation.Mail_model::$loginCheck,$info,$this->user_model->getUsernameState());
			echo json_encode($res);
			//echo json_encode($this->unpaid_model->insertUnpaidLeaveApply($data));
		}
	}


}