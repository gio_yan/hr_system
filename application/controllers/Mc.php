<?php
class Mc extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->library('encryption');
        $this->load->model("mc_model");
        $this->load->model("user_model");
        $this->load->model("mail_model");
    }

    public function indexView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
          //  $this->mc_model->test();
            //var_dump(date_diff(date_create("2017-01-01 09:30:00"),date_create("2017-01-01 18:15:00"))->format("%h"));exit;
            $data['mc_log'] = $this->mc_model->getMcLogState();
            $data['mc_submitted'] = $this->mc_model->getMcDataState();
            $this->load->view("/mc/index",$data);
        }
    }

    public function newMcRecordView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            //  $this->mc_model->test();
            $data['date'] = $this->uri->segment(3);
            $this->load->view("/mc/newmc",$data);
        }
    }

    public function newMcRecordAction()
    {
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101"));
        }
        else
        {
            $data = $this->input->post();

            echo json_encode($this->mc_model->newMcRecordState($data));
            $info = $this->user_model->getUpStreamList();
            $this->mail_model->send("New Leave Request",Mail_model::$mcContent,$info);
        }

    }

    public function ignoreRecordAction()
    {
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101"));
        }
        else
        {
            $data = $this->input->post();
            echo json_encode($this->mc_model->ignoreRecordState($data));
        }
    }
}