<?php
class Indexs extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
        $this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model("leave_model");
		$this->load->model("user_model");
	}
	public function index()
	{

		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			$this->load->view('index',$data);
		}
		else
		{
			//$this->user_model->getUpStreamList();
			//If company member table lacks , turns to profile page ,
            //get the info which could be filled by users himself/herself , and check if there is anything that is NULL , if there is , turns to profile page
			if($this->user_model->getIfTurnsToProfile())
            {
                echo "<script>location.href='/user/profile';</script>";
            }
			if($this->user_model->getIfOutBoard())
			{
				$this->load->view("errors/leave_error");
			}
			else
			{
				$this->user_model->updateLeaveDays();
				$data['years'] = $this->leave_model->getYears();
				$data['leave'] = $this->leave_model->getTotalLeaveDays();
				$data['sick'] = $this->leave_model->getTotalSickDays();
				$data['res'] = $this->user_model->getLeaveAndSickDaysState();
				$this->user_model->leaderStateDetected();
			    $temp = $this->user_model->getProfileState(array("user_id"=>$_SESSION['user_id']));
                $data['user_name'] =  $temp['user_name'];
				$this->load->view("main",$data);
			}

		}
	}

	public function getYearDatas()
	{
		$year = $this->input->post("year");
		$data = array();
		$data['leave'] = $this->leave_model->getTotalLeaveDays($year);
		$data['sick'] = $this->leave_model->getTotalSickDays($year);
		echo json_encode($data);
	}



	

}
