<?php
class Leave extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model("leave_model");
		$this->load->model("user_model");
		$this->load->model("mail_model");
	}

	public function leaveView()
	{
		//View used to Show Leave Data By Months
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
		    if($this->user_model->getLeaveVisible())
            {
                echo "<script>location.href='/'</script>";return;
            }
			$data['leave_data'] = $this->leave_model->getLeaveByMonthState(date("Y-m"));
			$data['days'] = $this->user_model->getLeaveAndSickDaysState();
			$this->load->view("/leave/index",$data);
		}
	}

	public function sickLeaveView()
	{
		//View used to Show Sick Leave Data By Months
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
            if($this->user_model->getLeaveVisible())
            {
                echo "<script>location.href='/'</script>";return;
            }
			$data['days'] = $this->user_model->getLeaveAndSickDaysState();
			$data['leave_data'] = $this->leave_model->getSickLeaveByMonthState(date("Y-m"));
			$this->load->view("/sick_leave/index",$data);
		}
	}

	

	public function detailView()
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			$id = $this->uri->segment(3);
			$data= array();
			$data['content'] = $this->leave_model->detailState($id);
			$data['id'] = $id;
			$this->load->view("leave/detail",$data);
		}
	}


	public function sickDetailView()
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			$id = $this->uri->segment(3);
			$data= array();
			$data['content'] = $this->leave_model->detailState($id);
			$data['id'] = $id;
			$this->load->view("sick_leave/detail",$data);
		}
	}
	public function editAction()
	{
		/*Edit Leave Information */
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101"));
		}
		else
		{
			$data = $this->input->post();
			echo json_encode($this->leave_model->editState($data));
		}
	}

//	public function pendAction()
//	{
//		if(!isset($_SESSION['user_state']))
//		{
//			echo json_encode(array("code"=>"101"));
//		}
//		else
//		{
//			$data = $this->input->post();
//			echo json_encode($this->leave_model->pendState($data));
//		}
//	}
	public function getLeaveByMonthAction()
	{
		//According to the post data to return responding leave data
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array());
		}
		else
		{
			$data = $this->input->post();
			$date = date("Y-m",strtotime($data['date']));
			echo json_encode($this->leave_model->getLeaveByMonthState($date));
		}
	}

	public function getSickLeaveByMonthAction()
	{
		//According to the post data to return responding sick leave data
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array());
		}
		else
		{
			$data = $this->input->post();
			$date = date("Y-m",strtotime($data['date']));
			echo json_encode($this->leave_model->getSickLeaveByMonthState($date));
		}
	}

	public function newLeaveView()
	{
		//View used to apply a new Leave Apply Page
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			$this->load->view("/leave/new_leave",$data);
		}
	}


	public function newSickLeaveView()
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			$this->load->view("/sick_leave/new_leave",$data);
		}
	}
	public function newLeaveAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101"));
		}
		else
		{
			$dayLenth = ($this->leave_model->getActualPayDaysState($data));
			$res = $this->leave_model->insertLeaveApply($data);
			$info = $this->user_model->getUpStreamList();
//			var_dump($this->mail_model->getManagers());exit;
            $detailInformation = "<br/>It is ".$data['leave_type']." and will begin at "
                .$data['start_time'].", end at ".$data['end_time'].", lasts for ".$data['time_length'].".<br/>The reason is ".$data['reason'];
			if($res['code'] == 200)
			{
				if($dayLenth['amount'] >= 10.0 || in_array($_SESSION['user_id'],$this->mail_model->getManagers()))
				{
					$this->mail_model->send("New Leave Request",Mail_model::$leaveContent.$detailInformation.Mail_model::$loginCheck,($this->mail_model->getDirectorEmails()),$this->user_model->getUsernameState());
				}
			}
			$this->mail_model->send("New Leave Request",Mail_model::$leaveContent.$detailInformation.Mail_model::$loginCheck,$info,$this->user_model->getUsernameState());
			echo json_encode($res);
		}
	}

	public function cancelLeaveAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101"));
		}
		else
		{
			echo json_encode($this->leave_model->updateApplyCancel($data));
		}
		
	}

	public function getActualPayDaysAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101"));
		}
		else
		{
			echo json_encode($this->leave_model->getActualPayDaysState($data));
		}
	}


	
}