<?php
class Leader extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model("user_model");
		$this->load->model("leader_model");
		$this->load->model("mail_model");
	}

	public function indexView()
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			$this->load->view('main',$data);
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				$data = $this->leader_model->getApplyAllState();
				$this->load->view("leader/index",$data);
			}
			else
			{
				echo "<script>location.href='/'</script>";
			}
			
		}
	}

	public function leaveView()
	{
		$id = $this->uri->segment(2);
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				$data = $this->leader_model->getLeaveApplyState(0);

				$this->load->view("leader/leave",$data);
			}
			else
			{
				echo "<script>location.href='/'</script>";
			}
			
		}
	}

	public function getApproveLeaveByMonthAction()
	{
		$page = $this->input->post("page") ;
		echo json_encode($this->leader_model->getLeaveApplyState($page));
	}
	public function getApproveSickByMonthAction()
	{
		$page = $this->input->post("page") ;
		echo json_encode($this->leader_model->getSickApplyState($page));
	}
	public function getApproveUnpaidByMonthAction()
	{
		$page = $this->input->post("page") ;
		echo json_encode($this->leader_model->getUnpaidApplyState($page));
	}

	public function getApproveAllowanceByMonthAction()
	{
		$page = $this->input->post("page") ;
		echo json_encode($this->leader_model->getAllowanceApplyState($page));
	}
	public function getApproveOvertimeByMonthAction()
	{
		$page = $this->input->post("page") ;
		echo json_encode($this->leader_model->getOvertimeApplyState($page));
	}
	public function getApproveMcByMonthAction()
	{
		$page = $this->input->post("page") ;
		echo json_encode($this->leader_model->getMcApplyState($page));
	}
	public function sickView()
	{
		$id = $this->uri->segment(2);
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				$data = $this->leader_model->getSickApplyState(0);
				$this->load->view("leader/sick",$data);
			}
			else
			{
				echo "<script>location.href='/'</script>";
			}
			
		}
	}

	public function unpaidView()
	{
		$id = $this->uri->segment(2);
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				$data = $this->leader_model->getUnpaidApplyState(0);
				$this->load->view("leader/unpaid",$data);
			}
			else
			{
				echo "<script>location.href='/'</script>";
			}
			
		}
	}

	public function overtimeView()
	{
		$id = $this->uri->segment(2);
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				$data = $this->leader_model->getOvertimeApplyState(0);
				$this->load->view("leader/overtime",$data);
			}
			else
			{
				echo "<script>location.href='/'</script>";
			}
			
		}
	}

	public function allowanceView()
	{
		$id = $this->uri->segment(2);
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				$data = $this->leader_model->getAllowanceApplyState(0);
				$this->load->view("leader/allowance",$data);
			}
			else
			{
				echo "<script>location.href='/'</script>";
			}

		}
	}

	public function getAllLinkedWorkersAction() // It'll be saved to use in the OT
	{
		$data['content'] = $this->leader_model->getSubWorkerState($_SESSION['user_id']);
		$data['state'] = !empty($data['content']);
		echo json_encode($data);
	}

	public function passLeaveAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				//var_dump($this->mail_model->getApplierEmail($data['id'],"leave"));exit;
				$res = ($this->leader_model->passLeaveState($data));
				if($res['code'] == '200') $this->mail_model->send("Request Approved",Mail_model::$approveLeave,array($this->mail_model->getApplierEmail($data['id'],"leave")));
				echo json_encode($res);
			}
			else
			{
				echo json_encode(array("code"=>"101","text"=>"No Permission"));
			}
			
		}
	}

	public function declineLeaveAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				echo json_encode($this->leader_model->declineLeaveState($data));
			}
			else
			{
				echo json_encode(array("code"=>"101","text"=>"No Permission"));
			}
			
		}
	}

	public function passUnpaidAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				//echo json_encode($this->leader_model->passUnpaidState($data));
				$res = ($this->leader_model->passUnpaidState($data));
				if($res['code'] == '200') $this->mail_model->send("Request Approved",Mail_model::$approveLeave,array($this->mail_model->getApplierEmail($data['id'],"unpaid")));
				echo json_encode($res);
			}
			else
			{
				echo json_encode(array("code"=>"101","text"=>"No Permission"));
			}
			
		}
	}

	public function passAllowanceAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				//echo json_encode($this->leader_model->passUnpaidState($data));
				$res = ($this->leader_model->passAllowanceState($data));
				if($res['code'] == '200') $this->mail_model->send("Request Approved",Mail_model::$approveLeave,array($this->mail_model->getApplierEmail($data['id'],"allowance")));
				echo json_encode($res);
			}
			else
			{
				echo json_encode(array("code"=>"101","text"=>"No Permission"));
			}

		}
	}

	public function cancelLeaveAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				//echo json_encode($this->leader_model->cancelLeaveState($data));
				$res = ($this->leader_model->cancelLeaveState($data));
				if($res['code'] == '200') $this->mail_model->send("Request Cancelled",Mail_model::$cancelLeave,array($this->mail_model->getApplierEmail($data['id'],"leave")));
				echo json_encode($res);
			}
			else
			{
				echo json_encode(array("code"=>"101","text"=>"No Permission"));
			}
			
		}
	}

	public function cancelUnpaidAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				//echo json_encode($this->leader_model->cancelUnpaidState($data));
				$res = ($this->leader_model->cancelUnpaidState($data));
				if($res['code'] == '200') $this->mail_model->send("Request Approved",Mail_model::$cancelLeave,array($this->mail_model->getApplierEmail($data['id'],"unpaid")));
				echo json_encode($res);
			}
			else
			{
				echo json_encode(array("code"=>"101","text"=>"No Permission"));
			}
			
		}
	}

    public function cancelAllowanceAction()
    {
        $data = $this->input->post();
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101","text"=>"Please Log in"));
        }
        else
        {
            if($this->user_model->isLeader() == "yes")
            {
                //echo json_encode($this->leader_model->cancelUnpaidState($data));
                $res = ($this->leader_model->cancelAllowanceState($data));
                if($res['code'] == '200') $this->mail_model->send("Request Approved",Mail_model::$cancelLeave,array($this->mail_model->getApplierEmail($data['id'],"allowance")));
                echo json_encode($res);
            }
            else
            {
                echo json_encode(array("code"=>"101","text"=>"No Permission"));
            }

        }
    }

	public function declineUnpaidAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				echo json_encode($this->leader_model->declineUnpaidState($data));
			}
			else
			{
				echo json_encode(array("code"=>"101","text"=>"No Permission"));
			}
			
		}
	}

    public function declineAllowanceAction()
    {
        $data = $this->input->post();
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101","text"=>"Please Log in"));
        }
        else
        {
            if($this->user_model->isLeader() == "yes")
            {
                $res =  ($this->leader_model->declineAllowanceState($data));
                if($res['code'] == '200') $this->mail_model->send("Request Approved",Mail_model::$decline,array($this->mail_model->getApplierEmail($data['id'],"allowance")));
                echo json_encode($res);
            }
            else
            {
                echo json_encode(array("code"=>"101","text"=>"No Permission"));
            }

        }
    }
    public function approveOvertimeAction()
    {
        $data = $this->input->post();
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101","text"=>"Please Log in"));
        }
        else
        {
            if($this->user_model->isLeader() == "yes")
            {
                echo json_encode($this->leader_model->approveOvertimeState($data));
            }
            else
            {
                echo json_encode(array("code"=>"101","text"=>"No Permission"));
            }

        }
    }

	public function cancelOvertimeAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				echo json_encode($this->leader_model->cancelOvertimeState($data));
			}
			else
			{
				echo json_encode(array("code"=>"101","text"=>"No Permission"));
			}
			
		}
	}

	public function leaveDetailView()
	{
		$data = array();
		$id  = $this->uri->segment(4);
		$data['content'] = $this->leader_model->leaveDetailState($id);
		$data['id'] = $id;
		$this->load->view("leader/leave_detail",$data);
	}

	public function sickDetailView()
	{
		$data = array();
		$id  = $this->uri->segment(4);
		$data['content'] = $this->leader_model->leaveDetailState($id);
		$data['id'] = $id;
		$this->load->view("leader/sick_detail",$data);
	}

	public function unpaidDetailView()
	{
		$data = array();
		$id  = $this->uri->segment(4);
		$data['content'] = $this->leader_model->unpaidDetailState($id);
		$data['id'] = $id;
		$this->load->view("leader/unpaid_detail",$data);
	}

    public function allowanceDetailView()
    {
        $data = array();
        $id  = $this->uri->segment(4);
        $data['content'] = $this->leader_model->allowanceDetailState($id);
        $data['id'] = $id;
        $this->load->view("leader/allowance_detail",$data);
    }

	public function overtimeDetailView()
	{
		$data = array();
		$id  = $this->uri->segment(4);
		$data['content'] = $this->leader_model->overtimeDetailState($id);
		$data['id'] = $id;
		$this->load->view("leader/overtime_detail",$data);
	}

	public function mcDetailView()
	{
		$data = array();
		$id  = $this->uri->segment(4);
		$data['content'] = $this->leader_model->mcDetailState($id);
		$data['id'] = $id;
		$this->load->view("leader/mc_detail",$data);
	}

	public function mcView()
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				$data = $this->leader_model->getMcApplyState(0);
				$data['unfilled'] = $this->leader_model->getUnAppliedMc();
				$this->load->view("leader/mc",$data);
			}
			else
			{
				echo "<script>location.href='/'</script>";
			}

		}
	}


	public function approveMcAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				//echo json_encode($this->leader_model->cancelLeaveState($data));
				$res = ($this->leader_model->passMcState($data));
				if($res['code'] == '200') $this->mail_model->send("Request Passed",Mail_model::$approveMc,array($this->mail_model->getApplierEmail($data['id'],"mc")));
				echo json_encode($res);
			}
			else
			{
				echo json_encode(array("code"=>"101","text"=>"No Permission"));
			}

		}
	}
	public function cancelMcAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				//echo json_encode($this->leader_model->cancelLeaveState($data));
				$res = ($this->leader_model->cancelMcState($data));
				if($res['code'] == '200') $this->mail_model->send("Request Cancelled",Mail_model::$cancelMc,array($this->mail_model->getApplierEmail($data['id'],"mc")));
				echo json_encode($res);
			}
			else
			{
				echo json_encode(array("code"=>"101","text"=>"No Permission"));
			}

		}
	}

	public function generalView()
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            if($this->user_model->isLeader() == "yes")
            {
               // $data['general'] = $this->leader_model->generalState();
                $this->load->view("leader/general",$data);
            }
            else
            {
                echo "<script>location.href='/'</script>";
            }

        }
    }

    public function generalAction()
    {
        $data = $this->input->post();
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101","text"=>"Please Log in"));
        }
        else
        {
            if($this->user_model->isLeader() == "yes")
            {


                $res = ($this->leader_model->generalState($data['month']));
                echo json_encode($res);
            }
            else
            {
                echo json_encode(array("code"=>"103","text"=>"No Permission"));
            }

        }
    }

	public function officialView()
	{
		$data = array();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				$data['content'] = ($this->leader_model->officialState());
				$this->load->view("/leader/official",$data);
			}
			else
			{
				echo json_encode(array("code"=>"103","text"=>"No Permission"));
			}

		}
	}

	public function officialDetailView()
	{
		$id = $this->uri->segment(4);
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes" && $this->leader_model->isBelongTo($id))
			{
				$data['content'] = ($this->leader_model->officialDetailState($id));
				$interns = array();
				$interns[] = $data['content']['Internable'];
				$interns[] = $interns[0] == 'no' ? "yes" : "no";
				$data['interns'] = $interns;

				$probations = array();
				$probations[] = $data['content']['Probationable'];
				$probations[] = $probations[0] == 'no' ? "yes" : "no";
				$data['probations'] = $probations;
				$this->load->view("/leader/official_detail",$data);
			}
			else
			{
				echo json_encode(array("code"=>"103","text"=>"No Permission"));
			}

		}
	}

	public function officialEditAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes" && $this->leader_model->isBelongTo($data['id']))
			{
				echo json_encode($this->leader_model->updateOfficialState($data));
			}
			else
			{
				echo json_encode(array("code"=>"103","text"=>"No Permission"));
			}

		}
	}

	public function getUnfilledMcAction()
	{
		$data = $this->input->post();
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes")
			{
				echo json_encode($this->leader_model->getUnAppliedMc($data['month']));
			}
			else
			{
				echo json_encode(array("code"=>"103","text"=>"No Permission"));
			}

		}
	}

	public function rawPanelView()
	{
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if($this->user_model->isLeader() == "yes" )
			{
				$data['content'] = $this->leader_model->getSubWorkerState($_SESSION['user_id']);
				$this->load->view("/leader/raw_data",$data);
			}
			else
			{
				echo json_encode(array("code"=>"103","text"=>"No Permission"));
			}

		}
	}

	public function balanceView()
	{
		if(!isset($_SESSION['user_state']))
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}
		else
		{
			if( $this->user_model->isLeader() == "yes" )
			{
				//$data = array();
				$data['content'] = $this->leader_model->getSubLeaveAndSickDaysState();//var_dump($data);exit;
				$this->load->view("/leader/team_balance",$data);
			}
			else
			{
				echo json_encode(array("code"=>"103","text"=>"No Permission"));
			}

		}
	}

    public function AnniversaryRemind()
    {
        echo json_encode($this->user_model->getAnniversaryThisMonth());
//        echo json_encode(array("code"=>"103","text"=>"No Permission"));
    }

}