<?php

/** Author AlexFu
   Time : 2017-03-10
   Motion Global
   Allowance Controller
*/
class Allowance extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model("allowance_model");
        $this->load->model("user_model");
        $this->load->model("mail_model");
        $this->load->model("leave_model");
    }

    public function allowanceView() // Index View,done
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            $data['leave_data'] = $this->allowance_model->getAllowanceByMonthState(date("Y-m"));
            $this->load->view("/allowance/index",$data);
        }
    }

    public function newAllowanceView() // Generates a new Allowance Apply View,done
    {
        $data = array();
        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            $this->load->view("/allowance/new_allow",$data);
        }
    }

    public function allowanceDetailView() //done
    {
        $data = array();

        if(!isset($_SESSION['user_state']))
        {
            echo "<script>location.href='/'</script>";
        }
        else
        {
            $id = $this->uri->segment(3);
            $data= array();
            $data['content'] = $this->allowance_model->allowanceDetailState($id);
            $data['id'] = $id;
            $this->load->view("allowance/detail",$data);
        }
    }

    public function editAllowanceAction() //done
    {
        /*Edit Leave Information */
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101"));
        }
        else
        {
            $data = $this->input->post();
            echo json_encode($this->allowance_model->editAllowanceState($data));
        }
    }

//	public function pendallowanceAction()
//	{
//		if(!isset($_SESSION['user_state']))
//		{
//			echo json_encode(array("code"=>"101"));
//		}
//		else
//		{
//			$data = $this->input->post();
//			echo json_encode($this->allowance_model->pendallowanceState($data));
//		}
//	}
    public function getAllowanceByMonthAction() //done
    {
        //According to the post data to return responding leave data
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array());
        }
        else
        {
            $data = $this->input->post();
            $date = date("Y-m",strtotime($data['date']));
            echo json_encode($this->allowance_model->getAllowanceByMonthState($date));
        }
    }

    public function cancelAllowanceAction()//done
    {
        $data = $this->input->post();
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101"));
        }
        else
        {
            echo json_encode($this->allowance_model->cancelAllowanceState($data));
        }

    }

    public function newAllowanceAction()//done
    {
        $data = $this->input->post();//var_dump($data);exit;
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101"));
        }
        else
        {
            $dayLenth = ($this->leave_model->getActualPayDaysState($data));//var_dump($dayLenth);exit;
            $res = $this->allowance_model->insertAllowanceApply($data);
            $info = $this->user_model->getUpStreamList();
            //var_dump($this->mail_model->getManagers());exit;
            $detailInformation = "<br/>It is ".$data['leave_type']." and will begin at "
                .$data['start_time'].", end at ".$data['end_time'].", lasts for ".$data['time_length'].".<br/>The reason is ".$data['reason'];
            if($res['code'] == 200)
            {
                if($dayLenth['amount'] >= 10.0 || in_array($_SESSION['user_id'],$this->mail_model->getManagers()))
                {
                    $this->mail_model->send("New Allowance Request",Mail_model::$allowanceContent.$detailInformation.Mail_model::$loginCheck,($this->mail_model->getDirectorEmails()));
                }
            }
            $this->mail_model->send("New allowance Leave Request",Mail_model::$allowanceContent.$detailInformation.Mail_model::$loginCheck,$info,$this->user_model->getUsernameState());
            echo json_encode($res);
            //echo json_encode($this->allowance_model->insertallowanceLeaveApply($data));
        }
    }

    public function getEndTimeByTypeAction()//done
    {
        $data = $this->input->post();//($data);exit;
        if(!isset($_SESSION['user_state']))
        {
            echo json_encode(array("code"=>"101"));
        }
        else
        {
            echo json_encode($this->allowance_model->getEndTimeByTypeState($data));
        }
    }


}