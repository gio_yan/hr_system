<?php
/* Author: Alex Fu
   Time : 2016/12/1
   Motion Global Leave Center

   Controller : User
*/
class User extends CI_Controller
{
	protected static $apikey = "a7f7ad617f2f54a6acd5d51828abcfdc";

	public function __construct()
	{
		parent::__construct();
        $this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->library("email");
		$this->load->model("user_model");
		$this->load->model("mail_model");
	}

	
	
	public function loginAction()
	{ 	
		/*
		Login function Controller , FOR AJAX 
		*/

		if($this->getLoginAction())
		{
			echo json_encode(array("code"=>"200","text"=>"Logged")) ;return;
		}
		$data = $this->input->post();
		echo json_encode($this->user_model->getLoginVerifyState($data));
	}

	public function getLoginAction()
	{
		return isset($_SESSION['user_state']);
	}

	public function registerView()
	{
		if($this->getLoginAction())
		{
			$this->load->view("main");
		}
		else
		{
			$this->load->view("user/register");
		}
		
	}

	public function registerAction()
	{
		$data = $this->input->post();
		//var_dump($this->mail($data));exit;
		$result = $this->user_model->getRegisterState($data);
		echo json_encode($result);
	}

	public function confirmView()
	{
		//echo ($_SESSION['con_user']).$_SESSION['con_pass'];exit;
        $res = $this->user_model->registerConfirmState();
		if($res['code'] == "200" )
		{
			$this->load->view("/user/confirm");
		}
		else
		{
			$this->load->view("/user/wrong",array("text"=>"The registration has overtimed, Please register Again. <br/> <a href='/register/view'>CLICK HERE</a>"));
		}
	}

	public function profileView()
	{
		if($this->getLoginAction())
		{
			//$data = $this->user_model->getProfileState(array("user_id"=>$_SESSION['user_id']));
			$res = $this->user_model->getLeaveAndSickDaysState();//var_dump($_SESSION['user_id']);exit;
			$info_all = ($this->user_model->getBasicInfo());//exit;
			$data = array_merge($info_all,$res);
			$this->load->view("/user/profile",array("data"=>$data));
		}
		else
		{
			echo '<script>location.href="/"</script>';
		}
	}

	public function profileEditAction()
	{
		if($this->getLoginAction())
		{
			$data = $this->input->post();
			$this->user_model->editBasicInfo($data);
			echo json_encode(array("code"=>"200"));
		}
		else
		{
			echo json_encode(array("code"=>"101","text"=>"Please Log in"));
		}

	}

	public function getUsernameAction()
	{
		echo json_encode($this->user_model->getUsernameState());
	}

	public function mail($data)
	{
		$config = array();
		$config['smtp_host'] = 'smtp.emailsrvr.com';
		$config['smtp_user'] = 'itom@motionglobal.com';
		$config['smtp_pass'] = 'motion888';
		$this->email->initialize($config);

		$this->email->from("smtp.emailsrvr.com","MotionGlobal");
		$this->email->to("ryoalex@foxmail.com");
		$this->email->subject("Please confirm You Registration");
		$this->email->message("FFFFF");
		$this->email->send();
		return ($this->email->print_debugger());
	}

	public function  logoutAction()
	{
		unset($_SESSION['user_id']);
		unset($_SESSION['user_state']);
		echo "<script>location.href='/'</script>";
	}

	public function getNavAction()
	{
		echo json_encode($this->user_model->getNavState());
	}


	public function settingsView()
    {
        if($this->getLoginAction())
        {

            $this->load->view("/user/settings");
        }
        else
        {
            echo '<script>location.href="/"</script>';
        }
    }

	public function privateRawDataView()
	{
		$data = array();
		$data['id'] = $_SESSION['user_id'];
		if(!isset($_SESSION['user_state']))
		{
			echo "<script>location.href='/'</script>";
		}
		else
		{
			$this->load->view("/user/private_raw",$data);
		}
	}

    public function alterPassAction()
    {
        $data = $this->input->post();
        echo json_encode($this->user_model->alterPassState($data));
    }

    public function getUnpaidVisibleAction()
    {
        echo json_encode($this->user_model->getUnpaidVisibleState());
    }


    public function forgetPassAction()
    {
        $data = $this->input->post();
        $user = $this->user_model->forgetPassState($data);
        if(!isset($user['code']))
        {
            $this->mail_model->send("Password Reset Result","Your password has been reset to ".$user['pass']."<br/>Please login and modify your password",array($user));
            echo json_encode(array("code"=>"200"));
        }
        else
        {
            echo json_encode($user);
        }
    }

    public function resetView()
    {
        $this->load->view("user/reset");
    }
    public function testView()
    {
//    	$data = array();
//    	$res = ($this->testPasswordResetAction());
//    	foreach($res as $r=>$v)
//        {
//            $this->mail_model->send("Password Reset Result","Your password has been reset to ".$v['pass']."<br/>Please login and modify your password",array($v));
//        }
//    	$data['data'] = $this->user_model->testState();
//    	//$this->load->view("test",$data);
		//var_dump($this->user_model->getUpStreamList());
		echo json_encode($this->mail_model->dealMail());
    }

    public function testPasswordResetAction()
    {
        return $this->user_model->testPasswordResetState();
    }

    public function tranAction()
    {
        echo ($this->user_model->transState());
    }
}


   
?>