<?php
require_once(getcwd().'/application/models/SS_model.php');

class Unpaid_model extends SS_model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->library('encryption');
	}




	public function getUnpaidLeaveByMonthState($date) //Get Unpaid By Month
	{
		$sql = 'SELECT `auto_id`,  DATE_FORMAT(`create_time`,"%Y-%m-%d %H:%i") as create_time, DATE_FORMAT(`start_time`,"%Y-%m-%d %H:%i") as start_time, DATE_FORMAT(`end_time`,"%Y-%m-%d %H:%i") as end_time, `time_length`,  `state`,leave_type FROM `unpaid_leave_apply` WHERE applier_id = ? AND DATE_FORMAT(start_time,"%Y-%m") = ? AND (state = "pending" OR state = "approved_by_mgr")  ORDER BY start_time DESC ;';
		return $this->db->query($sql,array($_SESSION['user_id'],$date))->result_array();
	}


	public function _updateUnpaidApplyCancel($data) //Cancel the apply
	{
		if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave')
		{
			return array("code"=>"102","text"=>"wrong leave type");
		}
		$sql = 'SELECT auto_id FROM unpaid_leave_apply WHERE auto_id = ? AND applier_id = ? AND state = ? ;';
		$res = $this->db->query($sql,array($data['id'],$_SESSION['user_id'],"pending"))->row_array();
		if(isset($res['auto_id']))
		{
			$sql = "UPDATE unpaid_leave_apply SET state = 'cancelld' WHERE auto_id = ? ;";
			$this->db->query($sql,array($data['id']));

			$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";

			$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"cancel-unpaid-".$data['leave_type'],date("Y-m-d H:i:s"),0,0));

			return array("code"=>"200");
		}
		return array("code"=>"102","text"=>"Cancel Operation has been denyed,While the manager/hr has approved your request  OR it is on cancelld state");
	}
	public function updateUnpaidApplyCancel($data)
	{
		return $this->_redisKeyInterFace("_updateUnpaidApplyCancel","_updateUnpaidApplyCancel",$data);
	}


	public function pendUnpaidState($data)
	{
		if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave')
		{
			return array("code"=>"102","text"=>"wrong leave type");
		}
		$sql = 'SELECT auto_id FROM unpaid_leave_apply WHERE auto_id = ? AND applier_id = ? AND state = ? ;';
		$res = $this->db->query($sql,array($data['id'],$_SESSION['user_id'],"cancelld"))->row_array();
		if(isset($res['auto_id']))
		{
			$sql = "UPDATE unpaid_leave_apply SET state = 'pending' WHERE auto_id = ? ;";
			$this->db->query($sql,array($data['id']));

			$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";

			$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"re-pend-".$data['leave_type'],date("Y-m-d H:i:s"),0,0));



			return array("code"=>"200");
		}
		return array("code"=>"102","text"=>"Pending Operation has been denyed,While the manager/hr has approved your request OR it is on pending state");
	}

	public function insertUnpaidLeaveApply($data)
	{
		return ($this->_insertUnpaidLeaveApply($data));//exit;
		//return $this->_redisKeyInterFace("_insertUnpaidLeaveApply","_insertUnpaidLeaveApply",$data);
	}

	public function _insertUnpaidLeaveApply($data)
	{

		if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave')
		{
			return array("code"=>"101","text"=>"wrong leave type");
		}
		if(strtotime($data['start_time']) > strtotime($data['end_time']) || empty($data['end_time']) || empty($data['start_time']) || empty($data['time_length'])  )
		{
			return array("code"=>"102","text"=>"Please enter Leave Time in correct format");
		}
		$data['reason'] = empty($data['reason']) ? "" : $data['reason'];
		//Insert new Leave

		$data['reason'] = trim($data['reason']);
		$sql = "INSERT INTO `unpaid_leave_apply`(`auto_id`, `applier_id`, `create_time`, `start_time`, `end_time`, `time_length`, `hr_id`, `leave_type`, `reason`, `state`,`doctor_notes`) VALUES (null,?,?,?,?,?,?,?,?,?,?) ;";
		$this->db->query($sql,array($_SESSION['user_id'],date("Y-m-d H:i:s"),$data['start_time'],$data['end_time'],$data['time_length'],0,$data['leave_type'],$data['reason'],"pending",$data['doctor_notes']));

		//Insert leave  log 
		$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";

		$this->db->query($sql ,array($_SESSION['user_id'],0,"pend-unpaid-".$data['leave_type'],date("Y-m-d H:i:s"),0,0));

		return array("code"=>"200");//exit;
	}



	public function unpaidDetailState($id)
	{
		$sql = "SELECT * FROM unpaid_leave_apply WHERE auto_id = ? AND applier_id = ? ;";
		return $this->db->query($sql,array($id,$_SESSION['user_id']))->row_array();
	}
	public function getNameAndMgrMail($id)
	{
		$sql = "SELECT mine.user_name ,mgr.user_id as `to` FROM profile as mine LEFT OUTER JOIN leader ON mine.user_id = leader.user_id LEFT OUTER JOIN users as mgr ON leader.leader_id = mgr.auto_id WHERE mine.user_id = ?;";
		return $this->db->query($sql,array($id))->row_array();
	}

	public function editUnpaidState($data)
	{
		if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave')
		{
			return array("code"=>"102","text"=>"wrong leave type");
		}
		$data['reason'] = empty($data['reason']) ? "" : $data['reason'];

		$sql = 'SELECT auto_id,leave_type,start_time,end_time FROM unpaid_leave_apply WHERE auto_id = ? AND applier_id = ? AND state = ? OR state = ?  ;';
		$res = $this->db->query($sql,array($data['id'],$_SESSION['user_id'],"pending","cancelld"))->row_array();

		if(isset($res['auto_id']))
		{
			$sql = "UPDATE unpaid_leave_apply SET start_time = ? ,end_time = ? ,time_length = ?,reason = ?, doctor_notes = ?  WHERE auto_id = ? ;";
			$this->db->query($sql,array($data['start_time'],$data['end_time'],$data['time_length'],$data['reason'],$data['doctor_notes'],$data['id']));

			$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";

			$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"edit-unpaid-".$data['leave_type'],date("Y-m-d H:i:s"),0,0));
			return array("code"=>"200");
		}
		return array("code"=>"102","text"=>"Edit Operation has been denyed,While the manager/hr has approved your request!");
	}
}