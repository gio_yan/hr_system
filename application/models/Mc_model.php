<?php
require_once(getcwd().'/application/models/SS_model.php');

class Mc_model extends SS_model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->library('encryption');
    }


    public function test()
    {
        $sql = "SELECT name ,work_email,users.auto_id FROM company_members LEFT OUTER JOIN users ON company_members.work_email = users.user_id ORDER BY users.auto_id ";
        $res = $this->db->query($sql)->result_array();
        foreach($res as $da)
        {
            $sql = "UPDATE profile SET user_name = ? WHERE user_id = ? ;";
            $this->db->query($sql,array($da['name'],$da['auto_id']));
        }
    }
    public function getMcLogState($start= '2006-12-01',$end = '2050-12-30')
    {
        $sql = "SELECT m.*,p.user_id FROM missing_card_log as m LEFT OUTER JOIN profile  as p ON  m.name = p.user_name WHERE p.user_id = ? AND record_date >= ? AND record_date <= ? ;";
        $res = $this->db->query($sql,array($_SESSION['user_id'],$start,$end))->result_array();//var_dump($res);exit;
        $holidays = $this->getHolidayDates();
//        var_dump($holidays);exit;
//        var_dump(in_array("2017-01-02",$holidays));exit;
        foreach($res as $r=>$v)
        {
            if($v['check_in'] != "00:00:00" && $v['check_out'] != "00:00:00" && date_diff(date_create($v['record_date']." ".$v['check_in']),date_create($v['record_date']." ".$v['check_out']))->format("%h") >=9/*$v['check_in'] <="10:00" && $v['check_out'] >= "18:30"*/ )
            {
                unset($res[$r]);
            }
            if( date('w',strtotime($v['record_date'])) == 6 || date('w',strtotime($v['record_date'])) == 0 || in_array($v['record_date'],$holidays) )
            {
                unset($res[$r]);
            }
        }

        $logged = $this->getMcDataState($start,$end);
        foreach($res as $r =>$v)
        {
            foreach($logged as $log)
            {
                if($log['record_date'] == $v['record_date'] && ($log['state'] == "approved" || $log['state'] == 'ignored'))
                {
                    unset($res[$r]);
                }
            }
        }

        return $res;
    }

    public function getMcDataState($start= '2006-12-01',$end = '2050-12-30')
    {
        $sql = 'SELECT DATE_FORMAT(start_time,"%Y-%m-%d") as record_date,DATE_FORMAT(start_time,"%H:%i") as check_in , DATE_FORMAT(end_time,"%H:%i") as check_out,state FROM missing_card WHERE user_id = ? AND DATE_FORMAT(end_time,"%Y-%m-%d") >= ? AND DATE_FORMAT(end_time,"%Y-%m-%d") <= ? ;';
        $res = $this->db->query($sql,array($_SESSION['user_id'],$start,$end))->result_array();
        return $res;
    }

    public function newMcRecordState($data)
    {
        //var_dump($data['date']);exit;
        $sql = "INSERT INTO `missing_card`(`auto_id`, `user_id`, `start_time`, `end_time`, `reason`, `record_time`) VALUES (?,?,?,?,?,?) ;";
        $start = date("Y-m-d H:i:s",strtotime($data['date']." ".$data['start_time'].":00"));
        $end = date("Y-m-d H:i:s",strtotime($data['date']." ".$data['end_time'].":00"));
       // var_dump($start);exit;
        $this->db->query($sql,array(null,$_SESSION['user_id'],$start,$end,$data['reason'],date("Y-m-d H:i:s")));
        return array("code"=>"200");
    }

    public function getHolidayDates()
    {
        $sql = "SELECT holiday_date FROM holiday_year WHERE 1 ;";
        $res = $this->db->query($sql)->result_array();
        $out = array();
        foreach ($res as $r => $v)
        {
            $out[] = $v['holiday_date'];
        }
        return $out;
    }

    public function ignoreRecordState($data)
    {
        $sql = "INSERT INTO `missing_card`(`auto_id`, `user_id`, `start_time`, `end_time`, `reason`, `record_time`,`state`) VALUES (?,?,?,?,?,?,?) ;";
        $start = date("Y-m-d H:i:s",strtotime($data['date']." 09:30:00"));
        $end = date("Y-m-d H:i:s",strtotime($data['date']." 18:30:00"));
        // var_dump($start);exit;
        $this->db->query($sql,array(null,$_SESSION['user_id'],$start,$end,"asked for leave before",date("Y-m-d H:i:s"),"ignored"));
        return array("code"=>"200");
    }


}