<?php
require_once(getcwd().'/application/models/SS_model.php');
class Hr_model extends SS_model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->library('encryption');
    }

    public function getAllDeparts()
    {
        $sql = 'SELECT DISTINCT(department) as departs FROM profile WHERE 1';
        $res = $this->db->query($sql)->result_array();
        //return $res;
        foreach($res as $r => $v)
        {
            if(strpos($v['departs'],",") >0 )
            {
                unset($res[$r]);
            }
        }
        return $res;
    }

    public function getEmployeeDataByDepartState($data)
    {
        $sql = 'SELECT DISTINCT(users.auto_id) ,user_name,users.user_id ,department,profile.title FROM profile LEFT OUTER JOIN users ON profile.user_id = users.auto_id LEFT OUTER JOIN leave_days ON users.auto_id = leave_days.user_id LEFT OUTER JOIN company_members ON users.user_id = company_members.work_email WHERE department = ? AND last_day IS NULL ORDER BY profile.user_name ASC;';
        return $this->db->query($sql,array($data['depart']))->result_array();
    }

    public function getResignedEmployeeDataByDepartState($data)
    {
        $sql = 'SELECT DISTINCT(users.auto_id) ,user_name,users.user_id ,department,profile.title,company_members.last_day FROM profile LEFT OUTER JOIN users ON profile.user_id = users.auto_id LEFT OUTER JOIN leave_days ON users.auto_id = leave_days.user_id LEFT OUTER JOIN company_members ON users.user_id = company_members.work_email WHERE department = ? AND last_day IS NOT NULL ORDER BY cal_year DESC;';
        return $this->db->query($sql,array($data['depart']))->result_array();
    }

    public function getEmployeeAllState($id)
    {
        if(!is_numeric($id)) return array("code"=>"101","content"=>"please enter id in right format");
        $sql = "SELECT u.auto_id,p.title,p.department,p.Internable,p.Probationable,c.* FROM users as u LEFT OUTER JOIN profile as p ON u.auto_id = p.user_id
              LEFT OUTER JOIN company_members as c ON u.user_id = c.work_email WHERE u.auto_id = ? ;";
        return $this->db->query($sql,array($id))->row_array();
    }

    public function updateEmployeeState($data)
    {
        $departs = $this->getAllDeparts();
        //var_dump($departs);
        $depart_flag = false;
        //var_dump($data);exit;
        foreach($departs as $depart)
        {
            if($data['department'] == $depart['departs']) $depart_flag = true;
        }
        if(!$depart_flag) return array("code"=>"101","msg"=>"Department Wrong");
        if(!strtotime($data['start_date']) || !strtotime($data['birthday'])) return array("code"=>"102","msg"=>"Date Format Wrong");

        $email = $this->db->query("SELECT user_id FROM users WHERE auto_id = ? ;",array($data['id']))->row_array();
        $probation_date = $data['probation_date'];

        if($data['probation'] == "yes") //set all leave_days to 0
        {
            $probation_date = "0000-00-00";
        }
        else
        {
            $res = $this->db->query("SELECT Probationable FROM profile WHERE user_id = ? ;",array($data['id']))->row_array();//var_dump($res);exit;
            if($res['Probationable'] == "yes")
            {
                //    $probation_date = date("Y-m-d");
                $this->db->query("UPDATE leave_days SET leave_balance = 10,sick_leave_balance = 3.5 WHERE user_id = ? ;",array($data['id']));

            }
        }


        if($data['intern'] == "yes") //set all leave_days to 0
        {
            $probation_date = "0000-00-00";
        }
        else
        {
            $res = $this->db->query("SELECT Internable FROM profile WHERE user_id = ? ;",array($data['id']))->row_array();//var_dump($res);exit;
            if($res['Internable'] == "yes")
            {
                //  $probation_date = date("Y-m-d");
                $this->db->query("INSERT INTO leave_days (user_id,cal_year,leave_balance,sick_leave_balance) VALUES (?,?,?,? ) ;",array($data['id'],$probation_date,10,3.5));

            }
        }
        if(isset($data['last_day']) && !empty($data['last_day']))
        {
            $sql = "UPDATE `company_members` SET `chinese_name`=?,`phone_number`=?,`moblie_number`=?,`personal_email`=?,`skype`=?,`work_location`=?,`start_date`=?,`birthday`=?,`home_address`=?,`probation_date` = ?,`last_day`=? WHERE work_email = ? ;";
            $this->db->query($sql,array($data['chinese_name'],$data['phone_number'],$data['moblie_number'],
                $data['personal_email'],$data['skype'],$data['work_location'],$data['start_date'],$data['birthday'],$data['home_address'],$probation_date,$data['last_day'],$email));
        }
        else
        {
            $sql = "UPDATE `company_members` SET `chinese_name`=?,`phone_number`=?,`moblie_number`=?,`personal_email`=?,`skype`=?,`work_location`=?,`start_date`=?,`birthday`=?,`home_address`=?,`probation_date` = ? WHERE work_email = ? ;";
            $this->db->query($sql,array($data['chinese_name'],$data['phone_number'],$data['moblie_number'],
                $data['personal_email'],$data['skype'],$data['work_location'],$data['start_date'],$data['birthday'],$data['home_address'],$probation_date,$email));
        }








        $sql = "UPDATE profile SET title = ? ,department = ?,Internable = ?,Probationable = ? WHERE user_id = ? ;";
        $this->db->query($sql , array($data['title'],$data['department'],$data['intern'],$data['probation'],$data['id']));
        return array("code"=>"200");
    }

    public function newEmployeeState($data)
    {
        return $this->_redisKeyInterFace("_newEmployeeState","_newEmployeeState",$data);

    }

    public function _newEmployeeState($data)
    {   //var_dump($data);return array("code"=>"100");
        if(empty($data['work_email'])|| empty($data['name'])||
            empty($data['title'])|| empty($data['department']) )
        {
            return array("code"=>"101","msg"=>"Please fill basic information of the new employed");
        }
        if(!array_key_exists("work_email",preg_grep("/^[0-9a-zA-z\.]+@motionglobal.com$/",$data)))
        {
            return array("code"=>"102","msg"=>"Mail Format illegal");
        }
        $res = $this->db->query("SELECT auto_id,user_id FROM users WHERE user_id = ?",$data['work_email'])->row_array();
        if (isset($res['auto_id']))  return array("code"=>"104","msg"=>"Employe existed");
        $sql = "INSERT INTO users (auto_id,user_id,passwords,mail,is_hr,is_leader) VALUES(null,?,?,?,'no','no');";
        $this->db->query($sql,array($data['work_email'],sha1("motion@global123"),$data['work_email']));
        $res = $this->db->query("SELECT auto_id FROM users WHERE user_id = ?",array($data['work_email']))->row_array();
        $sql = "INSERT INTO `profile`(`user_id`, `user_name`, `title`, `department`,Internable,Probationable) VALUES (?,?,?,?,?,?);";
        $this->db->query($sql,array($res['auto_id'],$data['name'],$data['title'],$data['department'],$data['intern'],$data['probation']));
        $sql = "INSERT INTO `company_members`(`name`, `chinese_name`, `work_email`, `phone_number`, `moblie_number`,
 `personal_email`, `skype`, `work_location`, `start_date`,`probation_date`, `birthday`, `home_address`, `last_day`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";

        if($data['probation'] == "yes")
        {
            $data['probation_date'] = "0000-00-00";
        }
        else
        {
            $data['probation_date'] = $data['start_date'];
        }
        $this->db->query($sql,array($data['name'],$data['chinese_name'],$data['work_email'],$data['phone_number'],$data['moblie_number'],
            $data['personal_email'],$data['skype'],$data['work_location'],$data['start_date'],$data['probation_date'],$data['birthday'],$data['home_address'],null));
        $date = empty($data['start_date']) ? date("Y-m-d") :$data['start_date'];
        if($data['probation'] == "no" && $data['intern'] == 'no')
        {
            $this->db->query("INSERT INTO leave_days (user_id,cal_year,leave_balance,sick_leave_balance) VALUES (?,?,?,? ) ;",array($res['auto_id'],$date,10,3.5));
        }
        else if($data['probation'] == "yes" && $data['intern'] == 'no')
        {
            $this->db->query("INSERT INTO leave_days (user_id,cal_year,leave_balance,sick_leave_balance) VALUES (?,?,?,? ) ;",array($res['auto_id'],$date,0,0));
        }
        else if($data['probation'] == "yes" && $data['intern'] == 'yes')
        {
            return array("code"=>"102","msg"=>"One could only be probation Or Intern, please do not choose both, thanks");
        }
        else
        {

        }
        $this->db->query("INSERT INTO leader (user_id,leader_id) VALUES (?,?)",array($res['auto_id'],$res['auto_id']));
        return array("code"=>"200");
    }

    public function getRelations($page = 0)
    {
        if(!is_numeric($page))
        {
            return array('code'=>'101' ,'content' => 'Illegal page');
        }

        $pageSize  = 10;
        $start = $page - 3;
        $end = $page + 3;
        $res = array();



        $sql = 'SELECT COUNT(em_co.name) as num FROM leader
LEFT OUTER JOIN users as em_u ON leader.user_id = em_u.auto_id
LEFT OUTER JOIN company_members as em_co ON em_u.user_id = em_co.work_email
LEFT OUTER JOIN profile as em_pr ON em_u.auto_id = em_pr.user_id
LEFT OUTER JOIN users as ma_u ON leader.leader_id = ma_u.auto_id
LEFT OUTER JOIN company_members as ma_co ON ma_u.user_id = ma_co.work_email
LEFT OUTER JOIN profile as ma_pr ON ma_u.auto_id = ma_pr.user_id WHERE em_co.last_day IS NULL ORDER BY em_pr.department';
        $result = $this->db->query($sql)->row_array();
        $page_sum = isset($result['num']) ? $result['num'] : 0;


        if($page_sum/$pageSize < $page)
        {
            return array('code'=>'102' ,'content' => 'Illegal page');
        }


        if($start < 0 ) $start = 0;
        if($end > $page_sum/$pageSize ) $end = ceil(1.0*$page_sum/$pageSize);

        if($end == $start)
        {
            $res['pages'] = array(0);
        }
        else
        {
            $res['pages'] = range($start,($end-1)<=0 ? 0 : ($end-1));
        }
        $sql = 'SELECT em_co.name as em_name,em_u.auto_id as em_id,em_pr.department as em_depart, ma_co.name as ma_name,ma_u.auto_id as ma_id,ma_pr.department as ma_depart FROM leader
LEFT OUTER JOIN users as em_u ON leader.user_id = em_u.auto_id
LEFT OUTER JOIN company_members as em_co ON em_u.user_id = em_co.work_email
LEFT OUTER JOIN profile as em_pr ON em_u.auto_id = em_pr.user_id
LEFT OUTER JOIN users as ma_u ON leader.leader_id = ma_u.auto_id
LEFT OUTER JOIN company_members as ma_co ON ma_u.user_id = ma_co.work_email
LEFT OUTER JOIN profile as ma_pr ON ma_u.auto_id = ma_pr.user_id WHERE em_co.last_day IS NULL
AND em_co.name IS NOT NULL AND em_u.auto_id IS NOT NULL ORDER BY em_pr.department LIMIT ?,?;';
        $result =  $this->db->query($sql,array($pageSize*$page,$pageSize))->result_array();

        $res['content'] = $result;
        $res['page'] = $page;
        $res['all'] = ceil(1.0*$page_sum/$pageSize);
        return $res;
    }

    public function updateRelationState($data)
    {
        if(!isset($data['ma_id']) || !isset($data['em_id'])) return array("code"=>"101");
        $sql = "UPDATE leader SET leader_id= ? WHERE user_id = ? ;";
        $this->db->query($sql,array($data['ma_id'],$data['em_id']));
        return array("code"=>"200");
    }

    public function getLeftEmployeeState($start = "2006-01-01",$end = "2020-12-31")
    {
        $sql = "SELECT auto_id,name,work_email,last_day,department FROM company_members LEFT OUTER JOIN users ON company_members.work_email = users.user_id LEFT OUTER JOIN profile ON users.auto_id = profile.user_id  WHERE last_day >= ? AND last_day <= ?";
        $res =  $this->db->query($sql,array($start,$end))->result_array();
        $holidays = $this->getHolidayDates();

        foreach ($res as $r => $v)
        {
            $amount = 0;
            $s = date_format(date_create(date_format(date_create($v['last_day']),"Y-m")."-01"),"Y-m-d");
            foreach ($holidays as $day)
            {

                if($day >= $s  && $day < $v['last_day'])
                {
                    $amount++;
                }
            }
            $res[$r]['attendance'] = $this->getMcLogState($s ,$v['last_day'],$v['auto_id']);
            $res[$r]['attendance'] += $amount;
            //var_dump($res[$r]['attendance'],$v['auto_id']);
            $res[$r]['balance'] = $this->getBalance($v['auto_id'],$v['last_day']);
            $res[$r]['balance'] = $res[$r]['balance']['balance'];
        }
        return $res;
    }

    public function getMcLogState($start= '2006-12-01',$end = '2020-12-30',$user_id= "0")
    {
        $sql = "SELECT m.* FROM missing_card_log as m LEFT OUTER JOIN profile as p ON m.name = p.user_name WHERE p.user_id = ? AND record_date >= ? AND record_date <= ? ;";
        $i = date_format(date_create(date_format(date_create($end),"Y-m")."-01"),"Y-m-d") ;
        // var_dump($i,$end);
        $res = $this->db->query($sql,array($user_id,$i,$end))->result_array();

        $holidays = $this->getHolidayDates();
        foreach($res as $r=>$v)
        {
//            if($v['check_in'] == "00:00:00" || $v['check_out'] == "00:00:00")
//            {
//                unset($res[$r]);
//            }
            if( date('w',strtotime($v['record_date'])) == 6 || date('w',strtotime($v['record_date'])) == 0 || in_array($v['record_date'],$holidays) )
            {
                unset($res[$r]);
            }
        }
        // var_dump(count($res),$user_id);
//        $logged = $this->getMcDataState($start,$end,$user_id);
//        foreach($res as $r =>$v)
//        {
//            foreach($logged as $log)
//            {
//                if($log['record_date'] == $v['record_date'])
//                {
//                    unset($res[$r]);
//                }
//            }
//        }

        return count($res);
    }

    public function getHolidayDates()
    {
        $sql = "SELECT holiday_date FROM holiday_year WHERE 1 ;";
        $res = $this->db->query($sql)->result_array();
        $out = array();
        foreach ($res as $r => $v)
        {
            $out[] = $v['holiday_date'];
        }
        return $out;
    }

    public function getMcDataState($start= '2006-12-01',$end = '2020-12-30',$user_id = 0)
    {
        $sql = 'SELECT DATE_FORMAT(start_time,"%Y-%m-%d") as record_date,DATE_FORMAT(start_time,"%H:%i") as check_in , DATE_FORMAT(end_time,"%H:%i") as check_out FROM missing_card WHERE user_id = ? AND DATE_FORMAT(end_time,"%Y-%m-%d") >= ? AND DATE_FORMAT(end_time,"%Y-%m-%d") <= ? ;';
        $res = $this->db->query($sql,array($user_id,$start,$end))->result_array();
        return $res;
    }

    public function getBalance($user_id,$last_day)
    {
        $ba0 =  $this->db->query("SELECT (leave_balance) as balance,cal_year FROM leave_days WHERE user_id = ? ORDER BY cal_year DESC LIMIT 0,1",array($user_id))->row_array();
        $ba1 =  $this->db->query("SELECT (leave_balance) as balance FROM leave_days WHERE user_id = ? ORDER BY cal_year DESC LIMIT 1,1",array($user_id))->row_array();
        $ba1 = $ba1['balance'];
        $val = $ba0['balance']*1.0*ceil((strtotime($last_day)-strtotime($ba0['cal_year']))/3600/24/30)/12;
        if ($val - floor($val) >= 0.75) {
            $val = ceil($val);
        }else if ($val - floor($val) > 0.25) {
            $val = floor($val) + 0.5;
        }else {
            $val = floor($val);
        }
        return array("balance"=>$val+$ba1);
    }

    public function getHeadCount($start_month = "",$end_month = "")
    {
        // var_dump("FFFFF");exit;
        $months = array();
        $date_now = date_create(date("Y-m-d"));
        date_add($date_now,date_interval_create_from_date_string("1 month"));
        $headCounts = array();
        $headCounts['new'] = $headCounts['leave'] = $headCounts['now'] = array();
        for($i=0;$i<8;$i++)
        {
            $months[] = date_format($date_now,"Y-m");
            date_sub($date_now,date_interval_create_from_date_string("1 month"));
        }

        $sql_new = "SELECT COUNT(company_members.name) as num ,department FROM company_members 
                    LEFT OUTER JOIN users ON company_members.work_email = users.user_id 
                    LEFT OUTER JOIN profile ON users.auto_id = profile.user_id WHERE start_date >=? AND start_date < ? GROUP BY department;"; //Month start && Next Month start

        $sql_leave = "SELECT COUNT(company_members.name) as num ,department FROM company_members 
                    LEFT OUTER JOIN users ON company_members.work_email = users.user_id 
                    LEFT OUTER JOIN profile ON users.auto_id = profile.user_id WHERE last_day >=? AND last_day < ? GROUP BY department;"; //Month start && Next Month Start

        $sql_now = "SELECT COUNT(company_members.name) as num ,department FROM company_members LEFT OUTER JOIN users ON company_members.work_email = users.user_id LEFT OUTER JOIN profile ON users.auto_id = profile.user_id WHERE start_date < ? AND (last_day IS NULL OR  last_day >= ?  ) GROUP BY department"; // Month start && Next Month start

        $departs = array("CS","IT","SC","MC","HR","FIN","MKT","BI","Director");
        for($i=1;$i<7;$i++)
        {
            $new_temp = $now_temp = $leave_temp =  array("CS"=>0,"IT"=>0,"SC"=>0,"MC"=>0,"HR"=>0,"FIN"=>0,"MKT"=>0,"BI"=>0,"Director"=>0);
            $headCounts['new'][$months[$i]] = $this->db->query($sql_new,array($months[$i]."-01",$months[$i-1]."-01"))->result_array();
            foreach ($headCounts['new'][$months[$i]] as $news)
            {
                if(in_array($news['department'],$departs))
                {
                    $new_temp[$news['department']] = $news['num'];
                }
            }
            $headCounts['new'][$months[$i]] = $new_temp;

            $headCounts['now'][$months[$i]] = $this->db->query($sql_now,array($months[$i]."-01",$months[$i-1]."-01"))->result_array();
            foreach ($headCounts['now'][$months[$i]] as $news)
            {

                if(in_array(trim($news['department']),$departs))
                {
                    $now_temp[$news['department']] = $news['num'];
                }
            }
            $headCounts['now'][$months[$i]] = $now_temp;

            $headCounts['leave'][$months[$i]] = $this->db->query($sql_leave,array($months[$i]."-01",$months[$i-1]."-01"))->result_array();
            foreach ($headCounts['leave'][$months[$i]] as $news)
            {
                if(in_array($news['department'],$departs))
                {
                    $leave_temp[$news['department']] = $news['num'];
                }
            }
            $headCounts['leave'][$months[$i]] = $leave_temp;
        }
        //     var_dump($headCounts['now']);exit;
        return $headCounts;
    }


    public function employeeReportState($start ,$end,$depart='',$user ='')
    {
        $i = 0;
        $res = array();
        //Get Work Year
        $sql = "SELECT department,user_name,DATE_FORMAT(start_time,'%Y-%m-%d') as s_date ,time_length,leave_type,reason FROM leave_apply LEFT OUTER JOIN `profile` ON leave_apply.applier_id = profile.user_id WHERE start_time >= ? AND start_time <= ? AND state = ? ORDER BY department ASC ,user_name ASC;";
        $leave = $this->db->query($sql,array($start,$end,"approved_by_mgr"))->result_array();

        foreach($leave as $item)
        {
            $res[$i] = array("Department"=>$item['department'],"Name"=>$item['user_name'],"Date"=>$item['s_date'],"Check_in"=>"","Check_out"=>"","Annual_leave"=>"","Sick_leave"=>"","Overtime_Al"=>"","Overtime_paid"=>"","Unpaid"=>"","Missing_card"=>"","Allowance"=>"","Reason"=>$item['reason']);
            if($item['leave_type'] == "leave") $res[$i]['Annual_leave'] = $item['time_length'];
            else $res[$i]['Sick_leave'] = $item['time_length'];
            $i++;
        }

        $sql = "SELECT department,user_name,DATE_FORMAT(start_time,'%Y-%m-%d') as s_date ,time_length,reason,leave_type,doctor_notes,profile.Internable,profile.Probationable FROM unpaid_leave_apply LEFT OUTER JOIN `profile` ON unpaid_leave_apply.applier_id = profile.user_id WHERE start_time >= ? AND start_time <= ? AND state = ? ORDER BY department ASC ,user_name ASC;";
        $unpaid = $this->db->query($sql,array($start,$end,"approved_by_mgr"))->result_array();

        foreach($unpaid as $item)
        {
            if($item['leave_type'] == "sick_leave")
            {
                $year = $this->db->query("SELECT TIMESTAMPDIFF(YEAR,start_date,DATE_FORMAT(NOW(),'%Y-%m-%d')) as year FROM company_members WHERE name = ?;",array($item['user_name']))->row_array();
                $year = $year['year'];
                if($item['doctor_notes'] == 'yes' && $item['Internable'] == 'no' && $item['Probationable'] == 'no'/*intern & probation ����*/)
                    $item['time_length'] = sick_transfer($item['time_length'],$year)."d";
            }
            $res[$i] = array("Department"=>$item['department'],"Name"=>$item['user_name'],"Date"=>$item['s_date'],"Check_in"=>"","Check_out"=>"","Annual_leave"=>"","Sick_leave"=>"","Overtime_Al"=>"","Overtime_paid"=>"","Unpaid"=>$item['time_length'],"Missing_card"=>"","Allowance"=>"","Reason"=>$item['reason']);
            $i++;
        }
        $sql = "SELECT department,user_name,DATE_FORMAT(start_time,'%Y-%m-%d') as s_date,time_length ,compension,pay_or_day FROM overtime_apply LEFT OUTER JOIN `profile` ON overtime_apply.employee_id = profile.user_id WHERE start_time >= ? AND start_time <= ?  AND state = ? ORDER BY department ASC ,user_name ASC;";
        $overtime = $this->db->query($sql,array($start,$end,"approved_by_mgr"))->result_array();

        foreach($overtime as $item)
        {
            $item['compension'] = $item['time_length'];//floatval($item['compension']) * floatval($item['time_length'])."d";
            $res[$i] = array("Department"=>$item['department'],"Name"=>$item['user_name'],"Date"=>$item['s_date'],"Check_in"=>"","Check_out"=>"","Annual_leave"=>"","Sick_leave"=>"","Overtime_Al"=>"","Overtime_paid"=>"","Unpaid"=>"","Missing_card"=>"","Allowance"=>"","Reason"=>"");

            if($item['pay_or_day'] == "pay") $res[$i]['Overtime_paid'] = $item['compension'];
            else $res[$i]['Overtime_Al'] = $item['compension'];
            $i++;
        }

        $sql = "SELECT department,user_name,DATE_FORMAT(start_time,'%Y-%m-%d') as s_date,start_time,end_time ,reason FROM missing_card LEFT OUTER JOIN `profile` ON missing_card.user_id = profile.user_id WHERE start_time >= ? AND start_time <= ? AND state = ? ORDER BY department ASC ,user_name ASC;";
        $mc = $this->db->query($sql,array($start,$end,"approved"))->result_array();

        foreach($mc as $item)
        {
            $res[$i] = array("Department"=>$item['department'],"Name"=>$item['user_name'],"Date"=>$item['s_date'],"Check_in"=>$item['start_time'],"Check_out"=>$item['end_time'],"Annual_leave"=>"","Sick_leave"=>"","Overtime_Al"=>'',"Overtime_paid"=>"","Unpaid"=>"","Missing_card"=>"Yes","Allowance"=>"","Reason"=>$item['reason']);
            $i++;
        }

        $sql = "SELECT department,user_name,DATE_FORMAT(start_time,'%Y-%m-%d') as s_date ,time_length,reason,leave_type FROM allowance_apply LEFT OUTER JOIN `profile` ON allowance_apply.applier_id = profile.user_id WHERE start_time >= ? AND start_time <= ? AND state = ? ORDER BY department ASC ,user_name ASC;";
        $allowance = $this->db->query($sql,array($start,$end,"approved_by_mgr"))->result_array();

        foreach($allowance as $item)
        {
            $res[$i] = array("Department"=>$item['department'],"Name"=>$item['user_name'],"Date"=>$item['s_date'],"Check_in"=>"","Check_out"=>"","Annual_leave"=>"","Sick_leave"=>"","Overtime_Al"=>"","Overtime_paid"=>"","Unpaid"=>"","Missing_card"=>"","Allowance"=>$item['time_length'],"Reason"=>$item['reason']);
            $i++;
        }

        $res_new = array();

        if($user != '')
        {
            foreach($res as $k=>$v)
            {
                if($v['Name'] == $user)
                {
                    $res_new[] = ($res[$k]);
                }
            }
            $res_new = $res_new = empty($res_new) ? array("Department"=>"","Name"=>$item['user_name'],"Date"=>"","Check_in"=>"","Check_out"=>"","Annual_leave"=>"","Sick_leave"=>"","Overtime_Al"=>"","Overtime_paid"=>"","Unpaid"=>"","Missing_card"=>"","Allowance"=>"","Reason"=>""): $res_new;
        }

        if(empty($res_new) && $depart != '')
        {
            foreach($res as $k=>$v)
            {
                if($depart == $v['Department'])
                {
                    $res_new[] = ($res[$k]);
                }
            }
            $res_new = empty($res_new) ? array("Department"=>"","Name"=>$item['user_name'],"Date"=>"","Check_in"=>"","Check_out"=>"","Annual_leave"=>"","Sick_leave"=>"","Overtime_Al"=>"","Overtime_paid"=>"","Unpaid"=>"","Missing_card"=>"","Allowance"=>"","Reason"=>""): $res_new ;
        }
        $res_new = empty($res_new) ? $res : $res_new;
        return $res_new;

    }


    public function monthReportState($start,$end)
    {
        $sql = "SELECT DISTINCT(applier_id) as id, profile.user_name,profile.department,start_date FROM unpaid_leave_apply LEFT OUTER JOIN profile ON unpaid_leave_apply.applier_id = profile.user_id LEFT OUTER JOIN company_members ON profile.user_name = company_members.name  WHERE DATE_FORMAT(start_time,'%Y-%m-%d') >= ? AND DATE_FORMAT(end_time,'%Y-%m-%d') <= ? AND state = ? UNION
                SELECT DISTINCT(employee_id) as id, profile.user_name,profile.department,start_date FROM overtime_apply LEFT OUTER JOIN profile ON overtime_apply.employee_id = profile.user_id LEFT OUTER JOIN company_members ON profile.user_name = company_members.name  WHERE  DATE_FORMAT(start_time,'%Y-%m-%d') >= ? AND DATE_FORMAT(end_time,'%Y-%m-%d') <= ? AND state = ? AND pay_or_day = ? UNION
                SELECT DISTINCT(users.auto_id) as id, profile.user_name,profile.department,start_date FROM users LEFT OUTER JOIN company_members ON users.user_id = company_members.work_email LEFT OUTER JOIN profile ON users.auto_id = profile.user_id WHERE company_members.start_date >= ?;";
        $res = $this->db->query($sql,array($start,$end,"approved_by_mgr",$start,$end,"approved_by_mgr","pay",$start))->result_array();
        $ids = array();//var_dump($res);exit;
        $id_vals = array();
        foreach($res as $v)
        {
            $ids[] = $v['id'];
            $id_vals[$v['id']] = array("overtime"=>0,"unpaid"=>0,"department"=>$v['department'],"name"=>$v['user_name'],"start_date"=>$v['start_date'],'notes'=>"");
        }

        $sql = "SELECT department, profile.user_name, unpaid_leave_apply.applier_id, DATE_FORMAT( unpaid_leave_apply.start_time,  '%Y-%m-%d' ) AS s_date, unpaid_leave_apply.time_length, reason, doctor_notes, profile.Internable, profile.Probationable, company_members.start_date
                FROM unpaid_leave_apply
                LEFT OUTER JOIN  `profile` ON unpaid_leave_apply.applier_id = profile.user_id
                LEFT OUTER JOIN  `users` ON unpaid_leave_apply.applier_id = users.auto_id
                LEFT OUTER JOIN  `company_members` ON company_members.work_email = users.user_id
                WHERE start_time >= ?
                AND start_time <= ?
                AND state = ?
                ORDER BY department ASC , user_name ASC ;";

        $unpaid = $this->db->query($sql,array($start,$end,"approved_by_mgr"))->result_array();
        foreach ($unpaid as $un)
        {
            if($un['doctor_notes'] == 'yes' && $un['Internable'] == 'no' && $un['Probationable'] == 'no'/*intern & probation ����*/)
            {
                $yearinterval = date_diff(date_create($un['start_date']),date_create($un['s_date']));
                $id_vals[$un['applier_id']]['unpaid'] += sick_transfer(floatval($un['time_length']), floor($yearinterval->format('%a')/365));
            }
            else
                $id_vals[$un['applier_id']]['unpaid'] += (floatval($un['time_length']));
            $id_vals[$un['applier_id']]['notes'] .= $un['s_date'].";".$un['time_length'].";unpaid<br/>";
        }

        $sql = "SELECT department,user_name,employee_id,DATE_FORMAT(start_time,'%Y-%m-%d') as s_date ,compension,time_length,pay_or_day FROM overtime_apply LEFT OUTER JOIN `profile` ON overtime_apply.employee_id = profile.user_id WHERE start_time >= ? AND start_time <= ?  AND state = ? AND pay_or_day = 'pay' ORDER BY department ASC ,user_name ASC;;";
        $overtime = $this->db->query($sql,array($start,$end,"approved_by_mgr"))->result_array();//var_dump($ids);exit;
        foreach ($overtime as $un)
        {
            //var_dump($un);
            $id_vals[$un['employee_id']]['overtime'] += 1*floatval($un['time_length']);
            $id_vals[$un['employee_id']]['notes'] .= $un['s_date'].":".$un['time_length'].":overtime<br/>";
        }
        // exit;
        foreach($id_vals as $r=>$v)
        {
            if($v['start_date'] > $start )
            {
                $id_vals[$r]['unpaid'] += $this->calNewUnpaidDays($v['start_date'],$start);
                $id_vals[$r]['notes'] .= "on-board date:".$v['start_date'].'<br/>';
            }
        }


        return $id_vals;
    }

    private function calNewUnpaidDays($start_date,$cal_date /* The former is larger as the cut-off day is later than when he/she arrived  */)
    {
        $day = 0;
        $holidays = $this->getHolidayDates();
        if($cal_date < date("Y-m-1",strtotime($start_date)))
        {
            $cal_date = date("Y-m-1",strtotime($start_date));
        }
        $i = date_create(date_format(date_create($cal_date),"Y-m")."-01") ;
        //var_dump($i);exit;
        while(date_format($i,"Y-m-d") < $start_date)
        {

            if( date('w',strtotime(date_format($i,"Y-m-d"))) == 6 || date('w',strtotime(date_format($i,"Y-m-d"))) == 0 || in_array(date_format($i,"Y-m-d"),$holidays) )
            {

            }
            else
            {
                $day++;
            }
            $i = date_add($i,date_interval_create_from_date_string("1 day"));//var_dump($i);exit;
        }
        return $day;
    }

    public function getRawDataSingle($data)
    {
        if(!isset($data['start']) || !isset($data['end']) || !isset($data['id']))
        {
            return array("code"=>"101","msg"=>"Information required");
        }

        if($data['id'] == -1024)
        {
            return $this->getRawDataSubworker($data);
        }

        if(isset($data['depart']) && $data['id'] == '-512')
        {
            return $this->getRawDataSubworker($data);
        }
        $sql = "SELECT missing_card_log.*,profile.user_id FROM missing_card_log LEFT OUTER JOIN profile ON missing_card_log.name = profile.user_name WHERE record_date >= ? AND record_date <= ? AND profile.user_id = ? ORDER BY record_date ASC;";
        $res =  $this->db->query($sql,array($data['start'],$data['end'],$data['id']))->result_array();
        $holidays = $this->getHolidayDates();
        foreach($res as $k => $v)
        {

            $res[$k]['weekday'] = date("w",strtotime($v['record_date']));

            if( date('w',strtotime($v['record_date'])) == 6 || date('w',strtotime($v['record_date'])) == 0 || in_array($v['record_date'],$holidays) )
            {
                $res[$k]['color_flag'] = "green";
            }
            else if($v['check_in'] == "00:00:00" || $v['check_out'] == "00:00:00")
            {
                if($v['check_in'] == "00:00:00") $res[$k]['check_in'] = "--";
                if($v['check_out'] == "00:00:00") $res[$k]['check_out'] = "--";
            }
            else
            {
                $interval = date_diff(date_create($v['record_date']." ".$v['check_in']),date_create($v['record_date']." ".$v['check_out']));
                $res[$k]['diff'] =  intval($interval->format("%h"))*60+intval($interval->format("%i"));
                if($res[$k]['diff'] < 535) $res[$k]['color_flag'] = "blue";
            }
            $res[$k]['reason'] = ' ';
            $res[$v['record_date']] = $res[$k];

            unset($res[$k]);

        }
        $other_flag = array("1"=>"Annual Leave","2"=>"Sick Leave","3"=>"Unpaid Leave","6"=>"Unpaid Sick Leave","5"=>"Overtime Pay",
            "10"=>"Overtime Day","11"=>'visa related (SH)',"22"=>'visa related (Medical)',"33"=>'Police Reg.',
            "44"=>'visa related(HK/Macau)',"55"=>'visa related(home/3rd country)',"66"=>'Funeral(1 day)',
            "77"=>'Funeral(2 days)',"88"=>'Funeral(3 days)', "99"=>'5 years award',"110"=>'Marriage leave',
            "121"=>'Maternity leave',"132"=>"3 years award(1 day leave)","143"=>"work from home","154"=>"Business trip");

        $other = "SELECT * FROM (SELECT applier_id as user_id,start_time,time_length,leave_type*3 as flag FROM `unpaid_leave_apply` WHERE applier_id = ? AND state = 'approved_by_mgr' UNION 
                  SELECT applier_id as user_id,start_time,time_length,leave_type*1 as flag FROM `leave_apply` WHERE applier_id = ? AND state = 'approved_by_mgr' UNION 
                  SELECT employee_id as user_id,start_time,time_length,pay_or_day*5 as flag FROM overtime_apply WHERE employee_id = ? AND state = 'approved_by_mgr' UNION 
                  SELECT applier_id as user_id,start_time,time_length,leave_type * 11 as flag FROM allowance_apply WHERE applier_id = ? AND state = 'approved_by_mgr') as a WHERE DATE_FORMAT(a.start_time,'%Y-%m-%d') > ? AND DATE_FORMAT(a.start_time,'%Y-%m-%d') <= ? ORDER BY start_time ASC";
        $other_res = $this->db->query($other,array($data['id'],$data['id'],$data['id'],$data['id'],$data['start'],$data['end']))->result_array();

        //  var_dump($other_res);exit;
        foreach($other_res as $ot)
        {
            //   var_dump(date("Y-m-d",strtotime($ot['start_time'])));
            //var_dump($other_flag[$ot['flag']]." started on ".date("H:i:s",$ot['start_time']).";");
            if(date("Y-m-d",strtotime($ot['start_time']) != "1970-01-01"))
            {
                if(!isset($res[date("Y-m-d",strtotime($ot['start_time']))])) $res[date("Y-m-d",strtotime($ot['start_time']))] = array("record_date"=>date("Y-m-d",strtotime($ot['start_time'])),"weekday"=>date("w",strtotime($ot['start_time'])),"check_in"=>"00:00:00","check_out"=>"00:00:00");

                $res[date("Y-m-d",strtotime($ot['start_time']))]['reason'] .= $ot['time_length']." ".$other_flag[$ot['flag']]." started on ".date("H:i:s",strtotime($ot['start_time'])).";";
                $res[date("Y-m-d",strtotime($ot['start_time']))]['flag'] = $ot['flag'];
            }

        }

        return $res;
    }

    public function getRelationAsSearch($data)
    {
        $sql = 'SELECT em_co.name as em_name,em_u.auto_id as em_id,em_pr.department as em_depart, ma_co.name as ma_name,ma_u.auto_id as ma_id,ma_pr.department as ma_depart FROM leader
LEFT OUTER JOIN users as em_u ON leader.user_id = em_u.auto_id
LEFT OUTER JOIN company_members as em_co ON em_u.user_id = em_co.work_email
LEFT OUTER JOIN profile as em_pr ON em_u.auto_id = em_pr.user_id
LEFT OUTER JOIN users as ma_u ON leader.leader_id = ma_u.auto_id
LEFT OUTER JOIN company_members as ma_co ON ma_u.user_id = ma_co.work_email
LEFT OUTER JOIN profile as ma_pr ON ma_u.auto_id = ma_pr.user_id WHERE em_co.last_day IS NULL AND em_co.name LIKE ? ORDER BY em_pr.department;';
        return $this->db->query($sql,array("%".$data['chars']."%"))->result_array();
    }

    public function getListAsSearch($data)
    {
        $sql = 'SELECT DISTINCT(users.auto_id) ,user_name,users.user_id ,department,title FROM profile LEFT OUTER JOIN users ON profile.user_id = users.auto_id LEFT OUTER JOIN leave_days ON users.auto_id = leave_days.user_id LEFT OUTER JOIN company_members ON users.user_id = company_members.work_email WHERE company_members.name LIKE ? AND last_day IS NULL ORDER BY cal_year DESC;';
        return $this->db->query($sql,array("%".$data['chars']."%"))->result_array();
    }

    public function getNameByDepartmentState($data)
    {
        $sql = "SELECT user_id,user_name  FROM profile LEFT OUTER JOIN company_members ON profile.user_name = company_members.name WHERE department = ? AND last_day IS NULL ORDER BY user_name ASC ;";
        return $this->db->query($sql,array($data['depart']))->result_array();
    }

    public function updateContactListTemp()
    {
        /*
         * Author : alex.fu
         * used only once on 3/15/2017 for updating contact list
         */
        // var_dump("FF");exit;
        $res = $this->db->query("SELECT * FROM temp_contact_list ")->result_array();
        $sql = "";
        // var_dump($res);exit;
        foreach($res as $r)
        {
            $sql .= "UPDATE company_members SET name = '".$r['name']."' WHERE work_email = '".$r['mail']."' ;";
        }
        var_dump($this->db->query($sql));
    }

    public function getBalanceByDepartState($depart)
    {
        $sql_resigned = 'SELECT user_id FROM company_members LEFT OUTER JOIN profile ON profile.user_name = company_members.name WHERE last_day IS NOT NULL ;';
        $resign_result = $this->db->query($sql_resigned)->result_array();
        $resign_list = array();
        foreach($resign_result as $v)
        {
            $resign_list[] = $v['user_id'];
        }//var_dump($resign_list);exit;
        $sql = "SELECT user_id,user_name FROM profile WHERE department= ? ";
        $workers = $this->db->query($sql,array($depart))->result_array();
        $return_array = array();
        //var_dump($workers);echo "<br>";var_dump($resign_list);var_dump(in_array("54",$resign_list));exit;
        foreach($workers as $worker)
        {
            if(!in_array($worker['user_id'],$resign_list )) {
                $sql = "SELECT leave_balance,sick_leave_balance,cal_year FROM leave_days  WHERE user_id = ?  ORDER BY cal_year DESC LIMIT 2 ;";
                $data = $this->db->query($sql,array($worker['user_id']))->result_array();
                $res = array();
                // $return_array[$worker['user_id']] = count($data);
                if(empty($data))
                {
                    $return_array[$worker['user_id']] = array("leave_days"=>"0","sick_days"=>"0","leave_overdue"=>array("num"=>"0","day"=>"0"),"sick_overdue"=>array("num"=>"0","day"=>"0"));
                    $return_array[$worker['user_id']]['user_name'] = $worker['user_name'];
                    continue;
                }
                $date_overdue = date_create(date("Y-m-d",strtotime("+1 year",strtotime($data[0]['cal_year']))));
                $date_now = date_create(date("Y-m-d"));
                if(count($data) == 2)
                {
                    $res['leave_days'] = $data[0]['leave_balance']+$data[1]['leave_balance'];

                    $res['leave_overdue'] = array("num"=>$data[1]['leave_balance'],"day"=>date_diff($date_overdue,$date_now)->format("%a"));
                }
                else
                {
                    $res['leave_days'] = $data[0]['leave_balance'];
                    $res['leave_overdue'] = array("num"=>"0","day"=>"0");
                }

                $res['sick_days'] = $data[0]['sick_leave_balance'];
                $res['sick_overdue'] = array("num"=>$data[0]['sick_leave_balance'],"day"=>date_diff($date_overdue,$date_now)->format("%a"));
                $return_array[$worker['user_id']] = $res;
                $return_array[$worker['user_id']]['user_name'] = $worker['user_name'];}
        }//var_dump($return_array);exit;
        return $return_array;
    }

    public function getRawDataSubworker($data)
    {
        if(!isset($data['start']) || !isset($data['end']) || !isset($data['id']))
        {
            return array("code"=>"101","msg"=>"Information required");
        }

        if($data['id'] == "-1024")
        {
            $sub_id = $this->db->query("SELECT * FROM subordinate_links WHERE user_id = ?",array($_SESSION['user_id']))->row_array();
            $sub_id = $sub_id['sub_id'];
            $ids = explode(",",$sub_id);
            $total = array();
        }
        else if($data['id'] == '-512' && isset($data['depart']) )
        {
            $sub_id = $this->db->query("SELECT user_id FROM profile WHERE department = ?",array($data['depart']))->result_array();
            foreach($sub_id as $i)
            {
                $ids[] = $i['user_id'];
            }//var_dump($ids);exit;
            $total = array();
        }

        foreach($ids as $id)
        {
            $sql = "SELECT missing_card_log.*,profile.user_id,profile.user_name as name FROM missing_card_log LEFT OUTER JOIN profile ON missing_card_log.name = profile.user_name WHERE record_date >= ? AND record_date <= ? AND profile.user_id = ? ORDER BY record_date ASC;";
            $res =  $this->db->query($sql,array($data['start'],$data['end'],$id))->result_array();
            $holidays = $this->getHolidayDates();
            $name = $this->db->query("SELECT user_name FROM profile WHERE user_id = ?",array($id))->row_array();
            $name = $name['user_name'];
            foreach($res as $k => $v)
            {
                $store_flag = 0;
                $res[$k]['weekday'] = date("w",strtotime($v['record_date']));

                if( date('w',strtotime($v['record_date'])) == 6 || date('w',strtotime($v['record_date'])) == 0 || in_array($v['record_date'],$holidays) )
                {
                    $res[$k]['color_flag'] = "green";
                    $store_flag = 1;
                }
                else if($v['check_in'] == "00:00:00" || $v['check_out'] == "00:00:00")
                {
                    if($v['check_in'] == "00:00:00") $res[$k]['check_in'] = "--";
                    if($v['check_out'] == "00:00:00") $res[$k]['check_out'] = "--";
                    $store_flag = 1;
                }
                else
                {
                    $interval = date_diff(date_create($v['record_date']." ".$v['check_in']),date_create($v['record_date']." ".$v['check_out']));
                    $res[$k]['diff'] =  intval($interval->format("%h"))*60+intval($interval->format("%i"));
                    if($res[$k]['diff'] < 535)
                    {
                        $res[$k]['color_flag'] = "blue";
                        $store_flag =1;
                    }
                }
                $res[$k]['reason'] = ' ';
                if($store_flag == 1) $res[$v['record_date']] = $res[$k];

                unset($res[$k]);

            }
            // var_dump($res);continue;
            $other_flag = array("1"=>"Annual Leave","2"=>"Sick Leave","3"=>"Unpaid Leave","6"=>"Unpaid Sick Leave","5"=>"Overtime Pay",
                "10"=>"Overtime Day","11"=>'visa related (SH)',"22"=>'visa related (Medical)',"33"=>'Police Reg.',
                "44"=>'visa related(HK/Macau)',"55"=>'visa related(home/3rd country)',"66"=>'Funeral(1 day)',
                "77"=>'Funeral(2 days)',"88"=>'Funeral(3 days)', "99"=>'5 years award',"110"=>'Marriage leave',
                "121"=>'Maternity leave');

            $other = "SELECT * FROM (SELECT applier_id as user_id,start_time,time_length,leave_type*3 as flag FROM `unpaid_leave_apply` WHERE applier_id = ? AND state = 'approved_by_mgr' UNION
                  SELECT applier_id as user_id,start_time,time_length,leave_type*1 as flag FROM `leave_apply` WHERE applier_id = ? AND state = 'approved_by_mgr' UNION
                  SELECT employee_id as user_id,start_time,time_length,pay_or_day*5 as flag FROM overtime_apply WHERE employee_id = ? AND state = 'approved_by_mgr' UNION
                  SELECT applier_id as user_id,start_time,time_length,leave_type * 11 as flag FROM allowance_apply WHERE applier_id = ? AND state = 'approved_by_mgr') as a WHERE DATE_FORMAT(a.start_time,'%Y-%m-%d') > ? AND DATE_FORMAT(a.start_time,'%Y-%m-%d') <= ? ORDER BY start_time ASC";
            $other_res = $this->db->query($other,array($id,$id,$id,$id,$data['start'],$data['end']))->result_array();


            foreach($other_res as $ot)
            {
                //   var_dump(date("Y-m-d",strtotime($ot['start_time'])));
                //var_dump($other_flag[$ot['flag']]." started on ".date("H:i:s",$ot['start_time']).";");
                if(date("Y-m-d",strtotime($ot['start_time']) != "1970-01-01"))
                {
                    if(!isset($res[date("Y-m-d",strtotime($ot['start_time']))])) $res[date("Y-m-d",strtotime($ot['start_time']))] = array("name"=>$name,"record_date"=>date("Y-m-d",strtotime($ot['start_time'])),"weekday"=>date("w",strtotime($ot['start_time'])),"check_in"=>"00:00:00","check_out"=>"00:00:00");
                    //var_dump($res[date("Y-m-d",strtotime($ot['start_time']))]);
                    $res[date("Y-m-d",strtotime($ot['start_time']))]['reason'] .= $ot['time_length']." ".$other_flag[$ot['flag']]." started on ".date("H:i:s",strtotime($ot['start_time'])).";";
                    $res[date("Y-m-d",strtotime($ot['start_time']))]['flag'] = $ot['flag'];

                }

            }

            foreach($res as $k=>$v)
            {
                if($v['reason'] == ' ' && $v['color_flag'] == 'green')
                {
                    unset($res[$k]);
                }
            }
            $total[$id] = $res;
        }

        return $total;
    }

    public function getLeaveBalanceChangeByWorker($data)
    {
        $userId = $data['id'];
        $year = $this->db->query("SELECT probation_date FROM company_members as c LEFT OUTER JOIN users as u ON c.work_email = u.user_id WHERE u.auto_id = ? AND c.probation_date != '0000-00-00'",$userId)->row_array();
        $newYearBalance = date("Y") - date("Y",strtotime($year['probation_date'])) - 1 +10;


        $sql = "SELECT * FROM leave_days 
                WHERE user_id = ?
                ORDER BY cal_year DESC
                LIMIT 2";
        $result = $this->db->query($sql,$userId)->result_array();
        $calYear = '2017-04-01';
        $sql = "SELECT * FROM april_leave_balance
                WHERE user_id = ?
                LIMIT 1 ";
        $aprilLeaveResult = $this->db->query($sql,$userId)->result_array();
        if(!empty($aprilLeaveResult))
        {
            $currentLeaveBalance = $aprilLeaveResult[0]['april_leave_balance'];
            $currentSickLeaveBalance = $aprilLeaveResult[0]['april_sick_balance'];
        }
        else
        {
            if(date_format(date_create($result[0]['cal_year']),"m-d") > date("m-d"))
                $currentLeaveBalance = $result[1]['leave_balance'] + $result[0]['leave_balance'];
            else
                $currentLeaveBalance = $result[1]['leave_balance'] + $newYearBalance >= 14 ? 15 : $newYearBalance+1;
            $currentSickLeaveBalance = 3.5;
        }
        $whenAddBalance = array();
        $whenAddBalance[0] = $result[0];
        $whenAddBalance[0]['leave_day_balance'] = $newYearBalance >= 14 ? 15 : $newYearBalance+1;
        $sql = "SELECT DISTINCT * FROM leave_apply
                WHERE applier_id = ? 
                AND state = 'approved_by_mgr'
                AND leave_type = 'leave'
                AND create_time > ?";
        $leaveResult = $this->db->query($sql,array($userId,$calYear))->result_array();
        $leaveResultCount = count($leaveResult);
        $resultTemp = array();
        foreach ($leaveResult as $key => $value)
        {
            $resultTemp[$key]['create_time'] = $leaveResult[$key]['create_time'];
            $resultTemp[$key]['start_time'] = $leaveResult[$key]['start_time'];
            $resultTemp[$key]['end_time'] = $leaveResult[$key]['end_time'];
            $resultTemp[$key]['time_length'] = $leaveResult[$key]['time_length'];
            $resultTemp[$key]['reason'] = $leaveResult[$key]['reason'];
            $resultTemp[$key]['flag'] = 0; // 0 means leave
        }
        $sql = "SELECT DISTINCT * FROM overtime_apply
                WHERE employee_id = ?
                AND state = 'approved_by_mgr'
                AND pay_or_day = 'day'
                AND create_time > ?";
        $otResult = $this->db->query($sql,array($userId,$calYear))->result_array();
        $overTimeResultCount = count($otResult);
        foreach ($otResult as $key => $value)
        {
            $resultTemp[$key+$leaveResultCount]['create_time'] = $otResult[$key]['create_time'];
            $resultTemp[$key+$leaveResultCount]['start_time'] = $otResult[$key]['start_time'];
            $resultTemp[$key+$leaveResultCount]['end_time'] = $otResult[$key]['end_time'];
            $resultTemp[$key+$leaveResultCount]['time_length'] = $otResult[$key]['time_length'];
            $resultTemp[$key+$leaveResultCount]['reason'] = $otResult[$key]['reason'];
            $resultTemp[$key+$leaveResultCount]['flag'] = 1; // 1 means overtime
        }
        $sql = "SELECT DISTINCT * FROM leave_apply
                WHERE applier_id = ? 
                AND state = 'approved_by_mgr'
                AND leave_type = 'sick_leave'
                AND create_time > ?";
        $sickLeaveResult = $this->db->query($sql,array($userId,$calYear))->result_array();
        foreach ($sickLeaveResult as $key => $value)
        {
            $resultTemp[$key+$leaveResultCount+$overTimeResultCount]['create_time'] = $sickLeaveResult[$key]['create_time'];
            $resultTemp[$key+$leaveResultCount+$overTimeResultCount]['start_time'] = $sickLeaveResult[$key]['start_time'];
            $resultTemp[$key+$leaveResultCount+$overTimeResultCount]['end_time'] = $sickLeaveResult[$key]['end_time'];
            $resultTemp[$key+$leaveResultCount+$overTimeResultCount]['time_length'] = $sickLeaveResult[$key]['time_length'];
            $resultTemp[$key+$leaveResultCount+$overTimeResultCount]['reason'] = $sickLeaveResult[$key]['reason'];
            $resultTemp[$key+$leaveResultCount+$overTimeResultCount]['flag'] = 2; // 2 means sick leave
        }
        $creatTime = array();
        foreach ($resultTemp as $item)
            $creatTime[] = $item['create_time'];
        array_multisort($creatTime,SORT_ASC,$resultTemp);
        $result = $resultTemp;
        $balanceChangeData = array();
        $isSick = 0; // when sick leave record appears firstly, it is 1
        $firstAnuualLeave = $firstSickLeaveAfterUpdate = ''; // 记录年假更新后首次请病假的下标
        foreach ($result as $key => $value)
        {
            if($result[$key]['flag'] != 2) // overtime or leave
            {
                $firstAnuualLeave = $key;
                break;
            }
        }
        foreach ($result as $key => $value)
        {
            if($result[$key]['flag'] == 2 && $result[$key]['create_time'] > $whenAddBalance[0]['cal_year']) // overtime or leave
            {
                $firstSickLeaveAfterUpdate = $key;
                break;
            }
        }
        foreach ($result as $key => $value)
        {
            $balanceChangeData[$key]['create_time'] = $result[$key]['create_time'];
            $balanceChangeData[$key]['start_time'] = $result[$key]['start_time'];
            $balanceChangeData[$key]['end_time'] = $result[$key]['end_time'];
            if($result[$key]['flag'] != 2) // overtime or leave
            {
                if($key > 0 && $key != $firstAnuualLeave)
                {
                    for($i = $key-1; $i >= 0; $i--)
                    {
                        if($result[$i]['flag'] == 2) //sick leave
                            continue;
                        else
                        {
                            $balanceChangeData[$key]['leave_day_balance'] = ($result[$i]['flag'])
                                ? $balanceChangeData[$i]['leave_day_balance']+floatval($balanceChangeData[$i]['time_length'])
                                : $balanceChangeData[$i]['leave_day_balance']-floatval($balanceChangeData[$i]['time_length']);
                            break;
                        }
                    }
                }
                else
                    $balanceChangeData[$key]['leave_day_balance'] = $currentLeaveBalance;
                $balanceChangeData[$key]['sick_leave_day_balance'] = "";
            }
            else
            {
                $balanceChangeData[$key]['leave_day_balance'] = "";
                if($isSick == 0)
                {
                    if($key == $firstSickLeaveAfterUpdate) //年假更新后第一次请病假，结余3.5天
                        $balanceChangeData[$key]['sick_leave_day_balance'] = 3.5;
                    else
                        $balanceChangeData[$key]['sick_leave_day_balance'] = $currentSickLeaveBalance;
                    $isSick = 1;
                }
                else
                {
                    for($i = $key-1; $i >=0; $i--)
                    {
                        if($result[$i]['flag'] != 2) // not sick leave
                            continue;
                        else
                        {
                            $balanceChangeData[$key]['sick_leave_day_balance'] = $balanceChangeData[$i]['sick_leave_day_balance']-floatval($balanceChangeData[$i]['time_length']);
                            break;
                        }
                    }
                }
            }
            $balanceChangeData[$key]['time_length'] = $result[$key]['time_length'];
            $balanceChangeData[$key]['reason'] = $result[$key]['reason'];
            $balanceChangeData[$key]['flag'] = $result[$key]['flag'];
        }
        foreach ($balanceChangeData as $key => $value)  // 如果有新加的年假结余就加进去
        {
            if($balanceChangeData[$key]['create_time'] > $whenAddBalance[0]['cal_year'] && $whenAddBalance[0]['cal_year'] > $calYear)
            {
                if($balanceChangeData[$key]['flag'] != 2) // add leave_balance
                {
                    $balanceChangeData[$key]['leave_day_balance'] += $whenAddBalance[0]['leave_day_balance'];
                }
            }

        }
        array_push($balanceChangeData,$whenAddBalance);
        return $balanceChangeData;

    }



}

function sick_transfer($time_length,$year)
{
    $mult = 1.0;
    if($year < 2) $mult = 0.4;
    else if($year < 4) $mult = 0.3;
    else if($year < 6) $mult = 0.2;
    else if($year < 8) $mult = 0.1;
    else $mult = 0;
    return $mult*(floatval($time_length));//exit;

}

