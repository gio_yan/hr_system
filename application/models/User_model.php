 <?php
require_once(getcwd().'/application/models/SS_model.php');
class User_model extends SS_model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->library('encryption');
	}


	public function getLoginVerifyState($data)
	{	
		if( array_key_exists("user",preg_grep("/^[0-9a-zA-z\.]+@motionglobal.com$/",$data)) && array_key_exists("passwords", preg_grep("/^[@&%0-9a-zA-z]{6,20}$/", $data)))
		{
			$sql = 'SELECT auto_id as user_id FROM `users` WHERE user_id = ? AND passwords = ? ;';
			$result = $this->db->query($sql,array($data['user'],sha1($data['passwords'])))->row_array();
			if(isset($result['user_id']))
			{
				$_SESSION['user_state'] = true;
				$_SESSION['user_id'] = $result['user_id'];
				return array("code"=>"200");
			}
			return array("code"=>"101","text"=>"Wrong email address Or password");//101 用户名密码错误
		}
		return array("code"=>"102","text"=>"Please enter email and password in correct format");//102 不符合规范
	}

	public function getRegisterState($data)
	{
		//return $data;
		if ( array_key_exists("user",preg_grep("/^[0-9a-zA-z\.]+@motionglobal.com$/",$data)))
		{
			if(strlen($data['pass']) >=6 && ($data['pass'] == $data['con_pass'] ))
			{
				$_SESSION['con_user'] = $data['user'];
				$_SESSION['con_pass'] = sha1($data['pass']);
				
				$va = "mail -s 'Please Confirm Your Registration' ".$data['user']." < mail.con";
				//return array($va);
				shell_exec($va);
				//mail($data['user'],"Please Confirm Your Registration","FFFF");
				return array("code"=>"200");
			}
			return array("code"=>"102","text"=>"Please make sure that length of the password must be over 6 digits~");
			
		}
		return array("code"=>"101","text"=>"Wrong email address");
		
	}

	public function registerConfirmState()
	{
		if(isset($_SESSION['con_user']) && isset($_SESSION['con_pass']))
		{
			$sql = 'INSERT INTO `users`(`auto_id`, `user_id`, `passwords`, `mail`, `phone`,`is_hr`,`is_leader`) VALUES (null,?,?,?,?,?,?);';
			$this->db->query($sql,array($_SESSION['con_user'],$_SESSION['con_pass'],$_SESSION['con_user'],"0","no","no"));
			unset($_SESSION['con_user']);
			unset($_SESSION['con_pass']);
			return array("code"=>"200");
		}
		return array("code"=>"101","text"=>"Overtime");
		
	}

	public  function getBasicInfo()
	{
		$sql = 'SELECT `name`, `chinese_name`, `work_email`, `phone_number`, `moblie_number`, `personal_email`, `skype`,`work_location`, `start_date`,
`birthday`, `home_address`,title,department FROM company_members LEFT OUTER JOIN users ON company_members.work_email = users.user_id LEFT OUTER JOIN profile ON  profile.user_id= users.auto_id WHERE profile.user_id = ?   ;';
		return $this->db->query($sql,$_SESSION['user_id'])->row_array();
	}

	public function getProfileState($data)
	{
		return $this->_redisKeyInterFace("_getProfileState","_getProfileState",$data);
	}

	public function _getProfileState($data)
	{
		$sql = 'SELECT * FROM `profile` WHERE user_id = ? ;';
		$result = $this->db->query($sql,array($data['user_id']))->row_array();
		if(isset($result['user_id']))
		{
			return $result;
		}
		$sql = "INSERT INTO `profile` (user_id,user_name,title,department) VALUES (?,?,?,?) ;";
		$this->db->query($sql,array($data['user_id'],"Anonymous","Not Defined","Not Defined"));
		return array("user_id"=>$data['user_id'],"user_name"=>"Anonymous","title"=>"Not Defined","department"=>"Not Defined");
	}

	public function editBasicInfo($data)
	{
        $email = $this->db->query("SELECT work_email FROM company_members LEFT OUTER JOIN users ON company_members.work_email = users.user_id WHERE users.auto_id = '{$_SESSION['user_id']}'")->row_array();

        $sql = "UPDATE `company_members` SET chinese_name = ? ,phone_number = ?,personal_email = ?,moblie_number = ?,home_address = ?,skype = ?  WHERE work_email = ? ;";
		$this->db->query($sql,array($data['chinese_name'],$data['phone_number'],$data['personal_email'],$data['moblie_number'],$data['home_address'],$data['skype'],$email['work_email']));
		return array("code"=>"200");
	}

	public function isLeader()
	{
		$sql  = "SELECT is_leader FROM users WHERE auto_id = ? ;";
		$res = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		return $res['is_leader'];
	}

	public function getLeaveAndSickDaysState()
	{
		$sql = "SELECT leave_balance,sick_leave_balance,cal_year FROM leave_days WHERE user_id = ? ORDER BY cal_year DESC LIMIT 2 ;";
		$data = $this->db->query($sql,array($_SESSION['user_id']))->result_array();
		$res = array();
		if(empty($data))
		{
			return array("leave_days"=>0,"sick_days"=>0);
		}
		$date_overdue = date_create(date("Y-m-d",strtotime("+1 year",strtotime($data[0]['cal_year']))));
		$date_now = date_create(date("Y-m-d"));
		if(count($data) == 2)
		{
			$res['leave_days'] = $data[0]['leave_balance']+$data[1]['leave_balance'];
			
			$res['leave_overdue'] = array("num"=>$data[1]['leave_balance'],"day"=>date_diff($date_overdue,$date_now)->format("%a"));
		}
		else
		{
			$res['leave_days'] = $data[0]['leave_balance'];
			$res['leave_overdue'] = array();
		}
		
		$res['sick_days'] = $data[0]['sick_leave_balance'];
		$res['sick_overdue'] = array("num"=>$data[0]['sick_leave_balance'],"day"=>date_diff($date_overdue,$date_now)->format("%a"));
		return $res;
	}
	//public function 
	public function getUsernameState()
	{
		$sql = 'SELECT name FROM company_members LEFT OUTER JOIN users ON company_members.work_email = users.user_id WHERE users.auto_id = ? ;';
		$res = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		return isset($res['name']) ? $res['name'] : "Anonymous";
	}

	public function isHr()
	{
		$sql  = "SELECT is_hr FROM users WHERE auto_id = ? ;";
		$res = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		return $res['is_hr'];
	}
	public function getUpStreamList()
	{
		// $sql = 'SELECT auto_id as user_id FROM `users` WHERE is_leader = "yes"';
		// $res = $this->db->query($sql,array($sql))->result_array();
		// $res_new = array();
		// foreach($res as $r)
		// {
		// 	$res_new[] = $r['user_id'];
		// }
		//       //var_dump($res_new);exit;
		// $sql = "SELECT * FROM leader ;";
		// $tree_old = $this->db->query($sql)->result_array();
		// $tree_new = array();
		// foreach($tree_old as $r=>$v)
		// {
		// 	$tree_new[$v['user_id']] = $v['leader_id'];
		// }
		// $num = $_SESSION['user_id'];
		// $list =array();
		// while(!in_array($num,$res_new))
		// {
		// 	$num = $tree_new[$num];
		// 	array_push($list,$num);
		// }var_dump($list);exit;
		$sql = "SELECT users.user_id,mail FROM users LEFT OUTER JOIN leader ON leader.leader_id = users.auto_id WHERE leader.user_id  = ?;";
		$res = $this->db->query($sql,array($_SESSION['user_id']))->result_array();
		return($res);//exit;
	}

	public function getDownStreamList()
	{
		$sql = "SELECT sub_id FROM subordinate_links WHERE user_id = ? ;";
		$res = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		$res_new = implode(",",$res);
		return $res_new;
	}

	public function getIfTurnsToProfile()
	{
		$info = $this->db->query("SELECT chinese_name , phone_number,personal_email,moblie_number,skype,home_address FROM company_members LEFT OUTER JOIN users ON company_members.work_email = users.user_id WHERE users.auto_id = '{$_SESSION['user_id']}'")->row_array();
		foreach($info as $r =>$v)
		{
			if(empty($v))
			{
				return true;
			}
		}
		return false;
	}

	public function getIfOutBoard()
	{
		$sql = "SELECT last_day FROM company_members LEFT OUTER JOIN users ON company_members.work_email = users.user_id WHERE users.auto_id = ? ;";
		$res = $this->db->query($sql ,array($_SESSION['user_id']))->row_array();
		if(empty($res['last_day'])) return false;
		else
		{
			$sql = "UPDATE password SET passwords = ? WHERE auto_id = ? ;";
			$this->db->query($sql,array(sha1(rand(0,65536)."AASFC"),$_SESSION['user_id']));
			unset($_SESSION['user_id']);
			unset($_SESSION['user_state']);
			return true;
		}
	}

	public function leaderStateDetected()
	{
		$sql = "SELECT COUNT(*) as num FROM leader WHERE leader_id = ? ;";
		$res = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		$own = $this->db->query("SELECT user_id,leader_id FROM leader WHERE user_id = ? ;",array($_SESSION['user_id']))->row_array();
		if($res['num'] == 0)
		{
			$judge = "no";
		}
		else if($res['num'] == 1 && $own['user_id'] == $own['leader_id'])
		{
			$judge = "no";
		}
		else
		{
			$judge = "yes";
		}
		$sql = "UPDATE users SET is_leader = ? WHERE auto_id = ? ;";
		return $this->db->query($sql,array($judge,$_SESSION['user_id']));
	}

	public function updateLeaveDays()
	{
		$sql = "SELECT * FROM leave_days WHERE user_id = ? ORDER BY cal_year DESC LIMIT 1";
		$res = $this->db->query($sql,array($_SESSION['user_id']))->row_array();//var_dump($res);exit;
		$year = $this->db->query("SELECT probation_date FROM company_members as c LEFT OUTER JOIN users as u ON c.work_email = u.user_id WHERE u.auto_id = ? AND c.probation_date != '0000-00-00'",array($_SESSION['user_id']))->row_array();
		if(!isset($year['probation_date'])) return ;
		$res['leave_balance'] = date("Y") - date("Y",strtotime($year['probation_date'])) - 1 + 10;
		//var_dump($res['leave_balance']);exit;
		$length = $res['leave_balance'] >= 14 ? 15 : $res['leave_balance']+1;
		$target_date = (date_format(date_add(date_create($res['cal_year']),date_interval_create_from_date_string("1 year")),"Y-m-d"));
		if($length)
			if(date("Y-m-d") >= $target_date )
			{
				$sql = "INSERT INTO leave_days (user_id,cal_year,leave_balance,sick_leave_balance) VALUES (?,?,?,?) ;";
				$this->db->query($sql,array($_SESSION['user_id'],$target_date,$length,"3.5"));
			}
	}

	public function getNavState()
	{
		$res = $this->db->query("SELECT is_leader,is_hr FROM users WHERE auto_id = {$_SESSION['user_id']}")->row_array();
		if($res['is_hr'] == 'yes') $hr_flag = 1;
		else $hr_flag = 0;
		if($res['is_leader'] == 'yes') $leader_flag = 1;
		else $leader_flag = 0;
		return $leader_flag + $hr_flag*2 ;
	}
	public function alterPassState($data)
    {
        if(sha1($data['password_confirm']) != sha1($data['password_new']))
            return array("code"=>"101","msg"=>"New password and confirmed password does not fit");
        $sql = "SELECT auto_id FROM users WHERE auto_id = ? AND passwords = ? ;";
        $res = $this->db->query($sql, array($_SESSION['user_id'],sha1($data['password'])))->row_array();
        if(isset($res['auto_id']))
        {
            $sql = "UPDATE users SET passwords = ? WHERE auto_id = ? ";
            $this->db->query($sql,array(sha1($data['password_new']),$_SESSION['user_id']));
            return array("code"=>"200");
        }
        else
        {
            return array("code"=>"102","msg"=>"Wrong password");
        }

    }


    public function getUnpaidVisibleState()
    {

        $proState = $this->db->query("SELECT Probationable,Internable FROM profile WHERE user_id = ?",array($_SESSION['user_id']))->row_array();
        if( $proState['Internable'] == "yes") return array("code"=>"200","msg"=>"intern");
        $dayState = $this->db->query("SELECT leave_balance as num,sick_leave_balance as sick_num FROM leave_days WHERE user_id = ? ORDER BY cal_year DESC LIMIT 1",array($_SESSION['user_id']))->row_array();
        if($dayState['num'] == 0) return array("code"=>"200","msg"=>"al");
		if($dayState['sick_num'] == 0) return array("code"=>"200","msg"=>"sl");
		return array("code"=>"101");
    }

    public function getLeaveVisible()
    {
        $sql = "SELECT Internable,Probationable FROM profile WHERE user_id = ? ;";
        $res = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
        if(($res['Internable']) == 'yes' )
        {
            return true;
        }else
        {
            return false;
        }
    }

    public function testState()
    {
//    	$sql = "SELECT leave_days_for_transfer.*,users.auto_id FROM leave_days_for_transfer LEFT OUTER JOIN users ON leave_days_for_transfer.user_email = users.user_id WHERE 1";
//    	$res = $this->db->query($sql)->result_array();
//    	//return $res;
//    	$data_now = array();
//    	foreach($res as $data)
//    	{
//    		$date=date_create($data['start_date']);
//    		$date_year_later = date_format(date_add($date,date_interval_create_from_date_string("1 year")),"Y-m-d");
//    		if($data['start_date'] == "0000-00-00")
//    		{
//    		}
//    		else if($date_year_later  > date("Y-m-d") )
//    		{
//    			$data_now[] = array("user_id"=>$data['auto_id'],"cal_year"=>$data['start_date'],"leave_balance"=>$data['this_leave'],"sick_leave_balance"=>$data['this_sick']);
//    			$this->db->query("INSERT INTO `leave_days`(`user_id`, `cal_year`, `leave_balance`, `sick_leave_balance`) VALUES (?,?,?,?); ",array($data['auto_id'],$data['start_date'],$data['this_leave'],$data['this_sick']));
//    		}
//    		else
//    		{
//    			$date_flag = $date_year_later;
//    			while($date_flag <= date("Y-m-d"))
//    			{
//    				$date_flag = date_format(date_add(date_create($date_flag),date_interval_create_from_date_string("1 year")),"Y-m-d");
//    			}
//    			$date_flag = date_format(date_sub(date_create($date_flag),date_interval_create_from_date_string("1 year")),"Y-m-d");
//    			$data_now[] = array("user_id"=>$data['auto_id'],"cal_year"=>$date_flag,"leave_balance"=>$data['this_leave'],"sick_leave_balance"=>$data['this_sick']);
//    			$this->db->query("INSERT INTO `leave_days`(`user_id`, `cal_year`, `leave_balance`, `sick_leave_balance`) VALUES (?,?,?,?); ",array($data['auto_id'],$date_flag,$data['this_leave'],$data['this_sick']));
//    			$date_flag = date_format(date_sub(date_create($date_flag),date_interval_create_from_date_string("1 year")),"Y-m-d");
//    			$data_now[] = array("user_id"=>$data['auto_id'],"cal_year"=>$date_flag,"leave_balance"=>$data['last_leave'],"sick_leave_balance"=>0);
//    			$this->db->query("INSERT INTO `leave_days`(`user_id`, `cal_year`, `leave_balance`, `sick_leave_balance`) VALUES (?,?,?,?); ",array($data['auto_id'],$date_flag,$data['last_leave'],0));
//    		}
//    	}
//    	return $data_now;
		$data = array();
		$data = $this->db->query("SELECT name,work_email,auto_id FROM company_members LEFT OUTER JOIN users ON company_members.work_email = users.user_id ;")->result_array();
		foreach($data as $row)
		{
			$this->db->query("UPDATE profile SET user_name = ? WHERE user_id = ? ",array($row['name'],$row['auto_id']));
		}
		return array();
    }

    public function testPasswordResetState()
    {
        $testers = $this->db->query("SELECT auto_id,user_id,mail FROM users WHERE auto_id = 37")->result_array();//var_dump($testers);exit;
        foreach($testers as $r=>$test )
        {
            $testers[$r]['pass'] = $this->generatePass();
            var_dump($testers[$r]);
            $this->db->query("UPDATE users SET passwords = ? WHERE auto_id = ?",array(sha1($testers[$r]['pass']),($test['auto_id'])));

        }
        return $testers;
    }


    protected function generatePass()
    {
        $string = "1234567890abcdefghijklmnopgrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@&%";
        $pass = "";
        for($i=0;$i<10;$i++)
        {
            $pass.= $string[rand(0,strlen($string)-1)];
        }
        return $pass;
    }

    public function forgetPassState($data)
    {

        $user = $this->db->query("SELECT auto_id,user_id,mail FROM users WHERE user_id = ?",array($data['email']))->row_array();
        if(!isset($user['auto_id'])) return array("code"=>"101","msg"=>"Email Address not exists");

		$sql = "SELECT last_day FROM company_members  WHERE work_email = ? ;";
		$res = $this->db->query($sql ,array($data['email']))->row_array();
		if(!empty($res['last_day'])) return array("code"=>"103","msg"=>"Account Forbidden ");



        $user['pass'] = $this->generatePass();

        $this->db->query("UPDATE users SET passwords = ? WHERE auto_id = ?",array(sha1($user['pass']),($user['auto_id'])));
        return $user;
    }

    public function transState()
    {
        //$query = "UPDATE missing_card_log_back SET date = DATE_FORMAT('%Y-%m-%d',date) WHERE 1";
        $query = "SELECT * FROM missing_card_log_back WHERE 1";
        $res =  $this->db->query($query)->result_array();
        $data = array();
        $query = 'INSERT INTO missing_card_log (name,record_date,check_in,check_out) VALUES ';
        foreach ($res as $r => $v)
        {
            $v['record_date'] = date("Y-m-d",strtotime($v['record_date']));
            $data[] = $v;
            $query.="('".$v['name']."' , '".$v['record_date']."' , '".$v['check_in']."' , '".$v['check_out']."' ),";
        }
        var_dump(($query));exit;
    }

    public function isRawCheckableState($data)
    {

        $sql = "SELECT * FROM leader WHERE user_id = ? AND leader_id = ? ;";
        $res = $this->db->query($sql,array($data['id'],$_SESSION['user_id']))->row_array();
        return isset($res['user_id']);
    }

    public function getAnniversaryThisMonth()
    {
        $sql = "SELECT * FROM users,company_members 
                WHERE users.user_id = company_members.work_email
                AND company_members.start_date LIKE '%-".$this->db->escape_like_str(date("m"))."-%' ESCAPE '!'
                AND company_members.last_day IS NULL";
        $anniversaryResult = $this->db->query($sql)->result_array();
        $sql = "SELECT DISTINCT leader_id FROM `leader` WHERE leader_id != 0;";
        $leaderIdKey = $this->db->query($sql)->result_array();
        $mailToSend = array();
        for($i = 0; $i < count($leaderIdKey); ++$i)
        {
            $mailToSend[$leaderIdKey[$i]['leader_id']] = array("subject" => "","body" => "","receiver" => "","flag" => 0);
        }
        for($i = 0;$i < count($anniversaryResult); ++$i)
        {
            $sql = "SELECT users.user_id,mail,users.auto_id FROM users LEFT OUTER JOIN leader ON leader.leader_id = users.auto_id WHERE leader.user_id  = ?;";
            $leaderInfo = $this->db->query($sql,array($anniversaryResult[$i]['auto_id']))->result_array();//var_dump($leaderInfo);die;
            if(in_array($leaderInfo[0]['auto_id'],array_keys($mailToSend)))
            {
                $mailToSend[$leaderInfo[0]['auto_id']]['subject'] = "HR System Remind";
                $mailToSend[$leaderInfo[0]['auto_id']]['body'] .= "The anniversary of ".$anniversaryResult[$i]['name']." is at ".$anniversaryResult[$i]['start_date'].", please prepare for the performance review.<br/>";
                $mailToSend[$leaderInfo[0]['auto_id']]['receiver'] = $leaderInfo[0]['user_id'];
                $mailToSend[$leaderInfo[0]['auto_id']]['flag'] = 1;
            }
        }
        $sql = "SELECT * FROM users,company_members 
                WHERE users.user_id = company_members.work_email
                AND company_members.birthday LIKE '%-".$this->db->escape_like_str(date("m"))."-%' ESCAPE '!'
                AND company_members.last_day IS NULL";
        $birthdayResult = $this->db->query($sql)->result_array();
        for($i = 0;$i < count($birthdayResult); ++$i)
        {
            $sql = "SELECT users.user_id,mail,users.auto_id FROM users LEFT OUTER JOIN leader ON leader.leader_id = users.auto_id WHERE leader.user_id  = ?;";
            $leaderInfo = $this->db->query($sql,array($birthdayResult[$i]['auto_id']))->result_array();//var_dump($leaderInfo);die;
            if(in_array($leaderInfo[0]['auto_id'],array_keys($mailToSend)))
            {
                $mailToSend[$leaderInfo[0]['auto_id']]['subject'] = "HR System Remind";
                $mailToSend[$leaderInfo[0]['auto_id']]['body'] .= "The birthday of ".$birthdayResult[$i]['name']." is at ".date("m-d",strtotime($birthdayResult[$i]['birthday'])).".<br/>";
                $mailToSend[$leaderInfo[0]['auto_id']]['receiver'] = $leaderInfo[0]['user_id'];
                $mailToSend[$leaderInfo[0]['auto_id']]['flag'] = 1;
            }
        }
//        print_r($mailToSend);die;
        $sql = "SELECT * FROM users,company_members 
                WHERE users.user_id = company_members.work_email
                AND company_members.probation_date LIKE '%-".$this->db->escape_like_str(date("m"))."-%' ESCAPE '!'
                AND company_members.last_day IS NULL";
        $probationResult = $this->db->query($sql)->result_array();
        for($i = 0;$i < count($probationResult); ++$i)
        {
            $sql = "SELECT users.user_id,mail,users.auto_id FROM users LEFT OUTER JOIN leader ON leader.leader_id = users.auto_id WHERE leader.user_id  = ?;";
            $leaderInfo = $this->db->query($sql,array($probationResult[$i]['auto_id']))->result_array();//var_dump($leaderInfo);die;
            if(in_array($leaderInfo[0]['auto_id'],array_keys($mailToSend)))
            {
                $mailToSend[$leaderInfo[0]['auto_id']]['subject'] = "HR System Remind";
                $mailToSend[$leaderInfo[0]['auto_id']]['body'] .= "The probation_date of ".$probationResult[$i]['name']." is at ".$probationResult[$i]['probation_date'].".<br/>";
                $mailToSend[$leaderInfo[0]['auto_id']]['receiver'] = $leaderInfo[0]['user_id'];
                $mailToSend[$leaderInfo[0]['auto_id']]['flag'] = 1;
            }
        }
        foreach ($mailToSend as $key => $value)
        {
            if($value['flag'] == 1)
            {
                $this->mail_model->send($value['subject'],$value['body'],array("0" => array("user_id" => $value['receiver'], "mail" => $value['receiver'])));
            }
        }
        return $mailToSend;

    }
}