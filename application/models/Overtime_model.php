<?php
require_once(getcwd().'/application/models/SS_model.php');

class Overtime_model extends SS_model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->library('encryption');
	}

	public function getOvertimeByMonthState($date)
	{
		$sql = 'SELECT `auto_id`,  DATE_FORMAT(`create_time`,"%Y-%m-%d %H:%i") as create_time, DATE_FORMAT(`start_time`,"%Y-%m-%d %H:%i") as start_time, DATE_FORMAT(`end_time`,"%Y-%m-%d %H:%i") as end_time, `time_length`,`pay_or_day`,`compension`,`state` FROM `overtime_apply` WHERE employee_id = ? AND DATE_FORMAT(start_time,"%Y-%m") = ? AND (state = "pending" OR state = "approved_by_mgr") ORDER BY start_time DESC ;';
		return $this->db->query($sql,array($_SESSION['user_id'],$date))->result_array();
	}
	public function _insertOvertimeApply($data) 
	{
		return $this->_redisKeyInterface("_insertOvertimeApply","_insertOvertimeApply",$data);
	}
	public function insertOvertimeApply($data)
	{
        if(strtotime($data['start_time']) > strtotime($data['end_time']) || empty($data['compensation']) || empty($data['pOd']) )
        {
            return array("code"=>"102","text"=>"Please enter Leave Time in correct format");
        }
        if(empty($data['end_time']) || empty($data['start_time']))
        {
            return array("code"=>"102","text"=>"Please enter both the Start Time and Leave Time");
        }
        if(empty($data['time_length']))
        {
            return array("code"=>"102","text"=>"Please click the button to generate time length");
        }
        if(empty($data['employee_id']))
        {
            return array("code"=>"102","text"=>"Please choose an employee");
        }
        if(empty($data['reason']) )
        {
            return array("code"=>"102","text"=>"Please enter reason");
        }
		switch($data['compensation'])
		{
			case 1:
			$day_amount = 1;break;
			case 2:
			$day_amount = 2;break;
			case 3:
			$day_amount = 3;break;
			case 4:
			$day_amount = 4;break;
			default : $day_amount = 0;break;
		}
		$date_now = date("Y-m-d H:i:s");
		$dayFlag = $data['pOd'] == "day" ? TRUE : FALSE;

        $data['reason'] = empty($data['reason']) ? "" : $data['reason'];

		if($dayFlag)
		{
			
			$sql = "INSERT INTO `overtime_apply`(`auto_id`, `manager_id`, `employee_id`, `create_time`, `start_time`, `end_time`, `time_length`, `pay_or_day`, `compension`, `reason`, `hr_id`,`state`) VALUES (null,?,?,?,?,?,?,?,?,?,?,?) ;";
			
			$res = $this->db->query($sql ,array($_SESSION['user_id'],$data['employee_id'],$date_now,$data['start_time'],$data['end_time'],$data['time_length'],$data['pOd'],$day_amount,$data['reason'],0,"approved_by_mgr"));

			$sql = 'SELECT auto_id FROM overtime_apply WHERE manager_id = ? AND employee_id = ? AND create_time = ?;';
			$id = $this->db->query($sql,array($_SESSION['user_id'],$data['employee_id'],$date_now))->row_array();
			$id = $id['auto_id'];
			$year = $this->db->query("select max(cal_year) as year FROM leave_days WHERE user_id = ?; ",array($data['employee_id']))->row_array();
			$year = $year['year'];

			$sql = "UPDATE leave_days SET leave_balance = leave_balance + ? WHERE user_id = ? AND cal_year = ? ;";
			$this->db->query($sql,array($day_amount,$data['employee_id'],$year));

			$sql = 'INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,? ,? ,? ,? ,? ,? );';
			$this->db->query($sql ,array($_SESSION['user_id'],$id,'pend-overtime-day',$date_now,$day_amount,0));
			return array("code"=>"200");
			//$this->db->query($sql, array($employee_id,$))
		}

		else
		{
			$sql = "INSERT INTO `overtime_apply`(`auto_id`, `manager_id`, `employee_id`, `create_time`, `start_time`, `end_time`, `time_length`, `pay_or_day`, `compension`, `reason`, `hr_id`,`state`) VALUES (null,?,?,?,?,?,?,?,?,?,?,?) ;";
			
			$res = $this->db->query($sql ,array($_SESSION['user_id'],$data['employee_id'],$date_now,$data['start_time'],$data['end_time'],$data['time_length'],$data['pOd'],$day_amount,$data['reason'],0,"approved_by_mgr"));
			$sql = 'INSERT INTO `overtime_pay_log`(`auto_id`, `employee_id`, `manager_id`, `create_time`, `amount`) VALUES (null,?,?,?,?);';
			$this->db->query($sql ,array($data['employee_id'],$_SESSION['user_id'],$date_now,$day_amount));

			$sql = 'SELECT auto_id FROM overtime_apply WHERE manager_id = ? AND employee_id = ? AND create_time = ?;';
			$id = $this->db->query($sql,array($_SESSION['user_id'],$data['employee_id'],$date_now))->row_array();
			$id = $id['auto_id'];
			
			$sql = 'INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,? ,? ,? ,? ,? ,? );';
			$this->db->query($sql ,array($_SESSION['user_id'],$id,'pend-overtime-pay',$date_now,0,0));
			return array("code"=>"200");
		}
	}

	public function overtimeDetailState($id)
	{
		$sql = 'SELECT * FROM `overtime_apply` LEFT OUTER JOIN profile ON overtime_apply.employee_id = profile.user_id WHERE auto_id = ? AND employee_id = ?;';
		return $this->db->query($sql,array($id,$_SESSION['user_id']))->row_array();
	}

	public function newSelfOvertimeState($data)
    {

        if(strtotime($data['start_time']) > strtotime($data['end_time']) || empty($data['compensation']) || empty($data['pOd']) )
        {
            return array("code"=>"102","text"=>"Please enter Leave Time in correct format");
        }
        if(empty($data['end_time']) || empty($data['start_time']))
        {
            return array("code"=>"102","text"=>"Please enter both the Start Time and Leave Time");
        }
        if(empty($data['time_length']))
        {
            return array("code"=>"102","text"=>"Please click the button to generate time length");
        }
        $data['reason'] = empty($data['reason']) ? "" : $data['reason'];

        if(empty($data['time_length']) ) return array("code"=>"103","text"=>"Please enter reason");
        switch($data['compensation'])
        {
            case 1:
                $day_amount = 1;break;
            case 2:
                $day_amount = 2;break;
            case 3:
                $day_amount = 3;break;
            case 4:
                $day_amount = 4;break;
            default : $day_amount = 0;break;
        }
        $date_now = date("Y-m-d H:i:s");
        $dayFlag = $data['pOd'] == "day" ? TRUE : FALSE;

        $leader_id = $this->getManagerId();
        if($dayFlag)
        {

            $sql = "INSERT INTO `overtime_apply`(`auto_id`, `manager_id`, `employee_id`, `create_time`, `start_time`, `end_time`, `time_length`, `pay_or_day`, `compension`, `reason`, `hr_id`,`state`) VALUES (null,?,?,?,?,?,?,?,?,?,?,?) ;";

            $res = $this->db->query($sql ,array($leader_id,$_SESSION['user_id'],$date_now,$data['start_time'],$data['end_time'],$data['time_length'],$data['pOd'],$day_amount,$data['reason'],0,"pending"));

            $sql = 'INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,? ,? ,? ,? ,? ,? );';
            $this->db->query($sql ,array($_SESSION['user_id'],0,'pend-overtime-day',$date_now,$day_amount,0));
            return array("code"=>"200");
            //$this->db->query($sql, array($employee_id,$))
        }

        else
        {
            $sql = "INSERT INTO `overtime_apply`(`auto_id`, `manager_id`, `employee_id`, `create_time`, `start_time`, `end_time`, `time_length`, `pay_or_day`, `compension`, `reason`, `hr_id`,`state`) VALUES (null,?,?,?,?,?,?,?,?,?,?,?) ;";

            $res = $this->db->query($sql ,array($leader_id,$_SESSION['user_id'],$date_now,$data['start_time'],$data['end_time'],$data['time_length'],$data['pOd'],$day_amount,$data['reason'],0,"pending"));


            $sql = 'INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,? ,? ,? ,? ,? ,? );';
            $this->db->query($sql ,array($_SESSION['user_id'],0,'pend-overtime-pay',$date_now,0,0));
            return array("code"=>"200");
        }
    }

    public function getManagerId()
    {
        $leader_id = $this->db->query("SELECT leader_id FROM leader WHERE user_id = ?",array($_SESSION['user_id']))->row_array();
        $leader_id = $leader_id['leader_id'];
        return $leader_id;
    }

    public function updateOvertimeApplyCancel($data)
    {
        $res = $this->overtimeDetailState($data['id']);
        if(!isset($res['auto_id']) || $res['state'] != "pending") return array("code"=>"106","text"=>"No Authentication");

        switch($res['pay_or_day'])
        {
            case "pay" :

                $sql = "INSERT INTO overtime_pay_log (auto_id,employee_id,manager_id,create_time,amount,opertation) VALUES (null,?,?,?,?,?);";
                $this->db->query($sql ,array($res['employee_id'],$_SESSION['user_id'],date("Y-m-d H:i:s"),$res['compension'],"minus"));

                break;
            case "day":
                break;
            default:
                break;
        }
        $sql = "UPDATE overtime_apply SET state = 'cancelled' WHERE auto_id = ? ;";
        $this->db->query($sql , array($data['id']));
        return array("code"=>"200");
    }
}
