<?php
require_once(getcwd().'/application/models/SS_model.php');

class Leave_model extends SS_model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->library('encryption');
	}

	public function getTotalLeaveDays($year=0)
	{

		$sql = 'SELECT DATE_FORMAT(start_time,"%m") as month ,COUNT(leave_apply.auto_id) as num FROM leave_apply WHERE DATE_FORMAT(start_time,"%Y") = ? AND applier_id = ? AND leave_type = "leave" AND state ="approved_by_mgr" GROUP BY DATE_FORMAT(start_time,"%m");';
		if($year == 0)  $year = date("Y");
		$data =  $this->db->query($sql,array($year,$_SESSION['user_id']))->result_array();
		$res = array();
		for($i=0;$i<12;$i++)
		{
			$res[$i] = 0;
		}
		foreach($data as $r=>$v)
		{
			$res[$v['month']-1] =(int) $v['num'];
		}
		return $res;
		//var_dump($res) ;exit;
	}

	public function getTotalSickDays($year=0)
	{
		$sql = 'SELECT DATE_FORMAT(start_time,"%m") as month ,COUNT(leave_apply.auto_id) as num FROM leave_apply WHERE DATE_FORMAT(start_time,"%Y") = ? AND applier_id = ? AND leave_type = "sick_leave" AND state ="approved_by_mgr" GROUP BY DATE_FORMAT(start_time,"%m");';
		if($year == 0)  $year = date("Y");
		$data =  $this->db->query($sql,array($year,$_SESSION['user_id']))->result_array();
		// var_dump($data);exit;
		$res = array();
		for($i=0;$i<12;$i++)
		{
			$res[$i] = 0;
		}
		foreach($data as $r=>$v)
		{
			$res[$v['month']-1] =(int) $v['num'];
			//parse_int
		}
		return $res;
	}

	public function getLeaveByMonthState($date)
	{
		$sql = 'SELECT `auto_id`,  DATE_FORMAT(`create_time`,"%Y-%m-%d %H:%i") as create_time, DATE_FORMAT(`start_time`,"%Y-%m-%d %H:%i") as start_time, DATE_FORMAT(`end_time`,"%Y-%m-%d %H:%i") as end_time, `time_length`,  `state` FROM `leave_apply` WHERE applier_id = ? AND DATE_FORMAT(start_time,"%Y-%m") = ? AND leave_type = ? AND (state = "pending" OR state = "approved_by_mgr") ORDER BY start_time DESC ;';
		return $this->db->query($sql,array($_SESSION['user_id'],$date,"leave"))->result_array();
	}

	public function getSickLeaveByMonthState($date)
	{
		$sql = 'SELECT `auto_id`,  DATE_FORMAT(`create_time`,"%Y-%m-%d %H:%i") as create_time, DATE_FORMAT(`start_time`,"%Y-%m-%d %H:%i") as start_time, DATE_FORMAT(`end_time`,"%Y-%m-%d %H:%i") as end_time, `time_length`,  `state` FROM `leave_apply` WHERE applier_id = ? AND DATE_FORMAT(start_time,"%Y-%m") = ? AND leave_type = ? AND (state = "pending" OR state = "approved_by_mgr") ORDER BY start_time DESC ;';
		return $this->db->query($sql,array($_SESSION['user_id'],$date,"sick_leave"))->result_array();
	}

	public function getYears()
	{
		$sql = 'SELECT DISTINCT(DATE_FORMAT(cal_year,"%Y")) as year FROM leave_days WHERE user_id = ?;';
		return $this->db->query($sql,array($_SESSION['user_id']))->result_array();
	}

	public function insertLeaveApply($data)
	{
		//return $this->getLeaveAndSickDaysState();
		return $this->_redisKeyInterFace("_insertLeaveApply","_insertLeaveApply",$data);
	}

	public function _insertLeaveApply($data)
	{
		
		if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave')
		{
			return array("code"=>"101","text"=>"wrong leave type");
		}
		if(strtotime($data['start_time']) > strtotime($data['end_time']) || empty($data['end_time']) || empty($data['start_time']) || empty($data['time_length'])  )
		{
			return array("code"=>"102","text"=>"Please enter Leave Time in correct format");
		}
		$data['reason'] = empty($data['reason']) ? "" : $data['reason'];
		$count = 0;
		$day = date("Y-m-d",strtotime($data['start_time'])) ;
		$val = (int)((strtotime($data['end_time']) - strtotime($data['start_time']))/3600.0+0.5);
		if($val%24 == 9 || $val%24 == 23)
		{
			$val = floor($val/24) +1;
		}
		else
		{
			$val = floor($val/24)+($val%24)/8.0;
		}
		while( $day <= date("Y-m-d",strtotime($data['end_time']))  )
		{
			if(date('w',strtotime($day)) == 6 || date('w',strtotime($day)) == 0)
			{
				$count++;
			}
			$day = date('Y-m-d', strtotime ("+1 day", strtotime($day)));
		}
		$val = $val - $count;
		//return $val;
		$available_days = $this->getLeaveAndSickDaysState();

		if($data['leave_type'] == "leave" && $val > $available_days['leave_days'] || $data['leave_type'] == "sick_leave" && $val > $available_days['sick_days'])
		{
			return array("code"=>"103","text"=>"Your paid Leave/Sick days are not enough ,please change the amount of leave/sick days");
		} 

		//Insert new Leave
		$data['reason'] = trim($data['reason']);
		$sql = "INSERT INTO `leave_apply`(`auto_id`, `applier_id`, `create_time`, `start_time`, `end_time`, `time_length`, `hr_id`, `leave_type`, `reason`, `state`) VALUES (null,?,?,?,?,?,?,?,?,?) ;";
		$this->db->query($sql,array($_SESSION['user_id'],date("Y-m-d H:i:s"),$data['start_time'],$data['end_time'],$val."d",0,$data['leave_type'],$data['reason'],"pending"));

		//Insert leave  log 
		$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?) ;";
		
		$this->db->query($sql ,array($_SESSION['user_id'],"pend-leave",date("Y-m-d H:i:s"),0,0));

		//Mail to Manager
		$data = $this->getNameAndMgrMail($_SESSION['user_id']);
		$data['title'] = "You have received a new leave request";
		$data['content'] = "You have received a new leave request from ".$data['user_name']."\nPlease log in and check .\nhttp://alexfu.cc/approve;";
		$va = "mail -s '".$data['title']."' ".$data['to']." < leave.con";
		shell_exec($va);
		//$this->mail_to($data);


		return array("code"=>"200");
	}

	public function _updateApplyCancel($data)
	{
		if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave')
		{
			return array("code"=>"102","text"=>"wrong leave type");
		}
		$sql = 'SELECT auto_id FROM leave_apply WHERE auto_id = ? AND applier_id = ? AND state = ? ;';
		$res = $this->db->query($sql,array($data['id'],$_SESSION['user_id'],"pending"))->row_array();
		if(isset($res['auto_id']))
		{
			$sql = "UPDATE leave_apply SET state = 'cancelld' WHERE auto_id = ? ;";
			$this->db->query($sql,array($data['id']));

			$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";

			$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"cancel-".$data['leave_type'],date("Y-m-d H:i:s"),0,0));



			return array("code"=>"200");
		}

		return array("code"=>"102","text"=>"Cancel Operation has been denyed,While the manager/hr has approved your request  OR it is on cancelld state");
	}
	public function updateApplyCancel($data)
	{
		return $this->_redisKeyInterFace("_updateApplyCancel","_updateApplyCancel",$data);
	}

//	public function pendState($data)
//	{
//		if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave')
//		{
//			return array("code"=>"102","text"=>"wrong leave type");
//		}
//		$sql = 'SELECT auto_id FROM leave_apply WHERE auto_id = ? AND applier_id = ? AND state = ? ;';
//		$res = $this->db->query($sql,array($data['id'],$_SESSION['user_id'],"cancelld"))->row_array();
//		if(isset($res['auto_id']))
//		{
//			$sql = "UPDATE leave_apply SET state = 'pending' WHERE auto_id = ? ;";
//			$this->db->query($sql,array($data['id']));
//
//			$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";
//
//			$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"re-pend-".$data['leave_type'],date("Y-m-d H:i:s"),0,0));
//
//
//
//			return array("code"=>"200");
//		}
//		return array("code"=>"102","text"=>"Pending Operation has been denyed,While the manager/hr has approved your request OR it is on pending state");
//
//	}

	public function detailState($id)
	{
		$sql = "SELECT * FROM leave_apply WHERE auto_id = ? AND applier_id = ? ;";
		return $this->db->query($sql,array($id,$_SESSION['user_id']))->row_array();
	}

	public function editState($data)
	{
		if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave')
		{
			return array("code"=>"102","text"=>"wrong leave type");
		}


		$sql = 'SELECT auto_id,leave_type,start_time,end_time FROM leave_apply WHERE auto_id = ? AND applier_id = ? AND state = ? OR state = ?  ;';
		$res = $this->db->query($sql,array($data['id'],$_SESSION['user_id'],"pending","cancelld"))->row_array();

		if(isset($res['auto_id']))
		{

			$val = (int)((strtotime($res['end_time']) - strtotime($res['start_time']))/3600.0+0.5);
			$val = floor($val/24)+($val%24)/8.0;
			$count = 0;
			$day = date("Y-m-d",strtotime($res['start_time'])) ;

			while( $day <= date("Y-m-d",strtotime($res['end_time']))  )
			{
				if(date('w',strtotime($day)) == 6 || date('w',strtotime($day)) == 0)
				{
					$count++;
				}
				$day = date('Y-m-d', strtotime ("+1 day", strtotime($day)));
			}
			$val = $val - $count;
			$available_days = $this->getLeaveAndSickDaysState();
			if($res['leave_type'] == "leave" && $val > $available_days['leave_days'] || $res['leave_type'] == "sick_leave" && $val > $available_days['sick_days'])
			{
				return array("code"=>"103","text"=>"Your paid Leave/Sick days are not enough ,please change the amount of leave/sick days");
			}
			$sql = "UPDATE leave_apply SET start_time = ? ,end_time = ? ,time_length = ?,reason = ?  WHERE auto_id = ? ;";
			$this->db->query($sql,array($data['start_time'],$data['end_time'],$data['time_length'],$data['reason'],$data['id']));

			$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";

			$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"edit-".$data['leave_type'],date("Y-m-d H:i:s"),0,0));
			return array("code"=>"200");
		}
		return array("code"=>"102","text"=>"Edit Operation has been denyed,While the manager/hr has approved your request!");
	}

	public function getNameAndMgrMail($id)
	{
		$sql = "SELECT mine.user_name ,mgr.user_id as `to` FROM profile as mine LEFT OUTER JOIN leader ON mine.user_id = leader.user_id LEFT OUTER JOIN users as mgr ON leader.leader_id = mgr.auto_id WHERE mine.user_id = ?;";
		return $this->db->query($sql,array($id))->row_array();
	}

	public function getLeaveAndSickDaysState()
	{
		$sql = "SELECT leave_balance,sick_leave_balance FROM leave_days WHERE user_id = ? ORDER BY cal_year DESC LIMIT 2 ;";
		$data = $this->db->query($sql,array($_SESSION['user_id']))->result_array();
		$res = array();
		if(count($data) == 1)
		{
			$res['leave_days'] = $data[0]['leave_balance'];
		}
		else
		{
			$res['leave_days'] = $data[0]['leave_balance']+$data[1]['leave_balance'];
		}
		$res['sick_days'] = $data[0]['sick_leave_balance'];
		return $res;
	}



	public function getUnpaidLeaveByMonthState($date)
	{
		$sql = 'SELECT `auto_id`,  DATE_FORMAT(`create_time`,"%Y-%m-%d %H:%i") as create_time, DATE_FORMAT(`start_time`,"%Y-%m-%d %H:%i") as start_time, DATE_FORMAT(`end_time`,"%Y-%m-%d %H:%i") as end_time, `time_length`,  `state`,leave_type FROM `unpaid_leave_apply` WHERE applier_id = ? AND DATE_FORMAT(start_time,"%Y-%m") = ?  ORDER BY start_time DESC ;';
		return $this->db->query($sql,array($_SESSION['user_id'],$date))->result_array();
	}

	public function getActualPayDaysState($data)
	{
//		if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave' && $data['leave_type'] != "overtime")
//		{
//			return array("code"=>"102","text"=>"wrong leave type");
//		}
		if(strtotime($data['start_time']) > strtotime($data['end_time']) || empty($data['end_time']) || empty($data['start_time']) )
		{
			return array("code"=>"102","text"=>"Please enter Leave Time in correct format","amount"=>"-1");
		}
		$count = 0;
		$day = date("Y-m-d",strtotime($data['start_time'])) ;
		$val = (int)((strtotime($data['end_time']) - strtotime($data['start_time']))/3600.0+0.5);
		if($val%24 == 9 || $val%24 == 23)
		{
			$val = floor($val/24) +1;
		}
		else
		{
			$val = floor($val/24)+($val%24)/8.0;
		}
		if($data['leave_type'] != "overtime")
        {
            while( $day <= date("Y-m-d",strtotime($data['end_time']))  )
            {
                if(date('w',strtotime($day)) == 6 || date('w',strtotime($day)) == 0)
                {
                    $count++;
                }
                $day = date('Y-m-d', strtotime ("+1 day", strtotime($day)));
            }
            $val = $val - $count;
        }

		return array("code"=>"200",'amount'=>$val);
	}
}