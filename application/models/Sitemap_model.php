<?php
error_reporting(0);
require_once(getcwd().'/application/models/SS_model.php');
class Sitemap_model extends SS_model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->library('encryption');
    }

    public function updateSubLinksState()
    {
        $sql = "TRUNCATE subordinate_links";
        $this->db->query($sql);
        $sql = 'SELECT MAX(auto_id) as num FROM users WHERE 1';
        $num = $this->db->query($sql)->row_array();
        $num = $num['num'];
        $array = array();
        for($i=1;$i<=$num ;$i++)
        {
            $array[$i] = array();
        }

        $sql = 'SELECT * FROM leader WHERE 1;';
        $res = $this->db->query($sql)->result_array();
        //var_dump($array);return;

        foreach($res as $obj)
        {
            if($obj['leader_id'] != NULL)
            array_push($array[$obj['leader_id']],$obj['user_id']);
        }
        $func = "INSERT INTO subordinate_links (user_id,sub_id) VALUES";//var_dump($array);exit;
        foreach($array as $r => $v)
        {
            $array[$r] = implode(",",$v);
            if(empty($array[$r])) $array[$r] = 0;
            $func.=" ({$r},'{$array[$r]}'),";
        }
        $func = substr($func,0,strlen($func)-1);
        $this->db->query($func);

    }



    public function getAllMapState()
    {
        $sql = "SELECT * FROM subordinate_links ;";
        $tree_old = $this->db->query($sql)->result_array();
        $tree_new = array();
        foreach($tree_old as $r=>$v)
        {
            $tree_new[$v['user_id']] = $v['sub_id'];
        }
        foreach($tree_new as $r =>$v)
        {
            $tree_new[$r] = explode(",",$v);
        }
        
        $i=82;
        $flag= 0;
        while($i>0)
        {
            if( $i == 76 && $flag == 0)
            {
                $flag = 1;
            }
            else
            {
                if(!isset($tree_new[$i]))
                {
                }
                else
                {
                    $v = $tree_new[$i];
                    foreach($v as $key => $value)
                    {
                        if(array_key_exists($value,$tree_new))
                        {
                            $tree_new[$i][$value] = $tree_new[$value];
                            unset($tree_new[$i][$key]);
                            unset($tree_new[$value]);
                        }
                    }
                    if($flag == 1 && $i == 55)
                    {
                        $v = $tree_new[76];
                        foreach($v as $key => $value)
                        {
                            if(array_key_exists($value,$tree_new))
                            {
                                $tree_new[76][$value] = $tree_new[$value];
                                unset($tree_new[76][$key]);
                                unset($tree_new[$value]);
                            }
                        }
                    }
                }

            }
            $i--;
        }



//        foreach($tree_new as $r=>$v)
//        {
//            foreach($v as $key => $value)
//            {
//                foreach($value as $key2=>$value2)
//                {
//                    if(array_key_exists($value2,$tree_new))
//                    {
//                        $tree_new[$r][$key][$value2] = $tree_new[$value2];
//                        unset($tree_new[$r][$key][$key2]);
//                        unset($tree_new[$value2]);
//                    }
//                }
//            }
//        }

//        foreach($tree_new as $r=>$v)
//        {
//            foreach($v as $key => $value)
//            {
//                foreach($value as $key2=>$value2)
//                {
//                    foreach($value2 as $key3=>$value3)
//                    if(array_key_exists($value3,$tree_new))
//                    {
//                        $tree_new[$r][$key][$key2][$value3] = $tree_new[$value3];
//                        unset($tree_new[$r][$key][$key2][$key3]);
//                        unset($tree_new[$value3]);
//                    }
//                }
//            }
//        }
        return $tree_new;
    }

    public function nameChart($map = null)
    {

        $sql = "SELECT user_id,user_name FROM profile WHERE 1";
        $res = $this->db->query($sql)->result_array();
        $res_new = array();
        foreach($res as $r=>$v)
        {
            $res_new[$v['user_id']] = $v['user_name'];
        }
        return $res_new;
    }

    public function getMapSingleState($data)
    {
        return $data;
    }

    public function getLinkedInfo($id)
    {
        $res = array();
        /*
         * Leader Part
         */
        $sql = "SELECT profile.department,profile.user_name, users.user_id,users.auto_id,profile.title FROM leader LEFT OUTER JOIN users ON leader.leader_id = users.auto_id LEFT OUTER JOIN profile ON users.auto_id = profile.user_id WHERE leader.user_id = ? ;";
        $leader = $this->db->query($sql,array($id))->row_array();
        if(!isset($leader['user_id']))
        {
            $leader = array("user_name"=>"","user_id"=>"","user_department"=>"");
            $ids = array("sub_id"=>"1,2,3");
        }
        else
        {

            $sql = "SELECT sub_id FROM subordinate_links WHERE user_id = ? ;";
            $ids = $this->db->query($sql,array($leader['auto_id']))->row_array();
        }
        $res['leader'] = "<div style='background-color:azure' class = 'person_td' id='{$leader['auto_id']}' onclick='getInfo({$leader['auto_id']})'><h4>".$leader['user_name']."</h4><h5>".$leader['title']."</h5><h5>".$leader['department']."</h5></div>";
        /*
          * Colleague Part
          */
        $coll = $this->db->query("SELECT profile.department,profile.user_name, users.user_id,users.auto_id,profile.title FROM users LEFT OUTER JOIN profile ON users.auto_id = profile.user_id LEFT OUTER JOIN company_members ON users.user_id = company_members.work_email WHERE company_members.last_day IS NULL AND users.auto_id IN ({$ids['sub_id']}) ")->result_array();
        $temp = array();
        foreach($coll as $co)
        {
            if($co['auto_id'] == $id)
            $temp[] = "<div style='background-color:aliceblue' class = 'person_td' id='{$co['auto_id']}' onclick='getInfo({$co['auto_id']})'><h4>".$co['user_name']."</h4><h5>".$co['title']."</h5><h5>".$co['department']."</h5></div>";
            else
            {
                $temp[] = "<div style='background-color:azure' class = 'person_td' id='{$co['auto_id']}' onclick='getInfo({$co['auto_id']})'><h4>".$co['user_name']."</h4><h5>".$co['title']."</h5><h5>".$co['department']."</h5></div>";
            }
        }
        $res['colleague'] = $temp;//var_dump($res);exit;
        /*
         * Team Part
         */
        $sql = "SELECT sub_id FROM subordinate_links WHERE user_id = ? ;";
        $ids = $this->db->query($sql,array($id))->row_array();
        $temp = array();
        if($ids['sub_id'] == "0")
        {
            $temp = array();
        }
        else
        {
            $team = $this->db->query("SELECT profile.department,profile.user_name, users.user_id,users.auto_id,profile.title
 FROM users LEFT OUTER JOIN profile ON users.auto_id = profile.user_id LEFT OUTER JOIN company_members ON users.user_id = company_members.work_email WHERE company_members.last_day IS NULL AND users.auto_id IN ({$ids['sub_id']}) ")->result_array();

            foreach($team as $co)
            {
                $temp[] = "<div style='background-color:azure' class = 'person_td' id='{$co['auto_id']}'  onclick='getInfo({$co['auto_id']})'><h4>".$co['user_name']."</h4><h5>".$co['title']."</h5><h5>".$co['department']."</h5></div>";
            }
        }

        $res['team'] = $temp;

        return $res;
    }

    public  function getBasicInfoState($id)
    {
        $sql = 'SELECT `name`, `chinese_name`, `work_email`, `phone_number`, `moblie_number`, `personal_email`, `skype` FROM company_members LEFT OUTER JOIN users ON company_members.work_email = users.user_id LEFT OUTER JOIN profile ON  profile.user_id= users.auto_id WHERE profile.user_id = ?   ;';
        return $this->db->query($sql,array($id))->row_array();
    }

    public function isOnShow()
    {
        $array = array(1,2,3,4);
        $res = $this->db->query("SELECT * FROM subordinate_links WHERE user_id in ? ",array($array))->result_array();
       // return $res;
        foreach($res as $r=>$v)
        {
            $temp = explode(",",$v['sub_id']);
            $array = array_merge($array,$temp);
        }
        return $array;
    }

}
