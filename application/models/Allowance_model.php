<?php
require_once(getcwd().'/application/models/SS_model.php');

class Allowance_model extends SS_model
{
    public $type_array;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->library('encryption');
        $this->type_array = array('visa related (SH)'=>"0.375d",'visa related (Medical)'=>"0.5d",'Police Reg.'=>"1d",'visa related(HK/Macau)'=>"1d",'visa related(home/3rd country)'=>"3d",'Funeral(1 day)'=>"1d",'Funeral(2 days)'=>"2d",'Funeral(3 days)'=>"3d",'5 years award'=>"5d",'Marriage leave'=>"10d",'Maternity leave'=>"128d");
    }


    public function getAllowanceByMonthState($date) //Get Unpaid By Month
    {
        $sql = 'SELECT `auto_id`,  DATE_FORMAT(`create_time`,"%Y-%m-%d %H:%i") as create_time, DATE_FORMAT(`start_time`,"%Y-%m-%d %H:%i") as start_time, DATE_FORMAT(`end_time`,"%Y-%m-%d %H:%i") as end_time, `time_length`,  `state`,leave_type FROM `allowance_apply` WHERE applier_id = ? AND DATE_FORMAT(start_time,"%Y-%m") = ?  AND (state = "pending" OR state = "approved_by_mgr") ORDER BY start_time DESC ;';
        return $this->db->query($sql,array($_SESSION['user_id'],$date))->result_array();
    }


    public function _cancelAllowanceState($data) //Cancel the apply
    {
//        if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave')
//        {
//            return array("code"=>"102","text"=>"wrong leave type");
//        }

        $sql = 'SELECT auto_id FROM allowance_apply WHERE auto_id = ? AND applier_id = ? AND state = ? ;';
        $res = $this->db->query($sql,array($data['id'],$_SESSION['user_id'],"pending"))->row_array();
        if(isset($res['auto_id']))
        {
            $sql = "UPDATE allowance_apply SET state = 'cancelld' WHERE auto_id = ? ;";
            $this->db->query($sql,array($data['id']));

            $sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";

            $this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"cancel-allowance",date("Y-m-d H:i:s"),0,0));

            return array("code"=>"200");
        }
        return array("code"=>"102","text"=>"Cancel Operation has been denied,While the manager/hr has approved your request  OR it is on cancelld state");
    }
    public function cancelAllowanceState($data)
    {
        return $this->_redisKeyInterFace("_cancelAllowanceState","_cancelAllowanceState",$data);
    }


//    public function pendUnpaidState($data)
//    {
//        if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave')
//        {
//            return array("code"=>"102","text"=>"wrong leave type");
//        }
//        $sql = 'SELECT auto_id FROM allowance_apply WHERE auto_id = ? AND applier_id = ? AND state = ? ;';
//        $res = $this->db->query($sql,array($data['id'],$_SESSION['user_id'],"cancelld"))->row_array();
//        if(isset($res['auto_id']))
//        {
//            $sql = "UPDATE allowance_apply SET state = 'pending' WHERE auto_id = ? ;";
//            $this->db->query($sql,array($data['id']));
//
//            $sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";
//
//            $this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"re-pend-".$data['leave_type'],date("Y-m-d H:i:s"),0,0));
//
//
//
//            return array("code"=>"200");
//        }
//        return array("code"=>"102","text"=>"Pending Operation has been denyed,While the manager/hr has approved your request OR it is on pending state");
//    }

    public function insertAllowanceApply($data)
    {
        //return array("code"=>"200");
        return $this->_redisKeyInterFace("_insertAllowanceApply","_insertAllowanceApply",$data);
    }

    public function _insertAllowanceApply($data)
    {
        //array_key_exists($data['leave_type'],$this->type_array)
        if(strtotime($data['start_time']) > strtotime($data['end_time']) || empty($data['end_time']) || empty($data['start_time']) || empty($data['time_length'])  )
        {
            return array("code"=>"102","text"=>"Please enter Leave Time in correct format");
        }
        $data['reason'] = empty($data['reason']) ? "" : $data['reason'];
        $count = 0;
        $day = date("Y-m-d",strtotime($data['start_time'])) ;
        $val = (int)((strtotime($data['end_time']) - strtotime($data['start_time']))/3600.0+0.5);
        if($val%24 == 9 || $val%24 == 23)
        {
            $val = floor($val/24) +1;
        }
        else
        {
            $val = floor($val/24)+($val%24)/8.0;
        }
        if($data['leave_type'] != "overtime")
        {
            while( $day <= date("Y-m-d",strtotime($data['end_time']))  )
            {
                if(date('w',strtotime($day)) == 6 || date('w',strtotime($day)) == 0)
                {
                    $count++;
                }
                $day = date('Y-m-d', strtotime ("+1 day", strtotime($day)));
            }
            $val = $val - $count;
        }
        //Insert new Leave
        $data['reason'] = trim($data['reason']);
        $sql = "INSERT INTO `allowance_apply`(`auto_id`, `applier_id`, `create_time`, `start_time`, `end_time`, `time_length`, `hr_id`, `leave_type`, `reason`, `state`) VALUES (null,?,?,?,?,?,?,?,?,?) ;";
        $this->db->query($sql,array($_SESSION['user_id'],date("Y-m-d H:i:s"),$data['start_time'],$data['end_time'],$val."d",0,$data['leave_type'],$data['reason'],"pending"));

        //Insert leave  log
        $sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";

        $this->db->query($sql ,array($_SESSION['user_id'],0,"pend-allowance-".$data['leave_type'],date("Y-m-d H:i:s"),0,0));



        return array("code"=>"200");
    }



    public function allowanceDetailState($id)
    {
        $sql = "SELECT * FROM allowance_apply WHERE auto_id = ? AND applier_id = ? ;";
        return $this->db->query($sql,array($id,$_SESSION['user_id']))->row_array();
    }
    public function getNameAndMgrMail($id)
    {
        $sql = "SELECT mine.user_name ,mgr.user_id as `to` FROM profile as mine LEFT OUTER JOIN leader ON mine.user_id = leader.user_id LEFT OUTER JOIN users as mgr ON leader.leader_id = mgr.auto_id WHERE mine.user_id = ?;";
        return $this->db->query($sql,array($id))->row_array();
    }

    public function editAllowanceState($data)
    {
//        if($data['leave_type'] != "leave" && $data['leave_type'] != 'sick_leave')
//        {
//            return array("code"=>"102","text"=>"wrong leave type");
//        }
        $data['reason'] = empty($data['reason']) ? "" : $data['reason'];

        $sql = 'SELECT auto_id,leave_type,start_time,end_time FROM allowance_apply WHERE auto_id = ? AND applier_id = ? AND state = ? OR state = ?  ;';
        $res = $this->db->query($sql,array($data['id'],$_SESSION['user_id'],"pending","cancelld"))->row_array();

        if(isset($res['auto_id']))
        {
            $sql = "UPDATE allowance_apply SET start_time = ? ,end_time = ? ,time_length = ?,reason = ?,leave_type = ?  WHERE auto_id = ? ;";
            $this->db->query($sql,array($data['start_time'],$data['end_time'],$data['time_length'],$data['reason'],$data['leave_type'],$data['id']));

            $sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";

            $this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"edit-allowance-".$data['leave_type'],date("Y-m-d H:i:s"),0,0));
            return array("code"=>"200");
        }
        return array("code"=>"102","text"=>"Edit Operation has been denied,While the manager/hr has approved your request!");
    }

    public function getEndTimeByTypeState($data)
    {
        $data['start_time'] .=":00";//return $data;

       $num =  floatval($this->type_array[$data['type']]);//var_dump( $num);exit;
        if($num < 1)
        {
            $num*= 8;//return $num;
            $end_day = date_create($data['start_time']);
            date_add($end_day,date_interval_create_from_date_string($num." hours"));
           // $end_day = (date_format($end_day,"Y-m-d H:i:s"));
            return array("end_date"=>date_format($end_day,"Y-m-d"),"end_time"=>date_format($end_day,"H:i"),"time_length"=>$num/8.0);
        }
        else
        {
            $end_day = date_create($data['start_time']);
            date_add($end_day,date_interval_create_from_date_string($num." days"));
            return array("end_date"=>date_format($end_day,"Y-m-d"),"end_time"=>date_format($end_day,"H:i"),"time_length"=>$num);
        }
    }
}