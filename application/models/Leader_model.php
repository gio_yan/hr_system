<?php
require_once(getcwd().'/application/models/SS_model.php');

class Leader_model extends SS_model
{
	protected $other_flag ;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->library('encryption');
		$this->other_flag = array("1"=>"Annual Leave","2"=>"Sick Leave","3"=>"Unpaid Leave","6"=>"Unpaid Sick Leave","5"=>"Overtime Pay",
			"10"=>"Overtime Day","11"=>'visa related (SH)',"22"=>'visa related (Medical)',"33"=>'Police Reg.',
			"44"=>'visa related(HK/Macau)',"55"=>'visa related(home/3rd country)',"66"=>'Funeral(1 day)',
			"77"=>'Funeral(2 days)',"88"=>'Funeral(3 days)', "99"=>'5 years award',"110"=>'Marriage leave',
			"121"=>'Maternity leave');
	}
	public function getSubWorkerState($leader_id)
	{
		$sql = "SELECT profile.user_id,profile.user_name , profile.title FROM leader LEFT OUTER JOIN profile ON leader.user_id = profile.user_id LEFT OUTER JOIN company_members ON company_members.name = profile.user_name  WHERE leader.leader_id = ? AND company_members.last_day IS NULL ";
		return $this->db->query($sql,array($leader_id))->result_array();
	}

	public function getLeaveApplyState($page = 0)
	{
		if(!is_numeric($page))
		{
			return array('code'=>'101' ,'content' => '页码不存在');
		}

		$pageSize  = 10;
		$start = $page - 3;
		$end = $page + 3;
		$res = array();


		//获取总数

		$sql = 'SELECT COUNT(leave_apply.auto_id) as num FROM users LEFT OUTER JOIN leader on users.auto_id = leader.leader_id LEFT OUTER JOIN profile ON leader.user_id = profile.user_id LEFT OUTER JOIN  leave_apply on leader.user_id = leave_apply.applier_id WHERE users.auto_id = ?  AND leave_apply.leave_type = "leave"  AND (state = "pending" OR state = "approved_by_mgr");';
		$result = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		$page_sum = isset($result['num']) ? $result['num']*1.0 : 0;


		if($page_sum/$pageSize < $page)
		{
			return array('code'=>'102' ,'content' => '页码不存在');
		}


		if($start < 0 ) $start = 0;
		if($end > $page_sum/$pageSize ) $end = ceil($page_sum/$pageSize);

		if($end == $start)
		{
			$res['pages'] = array(0);
		}
		else
		{
			$res['pages'] = range($start,($end-1)<=0 ? 0 : ($end-1));
		}
		$sql = 'SELECT leave_apply.auto_id,profile.user_name,start_time,end_time,time_length,leave_apply.state FROM users LEFT OUTER JOIN leader on users.auto_id = leader.leader_id LEFT OUTER JOIN profile ON leader.user_id = profile.user_id LEFT OUTER JOIN  leave_apply on leader.user_id = leave_apply.applier_id WHERE users.auto_id = ?  AND leave_apply.leave_type = "leave"  AND (state = "pending" OR state = "approved_by_mgr") ORDER BY create_time DESC LIMIT ?,? ;';
		$result =  $this->db->query($sql,array($_SESSION['user_id'],$pageSize*$page,$pageSize))->result_array();

		$res['content'] = $result;
		$res['page'] = $page;
		$res['all'] = ceil($page_sum/$pageSize);
		return $res;
	}



	public function getSickApplyState($page = 0)
	{

		if(!is_numeric($page))
		{
			return array('code'=>'101' ,'content' => '页码不存在');
		}

		$pageSize  = 10;
		$start = $page - 3;
		$end = $page + 3;
		$res = array();


		//获取总数

		$sql = 'SELECT COUNT(leave_apply.auto_id) as num FROM users LEFT OUTER JOIN leader on users.auto_id = leader.leader_id LEFT OUTER JOIN profile ON leader.user_id = profile.user_id LEFT OUTER JOIN  leave_apply on leader.user_id = leave_apply.applier_id WHERE users.auto_id = ?  AND leave_apply.leave_type = "sick_leave"  AND (state = "pending" OR state = "approved_by_mgr");';
		$result = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		$page_sum = isset($result['num']) ? $result['num']*1.0 : 0;


		if($page_sum/$pageSize < $page)
		{
			return array('code'=>'102' ,'content' => '页码不存在');
		}


		if($start < 0 ) $start = 0;
		if($end > $page_sum/$pageSize ) $end = ceil($page_sum/$pageSize);

		if($end == $start)
		{
			$res['pages'] = array(0);
		}
		else
		{
			$res['pages'] = range($start,($end-1)<=0 ? 0 : ($end-1));
		}
		$sql = 'SELECT leave_apply.auto_id,profile.user_name,start_time,end_time,time_length,leave_apply.state FROM users LEFT OUTER JOIN leader on users.auto_id = leader.leader_id LEFT OUTER JOIN profile ON leader.user_id = profile.user_id LEFT OUTER JOIN  leave_apply on leader.user_id = leave_apply.applier_id WHERE users.auto_id = ?  AND leave_apply.leave_type = "sick_leave" AND (state = "pending" OR state = "approved_by_mgr") ORDER BY create_time DESC LIMIT ?,? ;';
		$result =  $this->db->query($sql,array($_SESSION['user_id'],$pageSize*$page,$pageSize))->result_array();

		$res['content'] = $result;
		$res['page'] = $page;
		$res['all'] = ceil($page_sum/$pageSize);
		return $res;

	}

	public function getUnpaidApplyState($page = 0)
	{
		if(!is_numeric($page))
		{
			return array('code'=>'101' ,'content' => '页码不存在');
		}

		$pageSize  = 10;
		$start = $page - 3;
		$end = $page + 3;
		$res = array();


		//获取总数

		$sql = 'SELECT COUNT(unpaid_leave_apply.auto_id) as num  FROM users LEFT OUTER JOIN leader on users.auto_id = leader.leader_id LEFT OUTER JOIN profile ON leader.user_id = profile.user_id LEFT OUTER JOIN  unpaid_leave_apply on leader.user_id = unpaid_leave_apply.applier_id WHERE users.auto_id = ?  AND (state = "pending" OR state = "approved_by_mgr")  ;';
		$result = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		$page_sum = isset($result['num']) ? $result['num']*1.0 : 0;


		if($page_sum/$pageSize < $page)
		{
			return array('code'=>'102' ,'content' => '页码不存在');
		}


		if($start < 0 ) $start = 0;
		if($end > $page_sum/$pageSize ) $end = ceil($page_sum/$pageSize);

		if($end == $start)
		{
			$res['pages'] = array(0);
		}
		else
		{
			$res['pages'] = range($start,($end-1)<=0 ? 0 : ($end-1));
		}
		$sql = 'SELECT unpaid_leave_apply.auto_id,profile.user_name,start_time,end_time,time_length,leave_type,state FROM users RIGHT OUTER JOIN leader on users.auto_id = leader.leader_id RIGHT OUTER JOIN profile ON leader.user_id = profile.user_id RIGHT OUTER JOIN  unpaid_leave_apply on leader.user_id = unpaid_leave_apply.applier_id WHERE users.auto_id = ?  AND (state = "pending" OR state = "approved_by_mgr")  ORDER BY create_time DESC LIMIT ?,? ;';
		$result =  $this->db->query($sql,array($_SESSION['user_id'],$pageSize*$page,$pageSize))->result_array();

		$res['content'] = $result;
		$res['page'] = $page;
		$res['all'] = ceil($page_sum/$pageSize);
		return $res;


	}

	public function getOvertimeApplyState($page = 0)
	{

		if(!is_numeric($page))
		{
			return array('code'=>'101' ,'content' => '页码不存在');
		}

		$pageSize  = 10;
		$start = $page - 3;
		$end = $page + 3;
		$res = array();


		//获取总数

		$sql = 'SELECT COUNT(`auto_id`) as num FROM `overtime_apply` LEFT OUTER JOIN profile ON overtime_apply.employee_id = profile.user_id WHERE manager_id = ? AND (state = "pending" OR state = "approved_by_mgr")   ORDER BY start_time DESC;';
		$result = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		$page_sum = isset($result['num']) ? $result['num']*1.0 : 0;


		if($page_sum/$pageSize < $page)
		{
			return array('code'=>'102' ,'content' => '页码不存在');
		}


		if($start < 0 ) $start = 0;
		if($end > $page_sum/$pageSize ) $end = ceil($page_sum/$pageSize);

		if($end == $start)
		{
			$res['pages'] = array(0);
		}
		else
		{
			$res['pages'] = range($start,($end-1)<=0 ? 0 : ($end-1));
		}
		$sql = 'SELECT `auto_id`,  DATE_FORMAT(`create_time`,"%Y-%m-%d %H:%i") as create_time, DATE_FORMAT(`start_time`,"%Y-%m-%d %H:%i") as start_time, DATE_FORMAT(`end_time`,"%Y-%m-%d %H:%i") as end_time, `time_length`,`pay_or_day`,`compension`,user_name,state FROM `overtime_apply` LEFT OUTER JOIN profile ON overtime_apply.employee_id = profile.user_id WHERE manager_id = ? AND (state = "pending" OR state = "approved_by_mgr")   ORDER BY start_time DESC LIMIT ?,? ;';
		$result =  $this->db->query($sql,array($_SESSION['user_id'],$pageSize*$page,$pageSize))->result_array();

		$res['content'] = $result;
		$res['page'] = $page;
		$res['all'] = ceil($page_sum/$pageSize);
		return $res;
	}



	public function getAllowanceApplyState($page = 0)
	{
		if(!is_numeric($page))
		{
			return array('code'=>'101' ,'content' => '页码不存在');
		}

		$pageSize  = 10;
		$start = $page - 3;
		$end = $page + 3;
		$res = array();


		//获取总数

		$sql = 'SELECT COUNT(allowance_apply.auto_id) as num  FROM users LEFT OUTER JOIN leader on users.auto_id = leader.leader_id LEFT OUTER JOIN profile ON leader.user_id = profile.user_id LEFT OUTER JOIN  allowance_apply on leader.user_id = allowance_apply.applier_id WHERE users.auto_id = ? AND (state = "pending" OR state = "approved_by_mgr")  ;';
		$result = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		$page_sum = isset($result['num']) ? $result['num']*1.0 : 0;


		if($page_sum/$pageSize < $page)
		{
			return array('code'=>'102' ,'content' => '页码不存在');
		}


		if($start < 0 ) $start = 0;
		if($end > $page_sum/$pageSize ) $end = ceil($page_sum/$pageSize);

		if($end == $start)
		{
			$res['pages'] = array(0);
		}
		else
		{
			$res['pages'] = range($start,($end-1)<=0 ? 0 : ($end-1));
		}
		$sql = 'SELECT allowance_apply.auto_id,profile.user_name,start_time,end_time,time_length,leave_type,state FROM users RIGHT OUTER JOIN leader on users.auto_id = leader.leader_id RIGHT OUTER JOIN profile ON leader.user_id = profile.user_id RIGHT OUTER JOIN  allowance_apply on leader.user_id = allowance_apply.applier_id WHERE users.auto_id = ? AND (state = "pending" OR state = "approved_by_mgr")   ORDER BY create_time DESC LIMIT ?,? ;';
		$result =  $this->db->query($sql,array($_SESSION['user_id'],$pageSize*$page,$pageSize))->result_array();

		$res['content'] = $result;
		$res['page'] = $page;
		$res['all'] = ceil($page_sum/$pageSize);//var_dump($res);exit;
		return $res;


	}

	public function getMcApplyState($page = 0)
	{
		if(!is_numeric($page))
		{
			return array('code'=>'101' ,'content' => '页码不存在');
		}

		$pageSize  = 10;
		$start = $page - 3;
		$end = $page + 3;
		$res = array();


		//获取总数

		$sql = 'SELECT COUNT(`auto_id`) as num FROM `missing_card` LEFT OUTER JOIN leader ON missing_card.user_id = leader.user_id WHERE leader_id = ? AND (state = "pending" OR state = "approved")   ORDER BY record_time DESC;';
		$result = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		$page_sum = isset($result['num']) ? $result['num'] : 0;


		if($page_sum/$pageSize < $page)
		{
			return array('code'=>'102' ,'content' => 'Illegal page number');
		}


		if($start < 0 ) $start = 0;
		if($end > $page_sum/$pageSize ) $end = ceil($page_sum/$pageSize);

		if($end == $start)
		{
			$res['pages'] = array(0);
		}
		else
		{
			$res['pages'] = range($start,($end-1)<=0 ? 0 : ($end-1));
		}
		$sql = 'SELECT `auto_id`,  DATE_FORMAT(`record_time`,"%Y-%m-%d %H:%i") as record_time, DATE_FORMAT(`start_time`,"%Y-%m-%d %H:%i") as start_time, DATE_FORMAT(`end_time`,"%Y-%m-%d %H:%i") as end_time,user_name,state FROM `missing_card` LEFT OUTER JOIN leader LEFT OUTER JOIN profile ON leader.user_id = profile.user_id ON missing_card.user_id = leader.user_id WHERE leader_id = ? AND (state = "pending" OR state = "approved")   ORDER BY start_time DESC LIMIT ?,? ;';
		$result =  $this->db->query($sql,array($_SESSION['user_id'],$pageSize*$page,$pageSize))->result_array();

		$res['content'] = $result;
		$res['page'] = $page;
		$res['all'] = ceil($page_sum/$pageSize);
		return $res;
	}

	public function getApplyAllState()
	{
		$sql = 'SELECT count(*) as num ,leave_type FROM users left outer join leader on users.auto_id = leader.leader_id LEFT OUTER JOIN  leave_apply on leader.user_id = leave_apply.applier_id WHERE users.auto_id = ? AND (leave_apply.state = "pending" ) GROUP by leave_type ';
		$result = $this->db->query($sql, array($_SESSION['user_id']))->result_array();
		$res =array();
		if(count($result) == 0  )
		{
			$res['leave'] = array("num"=>0,"leave_type"=>"leave");
			$res['sick'] = array("num"=>0,"leave_type"=>"sick_leave");
		}
		else if (count($result) == 1)
		{
			if($result[0]['leave_type'] == "leave")
			{
				$res['leave'] = $result[0];
				$res['sick'] = array("num"=>0,"leave_type"=>"sick_leave");
			}
			else
			{
				$res['leave'] = array("num"=>0,"leave_type"=>"leave");
				$res['sick'] = $result[0];
			}
		}
		else
		{
			$res['leave'] = $result[0];
			$res['sick'] = $result[1];
		}

		$sql = 'SELECT count(*) as num  FROM users left outer join leader on users.auto_id = leader.leader_id LEFT OUTER JOIN  unpaid_leave_apply on leader.user_id = unpaid_leave_apply.applier_id WHERE users.auto_id = ? AND (unpaid_leave_apply.state = "pending")';
		$res['unpaid'] = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		$sql = 'SELECT count(*) as num  FROM users left outer join leader on users.auto_id = leader.leader_id LEFT OUTER JOIN  allowance_apply on leader.user_id = allowance_apply.applier_id WHERE users.auto_id = ? AND (allowance_apply.state = "pending")';
		$res['allowance'] = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		$sql = 'SELECT COUNT(*) as num  FROM overtime_apply WHERE overtime_apply.manager_id = ? AND state = "pending" ;';
		$res['overtime'] = $this->db->query($sql,array($_SESSION['user_id']))->row_array();

		$sql = "SELECT count(*) as num  FROM users left outer join leader on users.auto_id = leader.leader_id LEFT OUTER JOIN  missing_card on leader.user_id = missing_card.user_id WHERE users.auto_id = ? AND (missing_card.state = 'pending')";
		$res['mc'] = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		return $res;

	}
	public function _passLeaveState($data)
	{
		$res = $this->leaveDetailState($data['id']);
		if(!isset($res['auto_id']) || $res['state'] == "approved_by_mgr") return array("code"=>"106","text"=>"No Authentication");
		$val = (int)((strtotime($res['end_time']) - strtotime($res['start_time']))/3600.0+0.5);


		if($val%24 == 9 || $val%24 == 23)
		{
			$val = floor($val/24) +1;
		}
		else
		{
			$val = floor($val/24)+($val%24)/8.0;
		}

		$count = 0;
		$day = date("Y-m-d",strtotime($res['start_time'])) ;

		while( $day <= date("Y-m-d",strtotime($res['end_time']))  )
		{
			if(date('w',strtotime($day)) == 6 || date('w',strtotime($day)) == 0)
			{
				$count++;
			}
			$day = date('Y-m-d', strtotime ("+1 day", strtotime($day)));
		}
		$val = $val - $count;
		$real_val = $val;
		//return array($val);
		//count the total days of leaving

		$sql = "SELECT * FROM leave_days WHERE user_id = ? ORDER BY cal_year DESC LIMIT 2 ;";
		$datas = $this->db->query($sql,array($res['applier_id']))->result_array();
		if($res['leave_type'] == 'leave')
		{

			if(count($datas) == 2)
			{
				if($datas[1]['leave_balance'] < $val )
				{

					$val = $val - $datas[1]['leave_balance'] ;
					$val = $datas[0]['leave_balance'] - $val ;
					if($val < 0) return array("code"=>"108","text"=>"Annual Leave isn't enough");
					$sql = "UPDATE leave_days SET leave_balance = 0 WHERE user_id = ? AND cal_year = ? ;";
					$sql2 = "UPDATE leave_days SET leave_balance = ? WHERE user_id = ? AND cal_year = ? ;";
					$this->db->query($sql,array($res['applier_id'],$datas[1]['cal_year']));
					$this->db->query($sql2,array($val,$res['applier_id'],$datas[0]['cal_year']));
					//两次update
				}
				else
				{
					//	return array("gggg");
					$val = $datas[1]['leave_balance'] - $val;
					$sql = 'UPDATE leave_days SET leave_balance = ? WHERE user_id = ? AND cal_year = ? ;';
					$this->db->query($sql,array($val,$res['applier_id'],$datas[1]['cal_year']));
				}
			}
			else
			{
				$val = $datas[0]['leave_balance'] - $val ;
				if($val < 0) return array("code"=>"108","text"=>"Annual Leave isn't enough");
				$sql = "UPDATE leave_days SET leave_balance = ? WHERE user_id = ? AND cal_year = ? ;";
				$this->db->query($sql,array($val,$res['applier_id'],$datas[0]['cal_year']));
			}
		}
		else
		{
			$val = $datas[0]['sick_leave_balance'] - $val ;
			if($val < 0) return array("code"=>"108","text"=>"Annual Sick Leave isn't enough");
			$sql = "UPDATE leave_days SET sick_leave_balance = ? WHERE user_id = ? AND cal_year = ? ;";
			$this->db->query($sql,array($val,$res['applier_id'],$datas[0]['cal_year']));
		}


		$sql = "UPDATE leave_apply SET state = ? WHERE auto_id = ? ;";
		$this->db->query($sql,array("approved_by_mgr",$data['id']));

		$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";
		$leave_bal = $sick_bal = 0;
		if($res['leave_type'] == 'leave') $leave_bal = $real_val*-1;
		else $sick_bal = $real_val*-1 ;
		$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"pass-by-mgr-".$res['leave_type'],date("Y-m-d H:i:s"),$leave_bal,$sick_bal));
		//insert the log of insertion
		return array("code"=>"200");
	}
	public function passLeaveState($data)
	{
		return $this->_redisKeyInterFace("_passLeaveState","_passLeaveState",$data);
	}

	public function leaveDetailState($id)
	{
		$sql = 'SELECT auto_id,start_time,end_time,time_length,reason,leave_type,applier_id,state FROM `leave_apply` LEFT OUTER JOIN leader ON leave_apply.applier_id = leader.user_id  WHERE leader.leader_id = ? AND leave_apply.auto_id = ? ;';
		return $this->db->query($sql,array($_SESSION['user_id'],$id))->row_array();

	}

	public function unpaidDetailState($id)
	{
		$sql = 'SELECT auto_id,start_time,end_time,time_length,reason,leave_type,applier_id,state FROM `unpaid_leave_apply` LEFT OUTER JOIN leader ON unpaid_leave_apply.applier_id = leader.user_id  WHERE leader.leader_id = ? AND unpaid_leave_apply.auto_id = ? ;';
		return $this->db->query($sql,array($_SESSION['user_id'],$id))->row_array();
	}

	public function overtimeDetailState($id)
	{
		$sql = 'SELECT * FROM `overtime_apply` LEFT OUTER JOIN profile ON overtime_apply.employee_id = profile.user_id WHERE auto_id = ? AND manager_id = ?;';
		return $this->db->query($sql,array($id,$_SESSION['user_id']))->row_array();
	}

	public function mcDetailState($id)
	{
		$sql = 'SELECT auto_id,start_time,end_time,reason,state FROM `missing_card` LEFT OUTER JOIN leader ON missing_card.user_id = leader.user_id  WHERE leader.leader_id = ? AND missing_card.auto_id = ? ;';
		return $this->db->query($sql,array($_SESSION['user_id'],$id))->row_array();
	}

	public function allowanceDetailState($id)
	{
		$sql = 'SELECT auto_id,start_time,end_time,time_length,reason,leave_type,applier_id,state FROM `allowance_apply` LEFT OUTER JOIN leader ON allowance_apply.applier_id = leader.user_id  WHERE leader.leader_id = ? AND allowance_apply.auto_id = ? ;';
		return $this->db->query($sql,array($_SESSION['user_id'],$id))->row_array();
	}

	public function declineLeaveState($data)
	{
		$res = $this->leaveDetailState($data['id']);
		//var_dump($res);exit;
		if(!isset($res['auto_id']) || $res['state'] == "approved_by_mgr")
		{
			return array("code"=>"106","text"=>"No Authentication");
		}
		$sql = "UPDATE leave_apply SET state = ? WHERE auto_id = ? ;";
		$this->db->query($sql,array("declined",$data['id']));

		$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";
		$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"declined-by-mgr-".$res['leave_type'],date("Y-m-d H:i:s"),0,0));
		return array("code"=>"200");
	}

	public function passUnpaidState($data)
	{
		$res = $this->unpaidDetailState($data['id']);
		if(!isset($res['auto_id']) || $res['state'] == 'approved_by_mgr') return array("code"=>"106","text"=>"No Authentication");

		$sql = "UPDATE unpaid_leave_apply SET state = ? WHERE auto_id = ? ;";
		$this->db->query($sql,array("approved_by_mgr",$data['id']));

		$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";
		$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"pass-by-mgr-unpaid-".$res['leave_type'],date("Y-m-d H:i:s"),0,0));
		//insert the log of insertion
		return array("code"=>"200");
	}

	public function declineUnpaidState($data)
	{
		$res = $this->unpaidDetailState($data['id']);
		if(!isset($res['auto_id']) || $res['state'] == 'approved_by_mgr') return array("code"=>"106","text"=>"No Authentication");

		$sql = "UPDATE unpaid_leave_apply SET state = ? WHERE auto_id = ? ;";
		$this->db->query($sql,array("declined",$data['id']));

		$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";
		$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"declined-by-mgr-unpaid-".$res['leave_type'],date("Y-m-d H:i:s"),0,0));
		return array("code"=>"200");
	}

	public function declineAllowanceState($data)
	{
		$res = $this->allowanceDetailState($data['id']);
		if(!isset($res['auto_id']) || $res['state'] == 'approved_by_mgr') return array("code"=>"106","text"=>"No Authentication");

		$sql = "UPDATE allowance_apply SET state = ? WHERE auto_id = ? ;";
		$this->db->query($sql,array("declined",$data['id']));

		$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";
		$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"declined-by-mgr-unpaid-".$res['leave_type'],date("Y-m-d H:i:s"),0,0));
		return array("code"=>"200");
	}

	public function passAllowanceState($data)
	{
		$res = $this->allowanceDetailState($data['id']);
		if(!isset($res['auto_id']) || $res['state'] == 'approved_by_mgr') return array("code"=>"106","text"=>"No Authentication");

		$sql = "UPDATE allowance_apply SET state = ? WHERE auto_id = ? ;";
		$this->db->query($sql,array("approved_by_mgr",$data['id']));

		$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`,`apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?,?) ;";
		$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"pass-by-mgr-allowance-".$res['leave_type'],date("Y-m-d H:i:s"),0,0));
		//insert the log of insertion
		return array("code"=>"200");
	}

	public function cancelLeaveState($data)
	{
		return $this->_redisKeyInterFace("_cancelLeaveState","_cancelLeaveState",$data);
	}

	public function _cancelLeaveState($data)
	{
		$res = $this->leaveDetailState($data['id']);
		if(!isset($res['auto_id']) || $res['state'] == "pending") return array("code"=>"106","text"=>"No Authentication");
		// Find the amount via log
		$sql = 'SELECT leave_balance_change,sick_balance_change FROM leave_related_log WHERE apply_id = ? AND methods = ? ORDER BY create_time DESC LIMIT 1;';
		$balance = $this->db->query($sql, array($data['id'],"pass-by-mgr-".$res['leave_type']))->row_array();

		$sql = "SELECT * FROM leave_days WHERE user_id = ? ORDER BY cal_year DESC LIMIT 1 ;";
		$datas = $this->db->query($sql,array($res['applier_id']))->row_array();
		if($res['leave_type'] == 'leave')
		{
			$sql = "UPDATE leave_days SET leave_balance = leave_balance + ? WHERE user_id = ? AND cal_year = ? ;";
			$this->db->query($sql,array($balance['leave_balance_change']*(-1),$res['applier_id'],$datas['cal_year']));
		}
		else
		{
			$sql = "UPDATE leave_days SET sick_leave_balance = sick_leave_balance + ? WHERE user_id = ? AND cal_year = ?  ;";
			$this->db->query($sql,array($balance['sick_balance_change']*(-1),$res['applier_id'],$datas['cal_year']));
		}


		$sql = "UPDATE leave_apply SET state = ? WHERE auto_id = ? ;";
		$this->db->query($sql,array("cancelld",$data['id']));

		$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?) ;";
		$leave_bal = $sick_bal = 0;
		if($res['leave_type'] == 'leave') $leave_bal = $balance['leave_balance_change']*(-1);
		else $sick_bal = $balance['sick_balance_change']*(-1) ;

		$this->db->query($sql ,array($_SESSION['user_id'],"cancel-by-mgr-".$res['leave_type'],date("Y-m-d H:i:s"),$leave_bal,$sick_bal));
		//insert the log of insertion
		return array("code"=>"200");
	}

	public function cancelUnpaidState($data)
	{
		$res = $this->unpaidDetailState($data['id']);
		if(!isset($res['auto_id']) || $res['state'] == "pending") return array("code"=>"106","text"=>"No Authentication");
		// Find the amount via log



		$sql = "UPDATE unpaid_leave_apply SET state = ? WHERE auto_id = ? ;";
		$this->db->query($sql,array("cancelld",$data['id']));

		$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?) ;";
		$leave_bal = $sick_bal = 0;


		$this->db->query($sql ,array($_SESSION['user_id'],"cancel-by-mgr-unpaid-".$res['leave_type'],date("Y-m-d H:i:s"),0,0));
		//insert the log of insertion
		return array("code"=>"200");
	}

	public function cancelAllowanceState($data)
	{
		$res = $this->allowanceDetailState($data['id']);
		if(!isset($res['auto_id']) || $res['state'] == "pending") return array("code"=>"106","text"=>"No Authentication");
		// Find the amount via log



		$sql = "UPDATE allowance_apply SET state = ? WHERE auto_id = ? ;";
		$this->db->query($sql,array("cancelld",$data['id']));

		$sql = "INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,?,?,?,?,?) ;";
		$leave_bal = $sick_bal = 0;


		$this->db->query($sql ,array($_SESSION['user_id'],"cancel-by-mgr-unpaid-".$res['leave_type'],date("Y-m-d H:i:s"),0,0));
		//insert the log of insertion
		return array("code"=>"200");
	}

	public function _cancelOvertimeState($data)
	{
		$res = $this->overtimeDetailState($data['id']);
		if(!isset($res['auto_id']) ) return array("code"=>"106","text"=>"No Authentication");
		if($res['state'] == 'pending')
		{
			switch($res['pay_or_day'])
			{
				case "pay" :

					$sql = "INSERT INTO overtime_pay_log (auto_id,employee_id,manager_id,create_time,amount,opertation) VALUES (null,?,?,?,?,?);";
					$this->db->query($sql ,array($res['employee_id'],$_SESSION['user_id'],date("Y-m-d H:i:s"),$res['compension'],"minus"));

					break;
				case "day":
					break;
				default:
					break;
			}
			$sql = "UPDATE overtime_apply SET state = 'cancelled' WHERE auto_id = ? ;";
			$this->db->query($sql , array($data['id']));
			return array("code"=>"200");
		}


		//The followings are only used in approved_by_mgr status
		$date_now = date("Y-m-d H:i:s");
		switch($res['compension'])
		{
			case 1:
				$day_amount = 1.5;break;
			case 2:
				$day_amount = 2.0;break;
			case 3:
				$day_amount = 3.0;break;
			default : $day_amount = 0;break;
		}


		switch($res['pay_or_day'])
		{
			case "pay" :

				$sql = "INSERT INTO overtime_pay_log (auto_id,employee_id,manager_id,create_time,amount,opertation) VALUES (null,?,?,?,?,?);";
				$this->db->query($sql ,array($res['employee_id'],$_SESSION['user_id'],$date_now,$res['compension'],"minus"));

				$sql = 'INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null ,?,?,?,?,?,?);';
				$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"cancelld-by-mgr-overtime",$date_now,0,0));
				break;
			case "day":
				$sql = "SELECT * FROM leave_days WHERE user_id = ? ORDER BY cal_year DESC LIMIT 1 ;";
				$datas = $this->db->query($sql,array($res['employee_id']))->row_array();
				$sql = "UPDATE leave_days SET leave_balance = leave_balance - ? WHERE user_id = ? AND cal_year = ? ;";
				$this->db->query($sql,array($day_amount,$res['employee_id'],$datas['cal_year']));
				$sql = 'INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null ,?,?,?,?,?,?);';
				$this->db->query($sql ,array($_SESSION['user_id'],$data['id'],"cancelld-by-mgr-overtime",$date_now,(-1)*$day_amount,0));
				break;
			default:
				break;
		}
		$sql = "UPDATE overtime_apply SET state = 'cancelled' WHERE auto_id = ? ;";
		$this->db->query($sql , array($data['id']));
		return array("code"=>"200");
	}

	public function cancelOvertimeState($data)
	{
		return $this->_redisKeyInterFace("_cancelOvertimeState","_cancelOvertimeState",$data);
	}

	public function approveOvertimeState($data)
	{
		return $this->_redisKeyInterFace("_approveOvertimeState","_approveOvertimeState",$data);
	}
	public function _approveOvertimeState($data)
	{
		$res = $data;
		$sql = "SELECT * FROM overtime_apply WHERE auto_id = ? ;";
		$data = $this->db->query($sql,array($res['id']))->row_array();
		$this->db->query("DELETE FROM `overtime_apply` WHERE auto_id = ?",array($res['id']));

		switch($data['compension'])
		{
			case "1 day":
				$day_amount = 1.0;
				$com_real = 1;
				break;
			case "1.5 days":
				$day_amount = 1.5;
				$com_real = 2;
				break;
			case "2 days":
				$day_amount = 2.0;
				$com_real = 3;
				break;
			case "3 days":
				$day_amount = 3.0;
				$com_real = 4;
				break;
			default : $day_amount = 0;
				$com_real = 0;break;
		}
		$date_now = date("Y-m-d H:i:s");
		$dayFlag = $data['pay_or_day'] == "day" ? TRUE : FALSE;

		$data['reason'] = empty($data['reason']) ? "" : $data['reason'];
		if($dayFlag)
		{

			$sql = "INSERT INTO `overtime_apply`(`auto_id`, `manager_id`, `employee_id`, `create_time`, `start_time`, `end_time`, `time_length`, `pay_or_day`, `compension`, `reason`, `hr_id`,`state`) VALUES (null,?,?,?,?,?,?,?,?,?,?,?) ;";

			$res = $this->db->query($sql ,array($data['manager_id'],$data['employee_id'],$date_now,$data['start_time'],$data['end_time'],$data['time_length'],$data['pay_or_day'],$com_real,$data['reason'],0,"approved_by_mgr"));

			$sql = 'SELECT auto_id FROM overtime_apply WHERE manager_id = ? AND employee_id = ? AND create_time = ?;';
			$id = $this->db->query($sql,array($data['manager_id'],$data['employee_id'],$date_now))->row_array();$id = $id['auto_id'];

			$year = $this->db->query("select max(cal_year) as year FROM leave_days WHERE user_id = ?; ",array($data['employee_id']))->row_array();$year = $year['year'];

			$sql = "UPDATE leave_days SET leave_balance = leave_balance + ? WHERE user_id = ? AND cal_year = ? ;";
			$this->db->query($sql,array($day_amount,$data['employee_id'],$year));

			$sql = 'INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,? ,? ,? ,? ,? ,? );';
			$this->db->query($sql ,array($data['manager_id'],$id,'approve-overtime-day',$date_now,$day_amount,0));
			return array("code"=>"200");
			//$this->db->query($sql, array($employee_id,$))
		}

		else
		{
			$sql = "INSERT INTO `overtime_apply`(`auto_id`, `manager_id`, `employee_id`, `create_time`, `start_time`, `end_time`, `time_length`, `pay_or_day`, `compension`, `reason`, `hr_id`,`state`) VALUES (null,?,?,?,?,?,?,?,?,?,?,?) ;";

			$res = $this->db->query($sql ,array($data['manager_id'],$data['employee_id'],$date_now,$data['start_time'],$data['end_time'],$data['time_length'],$data['pay_or_day'],$day_amount,$data['reason'],0,"approved_by_mgr"));
			$sql = 'INSERT INTO `overtime_pay_log`(`auto_id`, `employee_id`, `manager_id`, `create_time`, `amount`) VALUES (null,?,?,?,?);';
			$this->db->query($sql ,array($data['employee_id'],$data['manager_id'],$date_now,$day_amount));

			$sql = 'SELECT auto_id FROM overtime_apply WHERE manager_id = ? AND employee_id = ? AND create_time = ?;';
			$id = $this->db->query($sql,array($data['manager_id'],$data['employee_id'],$date_now))->row_array();$id = $id['auto_id'];

			$sql = 'INSERT INTO `leave_related_log`(`auto_id`, `user_id`, `apply_id`, `methods`, `create_time`, `leave_balance_change`, `sick_balance_change`) VALUES (null,? ,? ,? ,? ,? ,? );';
			$this->db->query($sql ,array($data['manager_id'],$id,'approve-overtime-pay',$date_now,0,0));
			return array("code"=>"200");
		}
	}

	public function cancelMcState($data)
	{
		$res = $this->mcDetailState($data['id']);
		if(!isset($res['auto_id']) ) return array("code"=>"106","text"=>"No Authentication");

		$sql = "UPDATE missing_card SET state = ? WHERE auto_id = ? ;";
		$this->db->query($sql,array("cancelled",$data['id']));
		return array("code"=>"200");
	}

	public function passMcState($data)
	{
		$res = $this->mcDetailState($data['id']);
		if(!isset($res['auto_id']) || $res['state'] == 'approved') return array("code"=>"106","text"=>"No Authentication");
		$sql = "UPDATE missing_card SET state = ? WHERE auto_id = ? ;";
		$this->db->query($sql,array("approved",$data['id']));

		return array("code"=>"200");
	}

//    public function declineMcState($data)
//    {
//        $res = $this->mcDetailState($data['id']);
//        if(!isset($res['auto_id']) || $res['state'] == 'approved') return array("code"=>"106","text"=>"No Authentication");
//        $sql = "UPDATE missing_card SET state = ? WHERE auto_id = ? ;";
//        $this->db->query($sql,array("decline",$data['id']));
//
//        return array("code"=>"200");
//    }


	public function generalState($month = "2017-02")
	{
		$subworkers = $this->getSubWorkerState($_SESSION['user_id']);
		$workers = array();
		foreach ($subworkers as $work)
		{
			$workers[] = $work['user_id'];
		}
		return $this->getMcLogState($workers,$month);
	}


	/*
     * The followings are used to generate general board
     */

	public function getMcLogState($workers,$month)
	{
//		$sql = "SELECT m.*,p.user_id FROM missing_card_log as m LEFT OUTER JOIN `profile` as p ON m.name = p.user_name WHERE p.user_id  in ? AND DATE_FORMAT(record_date,'%Y-%m') = ? ;";
//		$res = $this->db->query($sql,array($workers,$month))->result_array();
//		$holidays = $this->getHolidayDates();
//		foreach($res as $r=>$v)
//		{
//			if($v['check_in'] != "00:00:00" && $v['check_out'] != "00:00:00" && $v['check_in'] <="10:00" && $v['check_out'] >= "18:30")
//			{
//				unset($res[$r]);
//			}
//			if( date('w',strtotime($v['record_date'])) == 6 || date('w',strtotime($v['record_date'])) == 0 || in_array($v['record_date'],$holidays) )
//			{
//				unset($res[$r]);
//			}
//		}
//		$logged = $this->getMcDataState($workers,$month);
//		foreach($res as $r =>$v)
//		{
//			foreach($logged as $log)
//			{
//				if($log['record_date'] == $v['record_date'] && $log['user_id'] == $v['user_id'])
//				{
//					unset($res[$r]);
//				}
//			}
//		}
		$sql = "SELECT sub_id FROM subordinate_links WHERE user_id = ? ;";
		$res = $this->db->query($sql,array($_SESSION['user_id']))->row_array();
		$users = explode(",",$res['sub_id']);

		$sql = "SELECT a.*,profile.user_name FROM (
                SELECT applier_id as user_id,start_time,time_length,leave_type*3 as flag FROM `unpaid_leave_apply` WHERE applier_id IN ? AND state = 'approved_by_mgr' UNION 
                SELECT applier_id as user_id,start_time,time_length,leave_type*1 as flag FROM `leave_apply` WHERE applier_id IN ? AND state = 'approved_by_mgr' UNION 
                SELECT employee_id as user_id,start_time,time_length,pay_or_day*5 as flag FROM overtime_apply WHERE employee_id IN ? AND state = 'approved_by_mgr' UNION 
                SELECT applier_id as user_id,start_time,time_length,leave_type * 11 as flag FROM allowance_apply WHERE applier_id IN ? AND state = 'approved_by_mgr') as a LEFT OUTER JOIN profile ON a.user_id = profile.user_id
                WHERE DATE_FORMAT(start_time,'%Y-%m') = ? ORDER BY start_time ASC";
		$res = $this->db->query($sql,array($users,$users,$users,$users,$month))->result_array();
		//var_dump($res);exit;


		//   return $res;

		$counter = 0;
		$year = date ( 'Y',strtotime($month."-01") );
		$month = date ( 'n',strtotime($month."-01") );
		$firstDay = date ( "w", mktime ( 0, 0, 0, $month, 1, $year ) );
		//获得当月第一天
		$daysInMonth = date ( "t", mktime ( 0, 0, 0, $month, 1, $year ) );
		//获得当月的总天数
		//echo $daysInMonth;
		$tempDays = $firstDay + $daysInMonth;   //计算数组中的日历表格数
		$weeksInMonth = ceil ( $tempDays/7 );   //算出该月一共有几周（即表格的行数）
		//创建一个二维数组
		for($j = 0; $j < $weeksInMonth; $j ++) {
			for($i = 0; $i < 7; $i ++) {
				$counter ++;
				$week [$j] [$i] = $counter;
				//offset the days
				$week [$j] [$i] -= $firstDay;

				if (($week [$j] [$i] < 1) || ($week [$j] [$i] > $daysInMonth)) {
					$week [$j] [$i] = "";
				}
				else
				{
					//if(date("w",mktime(0,0,0,$month,$week[$j][$i],$year)) == 0 || date("w",mktime(0,0,0,$month,$week[$j][$i],$year)) == "6")
					//$week[$j][$i] = "<h1 style='background-color: #111111'>{$week[$j][$i]}</h1><ul>".$this->getDailyOut($res,date("Y-m-d",mktime(0,0,0,$month,$week[$j][$i],$year)))."</ul>";
					$week[$j][$i] = "<h1>{$week[$j][$i]}</h1><ul>".$this->getDailyOut($res,date("Y-m-d",mktime(0,0,0,$month,$week[$j][$i],$year)))."</ul>";
				}

			}
		}
		return $week;


	}

	public function getMcDataState($workers,$month)
	{
		$sql = 'SELECT user_id,DATE_FORMAT(start_time,"%Y-%m-%d") as record_date,DATE_FORMAT(start_time,"%H:%i") as check_in , DATE_FORMAT(end_time,"%H:%i") as check_out FROM missing_card WHERE user_id in ? AND DATE_FORMAT(end_time,"%Y-%m") = ? ;';
		$res = $this->db->query($sql,array($workers,$month))->result_array();
		return $res;
	}
	public function getHolidayDates()
	{

		$sql = "SELECT holiday_date FROM holiday_year WHERE 1 ;";
		$res = $this->db->query($sql)->result_array();
		$out = array();
		foreach ($res as $r => $v)
		{
			$out[] = $v['holiday_date'];
		}
		return $out;
	}

	public function getDailyOut($res,$day)
	{
		$all = "";
		foreach($res as $r=>$v)
		{
			if(date("Y-m-d",strtotime($v['start_time'])) == $day)
			{
				$all.= "<li>".$v['user_name']." : ".$v['time_length']." ".$this->other_flag[$v['flag']]."</li>";

			}
		}
		return $all;
	}

	public function officialState()
	{
		$workers = $this->db->query("SELECT sub_id FROM subordinate_links WHERE user_id =?",array($_SESSION['user_id']))->row_array();
		$workers = $workers['sub_id'];
		$workers = explode(",",$workers);//var_dump($workers);exit;
		$sql = "SELECT name as user_name,start_date,probation_date,Internable,Probationable,users.auto_id FROM `company_members` LEFT OUTER JOIN users ON company_members.work_email = users.user_id LEFT OUTER JOIN profile ON profile.user_id = users.auto_id WHERE auto_id IN ? AND (Internable = 'Yes' OR Probationable = 'Yes');";
		return ($this->db->query($sql,array($workers))->result_array());
	}

	public function isBelongTo($id)
	{
		$workers = $this->db->query("SELECT user_id FROM leader WHERE user_id =? AND leader_id = ? ",array($id,$_SESSION['user_id']))->row_array();
		return (isset($workers['user_id']));
	}

	public function officialDetailState($id)
	{
		$sql = "SELECT name ,start_date,probation_date,Internable,Probationable,users.auto_id FROM `company_members` LEFT OUTER JOIN users ON company_members.work_email = users.user_id LEFT OUTER JOIN profile ON profile.user_id = users.auto_id WHERE auto_id = ? AND (Internable = 'Yes' OR Probationable = 'Yes');";
		return ($this->db->query($sql,array($id))->row_array());
	}

	public function updateOfficialState($data)
	{

		$data['probation_date'] = isset($data['probation_date']) ? $data['probation_date']: "";

		if($data['probation'] == "yes") //set all leave_days to 0
		{
		}
		else
		{
			$res = $this->db->query("SELECT Probationable FROM profile WHERE user_id = ? ;",array($data['id']))->row_array();//var_dump($res);exit;
			if($res['Probationable'] == "yes")
			{
				$probation_date = date("Y-m-d");
				$this->db->query("UPDATE leave_days SET leave_balance = 10,sick_leave_balance = 3.5 WHERE user_id = ? ;",array($data['id']));

			}
		}


		if($data['intern'] == "yes") //set all leave_days to 0
		{
		}
		else
		{
			$res = $this->db->query("SELECT Internable FROM profile WHERE user_id = ? ;",array($data['id']))->row_array();//var_dump($res);exit;
			if($res['Internable'] == "yes")
			{
				$probation_date = date("Y-m-d");
				$this->db->query("INSERT INTO leave_days (user_id,cal_year,leave_balance,sick_leave_balance) VALUES (?,?,?,? ) ;",array($data['id'],$probation_date,10,3.5));

			}
		}
		$email = $this->db->query("SELECT user_id FROM users WHERE auto_id = ? ;",array($data['id']))->row_array();
		$sql = "UPDATE `company_members` SET `probation_date` = ? WHERE work_email = ? ;";
		$this->db->query($sql,array($data['probation_date'],$email));


		$sql = "UPDATE profile SET Internable = ?,Probationable = ? WHERE user_id = ? ;";
		$this->db->query($sql , array($data['intern'],$data['probation'],$data['id']));
		return array("code"=>"200");
	}





	public function getUnAppliedMcRecord($workers,$month)
	{
		$sql = "SELECT m.*,p.user_id FROM missing_card_log as m LEFT OUTER JOIN `profile` as p ON m.name = p.user_name WHERE p.user_id  in ? AND DATE_FORMAT(record_date,'%Y-%m') = ? ;";
		$res = $this->db->query($sql,array($workers,$month))->result_array();
		$holidays = $this->getHolidayDates();
		foreach($res as $r=>$v)
		{
			if($v['check_in'] != "00:00:00" && $v['check_out'] != "00:00:00" && $v['check_in'] <="10:00" && $v['check_out'] >= "18:30")
			{
				unset($res[$r]);
			}
			if( date('w',strtotime($v['record_date'])) == 6 || date('w',strtotime($v['record_date'])) == 0 || in_array($v['record_date'],$holidays) )
			{
				unset($res[$r]);
			}
		}
		$logged = $this->getMcDataState($workers,$month);
		foreach($res as $r =>$v)
		{
			foreach($logged as $log)
			{
				if($log['record_date'] == $v['record_date'] && $log['user_id'] == $v['user_id'])
				{
					unset($res[$r]);
				}
			}
		}
		return $res;
	}

	public function getUnAppliedMc($date = "")
	{
		if($date == "") $date = date("Y-m");
		$subworkers = $this->getSubWorkerState($_SESSION['user_id']);
		$workers = array();
		foreach ($subworkers as $work)
		{
			$workers[] = $work['user_id'];
		}
		return $this->getUnAppliedMcRecord($workers,$date);
	}

	public function getSubLeaveAndSickDaysState()
	{
		$subworkers = $this->getSubWorkerState($_SESSION['user_id']);
		$return_array = array();
		foreach($subworkers as $worker)
		{
			$sql = "SELECT leave_balance,sick_leave_balance,cal_year FROM leave_days WHERE user_id = ? ORDER BY cal_year DESC LIMIT 2 ;";
			$data = $this->db->query($sql,array($worker['user_id']))->result_array();
			$res = array();
			// $return_array[$worker['user_id']] = count($data);
			if(empty($data))
			{
				$return_array[$worker['user_id']] = array("leave_days"=>"0","sick_days"=>"0","leave_overdue"=>array("num"=>"0","day"=>"0"),"sick_overdue"=>array("num"=>"0","day"=>"0"));
				$return_array[$worker['user_id']]['user_name'] = $worker['user_name'];
				continue;
			}
			$date_overdue = date_create(date("Y-m-d",strtotime("+1 year",strtotime($data[0]['cal_year']))));
			$date_now = date_create(date("Y-m-d"));
			if(count($data) == 2)
			{
				$res['leave_days'] = $data[0]['leave_balance']+$data[1]['leave_balance'];

				$res['leave_overdue'] = array("num"=>$data[1]['leave_balance'],"day"=>date_diff($date_overdue,$date_now)->format("%a"));
			}
			else
			{
				$res['leave_days'] = $data[0]['leave_balance'];
				$res['leave_overdue'] = array("num"=>"0","day"=>"0");
			}

			$res['sick_days'] = $data[0]['sick_leave_balance'];
			$res['sick_overdue'] = array("num"=>$data[0]['sick_leave_balance'],"day"=>date_diff($date_overdue,$date_now)->format("%a"));
			$return_array[$worker['user_id']] = $res;
			$return_array[$worker['user_id']]['user_name'] = $worker['user_name'];
		}
		return $return_array;
	}
}