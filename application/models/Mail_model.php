<?php
require_once(getcwd().'/application/models/SS_model.php');
require_once(getcwd()."/application/libraries/PHPMailerAutoload.php");

class Mail_model extends SS_model
{
    protected $mailConn ;
    public static $leaveContent = 'You have received a new leave request in the system from %s.';
    public static $unpaidContent = 'You have received a new unpaid leave request in the system from %s.';
    public static $allowanceContent = 'You have received a new allowance request in the system from %s.';
    public static $approveLeave = 'Your Manager has approved your leave request.<br/><a href="http://hr.motionglobal.com/">Please login and check</a>';
    public static $cancelLeave = 'Your Manager has cancelled your leave request.<br><a href="http://hr.motionglobal.com/">Please login and check</a>';
    public static $overtimeContent = 'You have received a new overtime request from %s.<br/><a href="http://hr.motionglobal.com/">Please login and check</a>';
    public static $mcContent = 'You have received a new missing card request in the system.<br/><a href="http://hr.motionglobal.com/">Please log in and check</a>';
    public static $approveMc = 'Your Manager has approved your missing card request.<br/><a href="http://hr.motionglobal.com/"> login and check</a>';
    public static $cancelMc = 'Your Manager has cancelled your missing card request.<br><a href="http://hr.motionglobal.com/">Please login and check</a>';
    public static $decline = 'Your Manager has declined your request.<br><a href="http://hr.motionglobal.com/">Please login and check</a>';
    public static $loginCheck = '<br/><a href="http://hr.motionglobal.com/">Please log in and check</a>';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->library('encryption');

    }

    public function send($subject = '',$body = '',$receivers = '',$sender ='')
    {
        if(strpos($body,"%s") > 0)
        {
            $head = substr($body,0,strpos($body,"%s")).$sender;
            $head .= substr($body, strpos($body,"%s")+2,strlen($body));
            $body = $head;
        }
        $this->db->query("INSERT INTO `mail_queue`(`auto_id`, `subject`, `body`, `receivers`, `status`, `create_time`, `send_time`) VALUES (null,?,?,?,'init',?,null)",
            array($subject,$body,serialize($receivers),date("Y-m-d H:i:s")));
        $log = '';
        $log.= 'Message has been recorded';
        $this->log($log);
    }
    public function dealMail()
    {
        $res = $this->db->query("SELECT * FROM mail_queue WHERE status = ? LIMIT 5",array("init"))->result_array();var_dump($res);
        foreach($res as $r=>$v)
        {
            $this->mailConn = new PHPMailer;
            $this->mailConn->isSMTP();
            $this->mailConn->CharSet = "UTF-8";
            $this->mailConn->Host = 'smtp.emailsrvr.com';  // Specify main and backup SMTP servers
            $this->mailConn->SMTPAuth = true;                               // Enable SMTP authentication
            $this->mailConn->Username = 'itom@motionglobal.com';                 // SMTP username
            $this->mailConn->Password = 'motion888';                           // SMTP password
            //$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $this->mailConn->Port = 25;
            $this->mailConn->setFrom('itom@motionglobal.com', 'HR System Admin');
            foreach(unserialize($v['receivers']) as $rec)
            {
                $this->mailConn->addAddress($rec['user_id'],$rec['mail']);     // Add a recipient
            }
            $this->mailConn->isHTML(true);                                  // Set email format to HTML

            $this->mailConn->Subject = $v['subject'];
            $this->mailConn->Body    = $v['body'];
            $this->mailConn->AltBody = '';
            $log = '';
            if(!$this->mailConn->send())
            {
                $log.= 'Message could not be sent.';
                $log.= 'Mailer Error: ' . $this->mailConn->ErrorInfo;
            }
            else
            {
                $this->db->query("UPDATE mail_queue SET status = ? ,send_time = ? WHERE auto_id = ? ",array("done",date("Y-m-d H:i:s"),$v['auto_id']));
                $log.= 'Message has been sent';
            }
            $this->log($log);
            unset($this->mailConn);
        }
        return $res;

    }
    
    public function getDirectorEmails()
    {
        return $this->db->query("SELECT users.mail as user_id , users.mail FROM users LEFT OUTER JOIN profile ON users.auto_id = profile.user_id WHERE profile.department = 'Director';")->result_array();
    }

    public function getManagers()
    {
        $sql = 'SELECT user_id FROM `profile` WHERE title like "%Manager%"';
        $res = $this->db->query($sql,array($sql))->result_array();
        $res_new = array();
        foreach($res as $r)
        {
            $res_new[] = $r['user_id'];
        }
        return $res_new;
    }

    public function getApplierEmail($apply_id,$type)
    {
        switch($type)
        {
            case "leave":
                $sql = "SELECT users.user_id as mail,users.user_id as user_id FROM leave_apply LEFT OUTER JOIN users ON leave_apply.applier_id = users.auto_id WHERE leave_apply.auto_id = ? ;";
                $res = $this->db->query($sql,array($apply_id))->row_array();break;
            case "unpaid":
                $sql = "SELECT users.user_id as mail,users.user_id as user_id FROM unpaid_leave_apply LEFT OUTER JOIN users ON unpaid_leave_apply.applier_id = users.auto_id WHERE unpaid_leave_apply.auto_id = ? ;";
                $res = $this->db->query($sql,array($apply_id))->row_array();break;
            case "mc" :
                $sql = "SELECT users.user_id as mail,users.user_id as user_id FROM missing_card LEFT OUTER JOIN users ON missing_card.user_id = users.auto_id WHERE missing_card.auto_id = ?";
                $res = $this->db->query($sql,array($apply_id))->row_array();break;
            case "allowance":
                $sql = "SELECT users.user_id as mail,users.user_id as user_id FROM allowance_apply LEFT OUTER JOIN users ON allowance_apply.applier_id = users.auto_id WHERE allowance_apply.auto_id = ? ;";
                $res = $this->db->query($sql,array($apply_id))->row_array();break;
            default :
                $res = array('mail'=>"","user_id"=>"");break;
        }
        return $res;
    }

}