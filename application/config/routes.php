<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'indexs/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
///////////////////////////////////////
$route['index/year'] = "Indexs/getYearDatas";

$route['login/action'] = 'User/loginAction';//post to Login
$route['user/reset/view'] = 'User/resetView';
//$route['register/action'] = 'User/registerAction';
//$route['user/register/confirm'] = 'User/confirmView';
$route['user/profile'] = 'User/profileView';
$route['user/edit/profile'] = "User/profileEditAction";//input:email
$route['user/get/name'] = 'User/getUsernameAction';
$route['user/settings'] = "User/settingsView";
$route['user/edit/settings'] = "User/alterPassAction";
$route['user/reset/password'] = "User/forgetPassAction";
$route['user/raw'] = "User/privateRawDataView";
//Controller : Leave
$route['leave'] = "Leave/leaveView";
$route['sick_leave'] = "Leave/sickLeaveView";
$route['leave/new'] = "Leave/newLeaveView";
$route['sick_leave/new'] = "Leave/newSickLeaveView";
$route['leave/add/new'] = "Leave/newLeaveAction";
$route['leave/cancel'] = "Leave/cancelLeaveAction";
$route['leave/detail/(:num)'] = "Leave/detailView/$1";
$route['sick_leave/detail/(:num)'] = "Leave/sickDetailView/$1";
$route['leave/detail/edit'] = "Leave/editAction";
$route['leave/detail/pend'] = "Leave/pendAction";

//Controller : Unpaid
$route['unpaid'] = "Unpaid/unPaidLeaveView";
$route['unpaid/new'] = "Unpaid/newUnpaidLeaveView";
$route['unpaid/add/new'] = "Unpaid/newUnpaidLeaveAction";
$route['unpaid/detail/(:num)'] = "Unpaid/unpaidDetailView/$1";
$route['unpaid/detail/edit'] = "Unpaid/editUnpaidAction";
$route['unpaid/detail/pend'] = "Unpaid/pendUnpaidAction";
$route['unpaid/cancel'] = "Unpaid/cancelUnpaidLeaveAction";


//Controller : Allowance
$route['allowance'] = "allowance/allowanceView";
$route['allowance/new'] = "allowance/newAllowanceView";
$route['allowance/add/new'] = "allowance/newAllowanceAction";
$route['allowance/detail/(:num)'] = "allowance/allowanceDetailView/$1";
$route['allowance/detail/edit'] = "allowance/editAllowanceAction";
$route['allowance/detail/pend'] = "allowance/pendAllowanceAction";
$route['allowance/cancel'] = "allowance/cancelAllowanceAction";


//Controller : Overtime
$route['overtime'] = 'Overtime/overtimeView';
$route['overtime/detail/(:num)'] = 'Overtime/overtimeDetailView/$1';
$route['overtime/new'] = 'Overtime/newOvertimeView';
$route['overtime/add/new'] = 'Overtime/newOvertimeAction';
$route['overtime/self/new'] = 'Overtime/newSelfOvertimeView';
$route['overtime/self/new/add'] = "Overtime/newSelfOvertimeAction";

$route['overtime/self/cancel'] = "Overtime/cancelOvertimeLeaveAction";
//$route['']


$route['api/leave/month'] = 'Leave/getLeaveByMonthAction';
$route['api/sick/month'] = 'Leave/getSickLeaveByMonthAction';
$route['api/allowance/month'] = "Allowance/getAllowanceByMonthAction";
$route['api/worker/list'] = "Leader/getAllLinkedWorkersAction";
$route['api/unpaid/month'] = 'Unpaid/getUnpaidLeaveByMonthAction' ; 
$route['api/overtime/month'] ="Overtime/getOvertimeByMonthAction";
$route['api/paid/days'] = 'Leave/getActualPayDaysAction';
$route['api/logout'] = "User/logoutAction";
$route['api/get/relation'] = "Hr/getRelationByPageAction";
$route['api/get/departs'] = "Hr/getDepartmentsAction";
$route['api/get/name/depart'] = "Hr/getNameByDepartmentAction";
$route['api/getState'] = "User/getNavAction";
$route['api/get/unfilled/mc'] = "Leader/getUnfilledMcAction";

$route['api/approve/leave/month'] = 'Leader/getApproveLeaveByMonthAction';
$route['api/approve/sick/month'] = 'Leader/getApproveSickByMonthAction';
$route['api/approve/unpaid/month'] ='Leader/getApproveUnpaidByMonthAction';
$route['api/approve/overtime/month'] ='Leader/getApproveOvertimeByMonthAction';
$route['api/approve/allowance/month'] ='Leader/getApproveAllowanceByMonthAction';
$route['api/approve/mc/month'] ='Leader/getApproveMcByMonthAction';
$route['api/get/last'] = "Hr/lastDayChartAction";
$route['api/sitemap/update/daily'] = "Sitemap/updateSubLinksTest";
$route['api/unpaid/judge'] = "User/getUnpaidVisibleAction";
$route['api/get/employee/report'] = "Hr/employeeReportAction";
$route['api/get/month/report'] = "Hr/monthReportAction";
$route['api/get/allowance/end'] = "Allowance/getEndTimeByTypeAction";
//Controller : Leader 
$route['approve'] = 'Leader/indexView';
$route['approve/leave'] = "Leader/leaveView";
$route['approve/sick'] = "Leader/sickView";
$route['approve/unpaid'] = "Leader/unpaidView"; 
$route['approve/overtime'] = "Leader/overtimeView";
$route['approve/allowance'] = "Leader/allowanceView";

$route['approve/leave/detail/(:num)'] = "Leader/leaveDetailView/$1";
$route['approve/sick/detail/(:num)'] = "Leader/sickDetailView/$1";
$route['approve/unpaid/detail/(:num)'] = "Leader/unpaidDetailView/$1";
$route['approve/overtime/detail/(:num)'] = "Leader/overtimeDetailView/$1";
$route['approve/allowance/detail/(:num)'] = "Leader/allowanceDetailView/$1";

$route['approve/leave/pass'] = "Leader/passLeaveAction";
$route['approve/leave/decline'] = 'Leader/declineLeaveAction';
$route['approve/unpaid/pass'] = "Leader/passUnpaidAction";
$route['approve/unpaid/decline'] = 'Leader/declineUnpaidAction';
$route['approve/allowance/pass'] = "Leader/passAllowanceAction";
$route['approve/allowance/decline'] = 'Leader/declineAllowanceAction';
$route['approve/overtime/pass'] = "Leader/approveOvertimeAction";

$route['approve/leave/cancel'] = "Leader/cancelLeaveAction";
$route['approve/unpaid/cancel'] = "Leader/cancelUnpaidAction";
$route['approve/overtime/cancel'] = "Leader/cancelOvertimeAction";
$route['approve/allowance/cancel'] = "Leader/cancelAllowanceAction";

//$route['approve/mc'] = 'Leader/mcView';
//$route['approve/mc/detail/(:num)'] = 'Leader/mcDetailView/$1';
//$route['approve/mc/pass'] = 'Leader/approveMcAction';
//$route['approve/mc/cancel'] = 'Leader/cancelMcAction';

$route['approve/general'] = "Leader/generalView";
$route['approve/general/data'] = "Leader/generalAction";

$route['approve/official'] = "Leader/officialView";
$route['approve/official/detail/(:num)'] = "Leader/officialDetailView/$1 ";
$route['approve/official/edit'] = "Leader/officialEditAction";

$route['approve/raw'] = "Leader/rawPanelView";
$route['approve/balance'] = "Leader/balanceView";

//
//$route['mc'] = 'Mc/indexView';
//$route['mc/new/(:any)'] = 'Mc/newMcRecordView/$1';
//$route['mc/add/new'] = "Mc/newMcRecordAction";
//$route['mc/set/ignore'] = "Mc/ignoreRecordAction";

$route['hr'] = 'Hr/indexView';
$route['hr/list'] = "Hr/listView";
$route['hr/resign/list'] = "Hr/resignedListView";
$route['hr/get/employee/depart'] = 'Hr/getEmployeeDataByDepartAction';
$route['hr/get/resign/employee/depart'] = 'Hr/getResignedEmployeeDataByDepartAction';
$route['hr/edit/employee/(:num)'] = 'Hr/EditEmployeeView/$1';
$route['hr/edit/employee/action'] = "Hr/editEmployeeAction";
$route['hr/new/employee'] ="Hr/newEmployeeView";
$route['hr/new/employee/action'] = "Hr/newEmployeeAction";
$route['hr/relation'] = "Hr/relationView";
$route['hr/set/relation'] = "Hr/updateRelationAction";
$route['hr/last'] = "Hr/lastDayChartView";
$route['hr/head'] = "Hr/headChartView";
$route['hr/employee/report'] = "Hr/employChartView";
$route['hr/month/report'] = "Hr/monthReportView";
$route['hr/raw/(:num)'] = "Hr/rawDataPanelView/$1";
$route['hr/raw/data'] = "Hr/rawDataAction";
$route['hr/get/relation/search'] = "Hr/searchRelationAction";
$route['hr/get/list/search'] = "Hr/searchListAction";
$route['sitemap'] = 'Sitemap/updateSubLinksAction';
$route['sitemap/(:num)'] = 'Sitemap/updateSubLinksAction/$1';
$route['sitemap/tree'] = "Sitemap/getAllMapAction";
$route['sitemap/get/single'] = "Sitemap/getMapSingleAction";
$route['relation/get/single'] = "Sitemap/getInfoAllState";
$route['relation/get/basic'] = "Sitemap/getBasicInfoAction";
$route['ttt'] = "user/testView";
//$route['data/transfer'] = 'user/tranAction';
$route['hr/balance/(:any)'] = "Hr/balanceView/$1";
$route['hr/balancechange/view'] = "Hr/balanceChangeView";
$route['hr/balancechange/get'] = "Hr/balanceChangeAction";
$route['monthlysendmail'] = "Leader/AnniversaryRemind";